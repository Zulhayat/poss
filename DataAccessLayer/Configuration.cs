﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    internal static class Configuration
    {
            const string DEFAULT_CONNECTION_KEY = "defaultConnection";

            public static string DefaultConnection
            {
                get
                {
                    return ConfigurationManager.AppSettings[DEFAULT_CONNECTION_KEY];
                }
            }

            public static string DBProvider
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings[DefaultConnection].ProviderName;
                }
            }

            public static string ConnectionString
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings[DefaultConnection].ConnectionString;
                }
            }
        }
    }
