﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DBParameter
    {
        private List<DBParameter> _parameterCollection = new List<DBParameter>();

        #region "Private Variables"
        private string _name = string.Empty;
        private object _value = null;
        private DbType _type;
        private ParameterDirection _paramDirection = ParameterDirection.Input;
        #endregion

        #region Constructor
        public DBParameter(string name, object value)
        {
            _name = name;
            _value = value;
        }

        public void Add(DBParameter parameter)
        {
            _parameterCollection.Add(parameter);
        }

        public DBParameter(string name, object value, ParameterDirection paramDirection)
        {
            _name = name;
            _value = value;
            _paramDirection = paramDirection;
        }

        public DBParameter(string name, object value, DbType dbType)
        {
            _name = name;
            _value = value;
            _type = dbType;
        }



        public DBParameter(string name, object value, DbType dbType, ParameterDirection paramDirection)
        {
            _name = name;
            _value = value;
            _type = dbType;
            _paramDirection = paramDirection;
        }

        
        #endregion

        #region "Public Properties"
        /// Gets or sets the name of the parameter.

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Gets or sets the value associated with the parameter.
        /// </summary>
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }



        /// <summary>
        /// Gets or sets the type of the parameter.
        /// </summary>
        public DbType Type
        {
            get { return _type; }
            set { _type = value; }

        }

        

        /// <summary>
        /// Gets or sets the direction of the parameter.
        /// </summary>
        public ParameterDirection ParamDirection
        {
            get
            {
                return _paramDirection;
            }
            set
            {
                _paramDirection = value;
            }
        }
        #endregion
    }
}
