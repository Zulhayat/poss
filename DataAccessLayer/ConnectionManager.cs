﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OracleClient;
using MySql.Data.MySqlClient;
using System.Data.OleDb;
using System.Data.Odbc;

namespace DataAccessLayer
{
   internal class ConnectionManager
    {
       internal IDbConnection GetConnection()
       {
           IDbConnection connection = null;
           string connectionString = Configuration.ConnectionString;
           switch (Configuration.DBProvider.Trim().ToUpper())
           {
               case Common.SQL_SERVER_DB_PROVIDER:
                   connection = new SqlConnection(connectionString);
                   break;
               case Common.MY_SQL_DB_PROVIDER:
                   connection = new MySqlConnection(connectionString);
                   break;
               case Common.ORACLE_DB_PROVIDER:
                   connection = new OracleConnection(connectionString);
                   break;
               case Common.EXCESS_DB_PROVIDER:
                   connection = new OleDbConnection(connectionString);
                   break;
               case Common.ODBC_DB_PROVIDER:
                   connection = new OdbcConnection(connectionString);
                   break;
               case Common.OLE_DB_PROVIDER:
                   connection = new OleDbConnection(connectionString);
                   break;
           }

           try
           {
               connection.Open();
           }
           catch (Exception err)
           {
               throw err;
           }

           return connection;
       }
    }
}
