﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DBParameterCollection
    {
        private List<DBParameter> _parameterCollection = new List<DBParameter>();
        public void Add(DBParameter parameter)
        {
            _parameterCollection.Add(parameter);
        }

        internal List<DBParameter> Parameters
        {
            get
            {
                return _parameterCollection;
            }
        }
    }
}
