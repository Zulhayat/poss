﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MetroFramework.Forms;


namespace POS
{
    public partial class Customer_Main : MetroForm
    {
        BusinessLogicLayer.Customers _customersManagement = new BusinessLogicLayer.Customers();
        public Customer_Main()
        {
            InitializeComponent();
        }

        private void Customer_Main_Load(object sender, EventArgs e)
        {
            _get_all_customers("");
        }


        #region get all customers
        public void _get_all_customers(string value1)
        {
            DataGridViewImageColumn img = new DataGridViewImageColumn();
            img.Width = 150;
            metroGrid3.RowTemplate.Height = 150;
            img.ImageLayout = DataGridViewImageCellLayout.Stretch;
            metroGrid3.Columns.Add("Ac_id", "Sr#"); metroGrid3.Columns.Add("C_id", "ID"); metroGrid3.Columns.Add("Name", "Name");
            metroGrid3.Columns.Add("dob", "Dob"); metroGrid3.Columns.Add("city", "City"); metroGrid3.Columns.Add("phone", "Phone");
            metroGrid3.Columns.Add("mobile", "Mobile"); metroGrid3.Columns.Add("cnic", "CNIC"); metroGrid3.Columns.Add("address", "Address");
            metroGrid3.Columns.Add("gender", "Gender"); metroGrid3.Columns.Add(img);
            DataTable dt = _customersManagement.GetCustomerProfile(value1);
            DataTable dt1 = _customersManagement.GetCountingCustomers();
            if (dt1.Rows.Count > 0)
            {
                lblTotalCustomers.Text = dt1.Rows[0][0].ToString();
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Bitmap bit = null;
                string picture = dt.Rows[i][10].ToString(); string folderpath = ""; string foldername = "";
                if (picture != "")
                {
                    folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    foldername = Path.Combine(folderpath, "Customer Images");
                    bit = new Bitmap(foldername + @"\" + picture);
                }
                else
                {
                    folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    foldername = Path.Combine(folderpath, "Customer Images");
                    bit = new Bitmap(foldername + @"\" + "download.png");
                }
                metroGrid3.Rows.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(), dt.Rows[i][3].ToString(),
                    dt.Rows[i][4].ToString(), dt.Rows[i][5].ToString(), dt.Rows[i][6].ToString(), dt.Rows[i][7].ToString(), dt.Rows[i][8].ToString(),
                    dt.Rows[i][9].ToString(), bit);
            }
        }
        #endregion

        private void button24_Click(object sender, EventArgs e)
        {
            Customers custadd = new Customers("");
            custadd.ShowDialog();

        }

        private void button26_Click(object sender, EventArgs e)
        {
            Customers custedit = new Customers(txtIdHandling.Text);
            custedit.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void Customer_Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            if (txtsearch.Text != "")
            {
                metroGrid3.Rows.Clear(); metroGrid3.Columns.Clear();
                _get_all_customers(txtsearch.Text);
            }
            else {
                metroGrid3.Rows.Clear(); metroGrid3.Columns.Clear();
                _get_all_customers("");
            }
        }

        private void txtsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                metroGrid3.Focus();
            }
        }

        private void metroGrid3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtsearch.Focus();
            }
            if (e.KeyCode == Keys.U && e.Control)
            {
                Customers editcustomer = new Customers(txtIdHandling.Text); 
                editcustomer.ShowDialog();
            }
        }

        private void metroGrid3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIdHandling.Text = metroGrid3.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void metroGrid3_SelectionChanged(object sender, EventArgs e)
        {
            if (metroGrid3.SelectedCells.Count > 0)
            {
                int selectedrowindex = metroGrid3.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = metroGrid3.Rows[selectedrowindex];

                string a = Convert.ToString(selectedRow.Cells[0].Value);

                txtIdHandling.Text = a;
            }
        }

        private void metroGrid3_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            metroGrid3.Rows[e.RowIndex].Height = 50;
        }

        private void metroGrid3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button25_Click(object sender, EventArgs e)
        {
            DialogResult dg = MessageBox.Show("Do you want to delete the Customer","Delete Customer?",MessageBoxButtons.YesNo);
            if (DialogResult.Yes == dg)
            {
                if (_customersManagement.DeleteCustomer(Convert.ToInt32(txtIdHandling.Text)) == true)
                {
                    MessageBox.Show("Customer has deleted", "Success", MessageBoxButtons.OK);
                    _get_all_customers("");
                }
                else
                {
                    MessageBox.Show("Customer is not deleted", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
