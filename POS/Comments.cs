﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Messaging;
using BusinessLogicLayer;

namespace POS
{
    public partial class Comments : MetroForm
    {
        BusinessLogicLayer.Comments _commentManagement = new BusinessLogicLayer.Comments();
        BusinessLogicLayer.Hospital _hospitalManagement = new Hospital();
        BusinessLogicLayer.Customers _customerManagement = new BusinessLogicLayer.Customers();
        Purchase _purchaseManagement = new Purchase();
        public string comments, patient, consultant, designation, hospital, message_focus, receipt;
        public Comments(string _comments, string _patient, string _consultant, string _designation, string _hospital, string _receipt)
        {
            InitializeComponent();
            comments = _comments;
            patient = _patient;
            consultant = _consultant;
            designation = _designation;
            hospital = _hospital;
            receipt = _receipt;
        }

        private void Comments_Load(object sender, EventArgs e)
        {
            if (comments != string.Empty)
            {
                txtComments.Text = comments;
            }
            if (patient != string.Empty)
            {
                txtPatients.Text = patient;
            }
            if (consultant != string.Empty)
            {
                txtConsultants.Text = consultant;
            }
            if (designation != string.Empty)
            {
                txtdesignation.Text = designation;
            }
            if (hospital != string.Empty)
            {
                txthospital.Text = hospital;
            }
            if (receipt != string.Empty)
            {
                txtinvoice.Text = receipt;
            }
            DataTable dt= _hospitalManagement.GetHospitalProfile("");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                txthospital.Items.Add(dt.Rows[i][0].ToString());
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            comments = txtComments.Text.Trim();
            patient = txtPatients.Text.Trim();
            consultant = txtConsultants.Text.Trim();
            designation = txtdesignation.Text.Trim();
            hospital = txthospital.Text.Trim();
            receipt = txtinvoice.Text;
            this.Hide();
            AddUpdateComments();
        }

        public void AddUpdateComments()
        {
            string comments = txtComments.Text.Trim();
            string patient = txtPatients.Text.Trim();
            string consultant = txtConsultants.Text.Trim();
            string designation = txtdesignation.Text.Trim();
            string hospital = txthospital.Text.Trim();
            string message = string.Empty;
            DataTable dt = _hospitalManagement.GetHospitalProfile(hospital);
            DataTable dtcustomer = _customerManagement.GetCustomerId(patient);
            if (_commentManagement.CheckInvoiceExist(receipt) == false)
            {
                if (_commentManagement.AddNewComment(_purchaseManagement.GetHighestInvoiceId(), comments, dtcustomer.Rows[0][0].ToString(), consultant, designation, dt.Rows[0][0].ToString(), txtinvoice.Text))
                {
                    message = MessageManager.GetMessage("38", comments);
                    MetroFramework.MetroMessageBox.Show(this, message, "Comments", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clear();
                }
                else
                {
                    message = MessageManager.GetMessage("36");
                    MetroFramework.MetroMessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (_commentManagement.UpdateComment(_purchaseManagement.GetHighestInvoiceId(), comments, dtcustomer.Rows[0][0].ToString(), consultant, designation, dt.Rows[0][0].ToString(), txtinvoice.Text))
                {
                    message = MessageManager.GetMessage("35", comments);
                    MetroFramework.MetroMessageBox.Show(this, message, "Comments", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clear();
                }
                else
                {
                    message = MessageManager.GetMessage("36");
                    MetroFramework.MetroMessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void Clear() {
            txtComments.Clear();
            txtPatients.Clear();
            txtConsultants.Clear();
            txtdesignation.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            City _brandrecord = new City("", "Hospital");
            _brandrecord.Show();
        }

        private void txtinvoice_TextChanged(object sender, EventArgs e)
        {
            if (_commentManagement.CheckInvoiceExist(txtinvoice.Text) == true)
            {
                DataTable dt = _commentManagement.GetCommentProfile(txtinvoice.Text);
                txtComments.Text = dt.Rows[0][1].ToString();
                txtPatients.Text = dt.Rows[0][2].ToString();
                txtConsultants.Text = dt.Rows[0][3].ToString();
                txtdesignation.Text = dt.Rows[0][4].ToString();
                txthospital.Text = dt.Rows[0][5].ToString();
            }
        }
    }
}
