﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;
namespace POS
{
    public partial class Receipt_Voucher : MetroForm
    {
        BusinessLogicLayer.Supplier _supplierManagement = new Supplier();
        Class_ReceiptVoucher receiptvoucher = new Class_ReceiptVoucher();
        AutoCompleteStringCollection _autocompletestringcollection = new AutoCompleteStringCollection();

        public Receipt_Voucher()
        {
            InitializeComponent();
        }

        private void Receipt_Voucher_Load(object sender, EventArgs e)
        {
            if (rdSupplier.Checked == true)
            {
                txtvoucherID.Text = receiptvoucher.GetHighestVoucherId();
                _autoSupplier();
            }
            else if (rdCustomer.Checked == true)
            {
                txtvoucherID.Text = "";
            }
            else
            {
                MessageBox.Show("Select Account Type for Voucher","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void button46_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rdSupplier_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSupplier.Checked == true)
            {
                txtvoucherID.Text = receiptvoucher.GetHighestVoucherId();
                _autoSupplier();
            }
            else if (rdCustomer.Checked == true)
            {
                txtvoucherID.Text = "";
            }
            else
            {
                MessageBox.Show("Select Account Type for Voucher", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSupplier.Checked == true)
            {
                txtvoucherID.Text = receiptvoucher.GetHighestVoucherId();
            }
            else if (rdCustomer.Checked == true)
            {
                txtvoucherID.Text = "";
            }
            else
            {
                MessageBox.Show("Select Account Type for Voucher", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void _autoSupplier()
        {
            DataTable dt = _supplierManagement.GetSupplierProfile("");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _autocompletestringcollection.Add(dt.Rows[i][2].ToString());
                }
            }
            txtparty.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtparty.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtparty.AutoCompleteCustomSource = _autocompletestringcollection;
        }

        public void _getSupplierVouchers(string userid)
        {
            metroGrid1.DataSource = null;
            DataTable dt = receiptvoucher.GetVoucherProfile(userid);
            metroGrid1.DataSource = dt;
        }

        private void txtparty_TextChanged(object sender, EventArgs e)
        {
            string value = "";
            if (rdSupplier.Checked == true)
            {
                if (txtparty.Text != "")
                {
                    DataTable dt = _supplierManagement.GetSupplierProfile(txtparty.Text);
                    if (dt.Rows.Count > 0)
                    {
                        lblPartyID.Text = dt.Rows[0][0].ToString();
                        _getSupplierVouchers(lblPartyID.Text);
                    }
                }
                else
                {
                    lblPartyID.Text = "";
                    _getSupplierVouchers(lblPartyID.Text);
                }
            }
            else if (rdCustomer.Checked == true)
            {
                if (txtparty.Text != "")
                {
                    lblPartyID.Text = "";
                }
                else
                {
                    lblPartyID.Text = "";
                }
            }
        }

        private void btnParty_Click(object sender, EventArgs e)
        {
            if (rdSupplier.Checked == true)
            {
                Add_Supplier add = new Add_Supplier();
                add.Show();
            }
            else if (rdCustomer.Checked == true)
            {
                Customers add = new Customers("");
                add.Show();
            }
        }

        private void btnComments_Click(object sender, EventArgs e)
        {
            Comments add = new Comments("","","","","","");
            add.Show();
        }

        private void lblPartyID_TextChanged(object sender, EventArgs e)
        {
            string id = string.IsNullOrEmpty(lblPartyID.Text) ? "" : lblPartyID.Text;
            string value = receiptvoucher.Getlatestpayment(id);
            lblBalance.Text = value;
        }

        private void lblAmount_TextChanged(object sender, EventArgs e)
        {
            double dynamicamount = Convert.ToDouble(string.IsNullOrEmpty(lblAmount.Text) ? "0" : lblAmount.Text);
            double totalamount = Convert.ToDouble(string.IsNullOrWhiteSpace(lblBalance.Text) ? "0" : lblBalance.Text);
            double remaining = totalamount - dynamicamount;
            lblRemainings.Text = remaining.ToString();
        }

        private void lblAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtvoucherID.Text != "")
            {
                if (lblPartyID.Text != "")
                {
                    receiptvoucher.CreateNewVoucher(txtvoucherID.Text, DateTime.Now.ToString(), txtCRV.Text, lblPartyID.Text, lblAmount.Text, lblRemainings.Text, "");
                    MessageBox.Show("Receipt has been generated", "Success", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Party ID is missing", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Voucher ID is missing", "Error", MessageBoxButtons.OK);
            }
        }

        private void metroGrid1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int lastvalue = metroGrid1.Rows.Count - 1;
            string value = metroGrid1.Rows[lastvalue].Cells[5].Value.ToString();
            label14.Text = value;
        }
    }
}
