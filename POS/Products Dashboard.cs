﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Drawing;
using BusinessLogicLayer;

namespace POS
{
    public partial class Products : MetroFramework.Forms.MetroForm
    {
        BusinessLogicLayer.Product _productManagement = new Product();
        public Products()
        {
            InitializeComponent();
        }

        string productid = ""; public static Boolean status =false; 
        private void Products_Load(object sender, EventArgs e)
        {
         _get_all_products("");
        }


        #region get all the products
        public void _get_all_products(string value1)
        {
            metroGrid1.Rows.Clear();
            metroGrid1.Columns.Clear();
            DataTable dt = _productManagement.GetProductProfile(value1);
            if (dt.Rows.Count>0)
            {
                DataGridViewImageColumn img = new DataGridViewImageColumn();
                img.Width = 100;
                metroGrid1.RowTemplate.Height = 100;
                img.ImageLayout = DataGridViewImageCellLayout.Stretch;
                metroGrid1.Columns.Add("ID", "ID"); metroGrid1.Columns.Add("Adcode", "Aditional Code"); metroGrid1.Columns.Add("Barcode", "Barcode");
                metroGrid1.Columns.Add("Description", "Description"); metroGrid1.Columns.Add("Brand", "Brand"); metroGrid1.Columns.Add("Category", "Category");
                metroGrid1.Columns.Add("Saleprice", "Sale Price"); metroGrid1.Columns.Add("Cost", "Cost"); metroGrid1.Columns.Add("Location", "Location");
                metroGrid1.Columns.Add("Generic", "Generic");metroGrid1.Columns.Add("unit", "Unit"); metroGrid1.Columns.Add("stocklimit", "Stock Limit"); metroGrid1.Columns.Add("comments", "Comments");
                metroGrid1.Columns.Add("packsize", "Pack Size"); metroGrid1.Columns.Add("disease", "Disease"); metroGrid1.Columns.Add("manufacturer", "Manufacturer");
                metroGrid1.Columns.Add("Supplier", "Supplier"); metroGrid1.Columns.Add("stocklevel", "Stock Level"); metroGrid1.Columns.Add("costpercentage", "Cost Percentage");
                metroGrid1.Columns.Add("salepercentage", "Sale Percentage"); metroGrid1.Columns.Add("ControlDrug", "Control Drug"); metroGrid1.Columns.Add("Isactive", "Active");
                metroGrid1.Columns.Add(img);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Bitmap bit = null;
                    string picture = dt.Rows[i][22].ToString(); string folderpath = ""; string foldername = "";
                    if (picture != "")
                    {
                        folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        foldername = Path.Combine(folderpath, "Product Images");
                        bit = new Bitmap(foldername + @"\" + picture);
                    }
                    else
                    {
                        folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        foldername = Path.Combine(folderpath, "Product Images");
                        bit = new Bitmap(foldername + @"\" + "drug-clipart-black-and-white-9.jpg");
                    }
                    
                    metroGrid1.Rows.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(),dt.Rows[i][3].ToString(), 
                        dt.Rows[i][4].ToString(), dt.Rows[i][5].ToString(),dt.Rows[i][6].ToString(), dt.Rows[i][7].ToString(), dt.Rows[i][8].ToString(), 
                        dt.Rows[i][9].ToString(), dt.Rows[i][10].ToString(),dt.Rows[i][11].ToString(), dt.Rows[i][12].ToString(),
                        dt.Rows[i][13].ToString(),dt.Rows[i][14].ToString(), dt.Rows[i][15].ToString(), dt.Rows[i][16].ToString(), dt.Rows[i][17].ToString(),
                        dt.Rows[i][18].ToString(), dt.Rows[i][19].ToString(), dt.Rows[i][20].ToString(), dt.Rows[i][21].ToString(), bit);
                }
                metroGrid1.Columns[4].Width = 120;
            }
            
            foreach (DataGridViewRow row in metroGrid1.Rows)
            {
                if (Convert.ToString(row.Cells[20].Value) == "False")
                {
                    row.DefaultCellStyle.BackColor = System.Drawing.Color.Pink;
                }
            }
        }
        #endregion
       

        private void button14_Click(object sender, EventArgs e)
        {
            
        }

        private void button16_Click(object sender, EventArgs e)
        {

        }

        private void button46_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Add_Product addproduct = new Add_Product("");
            addproduct.ShowDialog();
        }

        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString() != "")
            {
                productid = metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtproductid.Text != "")
            {
                Add_Product productdefinition = new Add_Product(txtproductid.Text);
                productdefinition.ShowDialog();
            }
            else
            {
                MessageBox.Show("Select Product Item","Digital Era",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtproductid.Text = metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtproductid.Text != "")
            {
                if (DialogResult.Yes == MetroFramework.MetroMessageBox.Show(this,"Do you want to delete the selected Product", "Digital Era", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    _productManagement.DeleteProduct(Convert.ToInt32(txtproductid.Text));
                    MetroFramework.MetroMessageBox.Show(this,"Product has been Deleted", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _get_all_products("");
                }
                else
                {
                    MessageBox.Show("Product is not Selected", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void metroGrid1_SelectionChanged(object sender, EventArgs e)
        {
            if (metroGrid1.Rows.Count > 0)
            {
                int selectedrowindex = metroGrid1.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = metroGrid1.Rows[selectedrowindex];

                string a = Convert.ToString(selectedRow.Cells["Sr#"].Value);

                txtproductid.Text = a;
            }
        }

        private void Products_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            if (txtsearch.Text != "")
            {
                _get_all_products(txtsearch.Text);
            }
            else
            {
                _get_all_products("");
            }
        }

        private void txtsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                metroGrid1.Focus();
            }
        }

        private void metroGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtsearch.Focus();
            }
            if (e.KeyCode == Keys.U && e.Control)
            {
                if (txtproductid.Text != "")
                {
                    Add_Product productdefinition = new Add_Product("");
                    productdefinition.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Select Product Item", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (e.KeyCode == Keys.D && e.Control)
            {
                if (txtproductid.Text != "")
                {
                    if (DialogResult.Yes == MessageBox.Show("Do you want to delete the selected Product", "Digital Era", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        //new Classes.Product.ProductDefinition()._insert_update_delete_Product("ProductDefinition", txtproductid.Text, "", "", "", "", "",
                        //        "", "", true, "", "Delete", "", "", "", "", "", "", "", "", "", "", "", null,status);
                        MessageBox.Show("Product has been Deleted", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _get_all_products("");
                    }
                    else
                    {
                        MessageBox.Show("Product is not Selected", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnDisabledProduct_Click(object sender, EventArgs e)
        {
                if (DialogResult.Yes == MetroFramework.MetroMessageBox.Show(this,"Do you want to change the drug situation", "Digital Era", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {    
                    string value = _productManagement._checkactivationProduct(Convert.ToInt32(txtproductid.Text));
                    if (value == "")
                    {
                        _productManagement._activeDeactiveProduct(Convert.ToInt32(txtproductid.Text), true);
                    }
                    else if (value == "False")
                    {
                        _productManagement._activeDeactiveProduct(Convert.ToInt32(txtproductid.Text), true);
                    }
                    else if (value == "True")
                    {
                        _productManagement._activeDeactiveProduct(Convert.ToInt32(txtproductid.Text), false);
                    }
                    MetroFramework.MetroMessageBox.Show(this,"Drug situation has been Changed", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _get_all_products("");
                }
        }

        private void metroGrid1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            metroGrid1.Rows[e.RowIndex].Height = 50;
        }

        private void picproduct_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
 