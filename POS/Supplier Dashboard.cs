﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class Supplier_Dashboard : MetroForm
    {
        string _message_id = "";
        Supplier _supplierManagement = new Supplier();
        public Supplier_Dashboard()
        {
            InitializeComponent();
        }

        private void Supplier_Dashboard_Load(object sender, EventArgs e)
        {
    _get_supplier("");
        }

        #region get the supplier list
        public void _get_supplier(string value)
        {
            metroGrid3.Rows.Clear();
            metroGrid3.Columns.Clear();
            DataTable dt = _supplierManagement.GetSupplierProfile(value);
            DataGridViewImageColumn img = new DataGridViewImageColumn();
            img.ImageLayout = DataGridViewImageCellLayout.Stretch;
            metroGrid3.Columns.Add("Sid", "Sr#"); metroGrid3.Columns.Add("userid", "ID"); metroGrid3.Columns.Add("Name", "Name");
            metroGrid3.Columns.Add("mobile", "Mobile"); metroGrid3.Columns.Add("Phone", "Phone"); metroGrid3.Columns.Add("NTN", "NTN");
            metroGrid3.Columns.Add("City", "City"); metroGrid3.Columns.Add("ZipCode", "ZipCode"); metroGrid3.Columns.Add("Email", "Email");
            metroGrid3.Columns.Add("Company", "Company"); metroGrid3.Columns.Add("Address", "Address"); metroGrid3.Columns.Add("Comments", "Comments");
            metroGrid3.Columns.Add(img);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    img.Width = 100;
                    metroGrid3.RowTemplate.Height = 100;
                    Bitmap bit = null;
                    string picture = dt.Rows[i][12].ToString(); string folderpath = ""; string foldername = "";
                    if (picture != "")
                    {
                        folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        foldername = Path.Combine(folderpath, "Supplier Images");
                        if (File.Exists(foldername + @"\" + picture))
                        {
                            bit = new Bitmap(foldername + @"\" + picture);
                        }
                        else
                        {
                            folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                            foldername = Path.Combine(folderpath, "Supplier Images");
                            bit = new Bitmap(foldername + @"\" + "51f6fb256629fc755b8870c801092942.png");   
                        }
                    }
                    metroGrid3.Rows.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(),
                        dt.Rows[i][3].ToString(), dt.Rows[i][4].ToString(), dt.Rows[i][5].ToString(), dt.Rows[i][6].ToString(),
                        dt.Rows[i][7].ToString(), dt.Rows[i][8].ToString(), dt.Rows[i][9].ToString(), dt.Rows[i][10].ToString(),
                        dt.Rows[i][11].ToString(), bit);
                }
            }
            
            this.metroGrid3.DefaultCellStyle.Font = new Font("Tahoma", 11);
            this.metroGrid3.DefaultCellStyle.ForeColor = Color.Black;
        }
        #endregion


        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void metroGrid3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button24_Click(object sender, EventArgs e)
        {
            Supplier_Form form = new Supplier_Form("");
            form.ShowDialog();
        }

        private void metroTextBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void metroTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (metroTextBox2.Text != "")
            {
                _get_supplier(metroTextBox2.Text);
            }
            else
            {
                _get_supplier("");
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            if (_message_id != "")
            {
                Supplier_Form _supplier_Add = new Supplier_Form(_message_id);
                _supplier_Add.Show();
            }
            else
            {
                MessageBox.Show("Select record for editing");
            }
        }

        private void metroGrid3_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            _message_id = metroGrid3.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void button25_Click(object sender, EventArgs e)
        {
            if (_message_id != "")
            {
                DialogResult dialog = MessageBox.Show("Do you want to delete the supplier?","Delete Supplier",MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (DialogResult.Yes == dialog)
                {
                    _supplierManagement.DeleteSupplier(Convert.ToInt32(_message_id));
                    MessageBox.Show("Record has deleted", "Success", MessageBoxButtons.OK);
                    _get_supplier("");
                }
            }
            else
            {
                MessageBox.Show("Select your record for deleting purpose","Error",MessageBoxButtons.OK);
            }
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
