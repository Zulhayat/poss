﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Forms;
using System.Windows.Forms;
using BusinessLogicLayer;
using Messaging;

namespace POS
{
    public partial class City : MetroForm
    {
        BusinessLogicLayer.City _cityManagement = new BusinessLogicLayer.City();
        BusinessLogicLayer.Hospital _hospitalManagement = new Hospital();
        BusinessLogicLayer.Brand _brandManagement = new Brand();
        BusinessLogicLayer.Category _categoryManagement = new BusinessLogicLayer.Category();
        BusinessLogicLayer.Manufacturer _manManagement = new Manufacturer();
        BusinessLogicLayer.Supplier _supplierManagement = new Supplier();
        Generic _genericManagement = new Generic();
        Unit _unitManagement = new Unit();
        string city_name = string.Empty;
        string _form_text = string.Empty;

        public City(string _city_name, string form_text)
        {
            InitializeComponent();
            city_name = _city_name;
            this.Text = form_text;
            _form_text = form_text;
        }

        private void Add_City_Load(object sender, EventArgs e)
        {
            txtautomaticcode.Enabled = false;
            if (_form_text == "City")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _cityManagement.GetHighestID().ToString();
                    label2.Text = "City";
                }
                else
                {
                    DataTable dt = _cityManagement.GetCityProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
            if (_form_text == "Hospital")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _hospitalManagement.GetHighestID().ToString();
                    label2.Text = "Hospital";
                }
                else
                {
                    DataTable dt = _hospitalManagement.GetHospitalProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
            if (_form_text == "Manufacturer")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _manManagement.GetHighestID().ToString();
                    label2.Text = "Manufacturer";
                }
                else
                {
                    DataTable dt = _manManagement.GetManProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
            if (_form_text == "Unit")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _unitManagement.GetHighestID().ToString();
                    label2.Text = "Unit";
                }
                else
                {
                    DataTable dt = _unitManagement.GetUnitProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
            if (_form_text == "Brand")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _brandManagement.GetHighestID().ToString();
                    label2.Text = "Brand";
                }
                else
                {
                    DataTable dt = _brandManagement.GetBrandProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
            if (_form_text == "Category")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _categoryManagement.GetHighestID().ToString();
                    label2.Text = "Category";
                }
                else
                {
                    DataTable dt = _categoryManagement.GetCategoryProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }

            if (_form_text == "Supplier")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _supplierManagement.GetHighestSupplierId().ToString();
                    label2.Text = "Supplier";
                }
                else
                {
                    DataTable dt = _supplierManagement.GetSupplierProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][2].ToString();
                    }
                }
            }
            if (_form_text == "Generic")
            {
                if (city_name == "")
                {
                    txtautomaticcode.Text = _genericManagement.GetHighestID().ToString();
                    label2.Text = "Generic";
                }
                else
                {
                    DataTable dt = _genericManagement.GetGenericProfile(city_name);
                    if (dt.Rows.Count > 0)
                    {
                        txtautomaticcode.Text = dt.Rows[0][0].ToString();
                        txtCity.Text = dt.Rows[0][1].ToString();
                    }
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_form_text == "City")
            {
                _addnewcity();
            }
            else if (_form_text == "Brand")
            {
                _addnewbrand();
            }
            else if (_form_text == "Category")
            {
                _addnewcategory();
            }
            else if (_form_text == "Unit")
            {
                _addnewUnit();
            }
            else if (_form_text == "Manufacturer")
            {
                _addnewMan();
            }
            else if (_form_text == "Supplier")
            {
                _addnewSupplier();
            }
            else if (_form_text == "Generic")
            {
                _addnewGeneric();
            }
            else if (_form_text == "Hospital")
            {
                _addnewHospital();
            }
            _Clear();
        }

        #region adding new generic
        public void _addnewGeneric()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _genericManagement.UpdateGenericDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Generic", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _genericManagement.CreateNewGeneric(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Generic", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Generic Detail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Generic ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region add new hospital
        public void _addnewHospital()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _hospitalManagement.UpdateHospitalDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Hospital", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _hospitalManagement.CreateNewHospital(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Hospital", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Hospital Detail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Hospital ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new Supplier
        public void _addnewSupplier()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _supplierManagement.UpdateSupplierDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Supplier", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _supplierManagement.CreateNewSupplier(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Supplier", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Supplier Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Supplier ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new Manufacturer
        public void _addnewMan()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _manManagement.UpdateManDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Manufacturer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _manManagement.CreateNewMan(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Manufacturer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Manufacturer Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Manufacturer ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new Unit
        public void _addnewUnit()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _unitManagement.UpdateUnitDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Unit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _unitManagement.CreateNewUnit(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Unit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtCity.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Unit Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Brand ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new brand
        public void _addnewbrand()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _brandManagement.UpdateBrandDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Brand", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _brandManagement.CreateNewBrand(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Brand", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Brand Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Brand ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new category
        public void _addnewcategory()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _categoryManagement.UpdateCategoryDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update Category", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _categoryManagement.CreateNewCategory(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New Category", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "Category Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Brand ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region adding new city
        public void _addnewcity()
        {
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                if (txtCity.Text != string.Empty)
                {
                    if (city_name != string.Empty)
                    {
                        _cityManagement.UpdateCityDetails(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("4", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Update City", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                    else
                    {
                        _cityManagement.CreateNewCity(txtautomaticcode.Text, txtCity.Text);
                        message = MessageManager.GetMessage("1", txtCity.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Add New City", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Hide();
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", txtautomaticcode.Text);
                    MetroFramework.MetroMessageBox.Show(this, message, "City Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "City ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void City_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void txtautomaticcode_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void txtautomaticcode_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
        }

        public void _Clear()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
        }
    }
}
