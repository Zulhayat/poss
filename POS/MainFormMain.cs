﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace POS
{
    public partial class MainFormMain : MetroForm
    {
        string _message;
        public MainFormMain(string message)
        {
            InitializeComponent();
            _message = message;
        }

        private void MainFormMain_Load(object sender, EventArgs e)
        {
            if (_message == "Supplier")
            {
                Supplier_Dashboard Dashboard = new Supplier_Dashboard();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
            //    _viewChildForm(Dashboard);

            }
            else if (_message == "Customer")
            {
                Customer_Main Dashboard = new Customer_Main();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
                //_viewChildForm(Dashboard);
            }
            else if (_message == "Products")
            {
                Products Dashboard = new Products();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
                //_viewChildForm(Dashboard);
            }
            else if (_message == "Purchasing")
            {
                Purchases_Dashboard Dashboard = new Purchases_Dashboard();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
                //_viewChildForm(Dashboard);
            }
            else if (_message == "Selling")
            {
                Sales Dashboard = new Sales();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
                //_viewChildForm(Dashboard);
            }
            else if (_message == "Accounts")
            {
                Vouchers Dashboard = new Vouchers();
                panel1.Controls.Clear();
                Dashboard.TopLevel = false;
                panel1.Controls.Add(Dashboard);
                Dashboard.Show();
                //_viewChildForm(Dashboard);
            }
            else if (_message == "Settings")
            {
                 
            }
            else if (_message == "Users")
            {
                
            }
            else if (_message == "Reports")
            {
                
            }
        }
        #region check that form is already activated or not
        public bool isFormactivated(Form form)
        {
            bool IsOpened = false;
            if (MdiChildren.Count() > 0)
            {
                foreach (var Item in MdiChildren)
                {
                    if (form.Name == Item.Name)
                    {
                        IsOpened = true;
                    }
                }
            }
            return IsOpened;
        }
        #endregion


        public void _viewChildForm(Form _form)
        {
            _form.MdiParent = this;
            _form.Size = _form.MdiParent.ClientSize;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            _form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            _form.Dock = DockStyle.Fill;
            
            _form.Show();
        }
        private void button32_Click(object sender, EventArgs e)
        {
            Sales saleDashboard = new Sales();
            panel1.Controls.Clear();
            saleDashboard.TopLevel = false;
            panel1.Controls.Add(saleDashboard);
            saleDashboard.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Customer_Main customerDashboard = new Customer_Main();
            panel1.Controls.Clear();
            customerDashboard.TopLevel = false;
            panel1.Controls.Add(customerDashboard);
            customerDashboard.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Products productsDashboard = new Products();
            panel1.Controls.Clear();
            productsDashboard.TopLevel = false;
            panel1.Controls.Add(productsDashboard);
            productsDashboard.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Purchases_Dashboard purchaseDashboard = new Purchases_Dashboard();
            panel1.Controls.Clear();
            purchaseDashboard.TopLevel = false;
            panel1.Controls.Add(purchaseDashboard);
            purchaseDashboard.Show();
            //    _viewChildForm(Dashboard);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Supplier_Dashboard Dashboard = new Supplier_Dashboard();
            panel1.Controls.Clear();
            Dashboard.TopLevel = false;
            panel1.Controls.Add(Dashboard);
            Dashboard.Show();
            //    _viewChildForm(Dashboard);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vouchers Dashboard = new Vouchers();
            panel1.Controls.Clear();
            Dashboard.TopLevel = false;
            panel1.Controls.Add(Dashboard);
            Dashboard.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            MainForm main = new MainForm();
            main.Show();
        }
    }
}
