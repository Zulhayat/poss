﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
using BusinessLogicLayer;

namespace POS
{
    public partial class Form1 : Form
    {
        backup _backupManagement = new backup();
        ExtendedButtons extendedbutton = new ExtendedButtons();
        CsUsers csusers = new CsUsers();
        public string username = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void chkHide_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHide.Checked)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (username != "")
            {
                if (txtPassword.Text != "")
                {
                    bool value = csusers.CheckUserLogin(username, txtPassword.Text);
                    if (value == true)
                    {
                        MetroFramework.MetroMessageBox.Show(this,"Login Successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this.Hide();
                        MainForm main = new MainForm();
                        main.Show();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Login unsuccessful", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this,"Give password for aurthentication", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPassword.Focus();
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this,"Select username in the given list", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;  //in milliseconds

            timer1.Tick += new EventHandler(this.timer1_Tick);

            //start timer when form loads
            timer1.Start();  //this will use t_Tick() method
            _get_buttons_name();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label4.Text = DateTime.Now.ToString("hh:mm:ss tt"); //time;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn1.Text;
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn2.Text;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn3.Text;
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn4.Text;
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn5.Text;
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn6.Text;
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn7.Text;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn8.Text;
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn9.Text;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtPassword.Text += btn0.Text;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPassword.Clear();
        }

        public void _get_buttons_name()
        {
            DataTable dt = csusers.GetUserProfile("");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ExtendedButtons b = new ExtendedButtons(); //with ExtendedButton this time
                b.Text = dt.Rows[i][0].ToString();
                b.Height = 50;
                b.Width = 200;
                b.ForeColor = Color.White;
                b._myval = dt.Rows[i][0].ToString();//this asigns the fruit name to the extended button
                b.Click += new EventHandler(onButtonClick);
                b.Location = new Point(670, 190 + (i * 50));
                this.Controls.Add(b);
            }
        }

        private void onButtonClick(object sender, EventArgs e)
        {
            username = ((ExtendedButtons)sender)._myval.ToString();
        }

        private void btnDB_Click(object sender, EventArgs e)
        {
            //if (txtDirectory.Text == "" && txtbackupname.Text != "")
            //{
            //    MessageBox.Show("Please give the folder path before making backup...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //else
            //{
            //    DialogResult dialogresult = MessageBox.Show("Do you realy want to Make Backup", "Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //    if (dialogresult == DialogResult.Yes)
            //    {
            //        _backupManagement._make_backup(txtDirectory.Text, lbldbnamedot.Text, txtbackupname.Text);
            //    }
            //}
        }
    }
}
