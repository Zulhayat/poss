﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
using BusinessLogicLayer;

namespace POS
{
    public partial class Add_Supplier : MetroForm
    {
        Supplier sp = new Supplier();
        BusinessLogicLayer.City city = new BusinessLogicLayer.City();
        public string message_focus = "";
        public Add_Supplier()
        {
            InitializeComponent();
        }

        private void Add_Supplier_Load(object sender, EventArgs e)
        {
            _get_supplier("GetHighID");
        }

       

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }


        #region all click events
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listitems_Click(object sender, EventArgs e)
        {
            if (message_focus == "City")
            {
                if (listitems.Visible == true)
                {
                    foreach (object selectedItem in listitems.SelectedItems)
                    {
                        DataRowView dr = (DataRowView)selectedItem;
                        String result = dr["City"].ToString();
                        txtoldvalue.Text = result;
                    }
                }
            }

            if (message_focus == "Area")
            {
                if (listitems.Visible == true)
                {
                    foreach (object selectedItem in listitems.SelectedItems)
                    {
                        DataRowView dr = (DataRowView)selectedItem;
                        String result = dr["Area"].ToString();
                        txtoldvalue.Text = result;
                    }
                }
            }
        }

        private void listitems_DoubleClick(object sender, EventArgs e)
        {
            if (message_focus == "City")
            {
                if (listitems.Visible == true)
                {
                    foreach (object selectedItem in listitems.SelectedItems)
                    {
                        DataRowView dr = (DataRowView)selectedItem;
                        String result = dr["City"].ToString();
                        txtsuppliercity.Text = result;
                    }
                }
            }
            if (message_focus == "Area")
            {
                if (listitems.Visible == true)
                {
                    foreach (object selectedItem in listitems.SelectedItems)
                    {
                        DataRowView dr = (DataRowView)selectedItem;
                        String result = dr["Area"].ToString();
                        txtsupplierarea.Text = result;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (message_focus == "City")
            {
                City _cityrecord = new City("", "City");
                _cityrecord.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (message_focus == "City")
            {
                _Execute_Non_Query("City", "Update");
                SqlDataReader sdr = null;// new Classes.Supplier.SupplierClass()._get_high_id("City");
                DataTable dt = new DataTable();
                dt.Load(sdr);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                listitems.DataSource = ds.Tables[0];
            }
            if (message_focus == "Area")
            {
                _Execute_Non_Query("Area", "Update");
                SqlDataReader sdr = null;// new Classes.Supplier.SupplierClass()._get_high_id("City");
                DataTable dt = new DataTable();
                dt.Load(sdr);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                listitems.DataSource = ds.Tables[0];
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (message_focus == "City")
            {
                _Execute_Non_Query("City", "Delete");
                SqlDataReader sdr = null;// new Classes.Supplier.SupplierClass()._get_high_id("City");
                DataTable dt = new DataTable();
                dt.Load(sdr);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                listitems.DataSource = ds.Tables[0];
            }
            if (message_focus == "Area")
            {
                _Execute_Non_Query("Area", "Delete");
                SqlDataReader sdr = null;// new Classes.Supplier.SupplierClass()._get_high_id("City");
                DataTable dt = new DataTable();
                dt.Load(sdr);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                listitems.DataSource = ds.Tables[0];
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            _Execute_Non_Query("Supplier", "Insert");
            _get_supplier("GetHighID");
            MessageBox.Show("Supplier Record has been Added","Digital Era",MessageBoxButtons.OK,MessageBoxIcon.Information);
            _clear_textboxes();
            txtsupplierid.Text = sp.GetHighestSupplierId();
        }
        #endregion

        #region clear textbox
        public void _clear_textboxes()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
            _get_supplier("GetHighID");
        }
        #endregion

        #region save the record
        public void _Execute_Non_Query(string message, string Operation)
        {   
            if (txtsupplierid.Text != "")
            {

                ////sp.CreateNewSupplier(txtSuppliername.Text
                ////    , txtsuppliermobile.Text, txtsupplierphone.Text, txtsupplieremail.Text, txtsupplierntn.Text,
                ////    txtsuppliercity.Text, txtsupplierarea.Text, txtsuppliercomment.Text, message, Operation, txtoldvalue.Text);
            }
            else
            {
                MessageBox.Show("Supplier code does not Exist", "Digital Era", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region get the highest id of the supplier record
        public void _get_supplier(string message)
        {
            txtsupplierid.Text = sp.GetHighestSupplierId();
            
        }
        #endregion

        #region all key down events
        private void Add_Supplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                _Execute_Non_Query("Supplier", "Insert");
                _get_supplier("GetHightID");
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void txtsuppliercity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (message_focus == "City")
                {
                    if (listitems.Visible == true)
                    {
                        foreach (object selectedItem in listitems.SelectedItems)
                        {
                            DataRowView dr = (DataRowView)selectedItem;
                            String result = dr["City"].ToString();
                            txtsuppliercity.Text = result;
                        }
                    }
                }
                buttonPanels.Visible = false; listitems.Visible = false;
            }
        }

        private void txtsupplierarea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (message_focus == "Area")
                {
                    if (listitems.Visible == true)
                    {
                        foreach (object selectedItem in listitems.SelectedItems)
                        {
                            DataRowView dr = (DataRowView)selectedItem;
                            String result = dr["Area"].ToString();
                            txtsupplierarea.Text = result;
                        }
                    }
                }
                buttonPanels.Visible = false; listitems.Visible = false;
            }
        }

        #endregion

        #region textchanged events
        private void txtsuppliercity_TextChanged(object sender, EventArgs e)
        {
            message_focus = "City";
            List<string> items = new List<string>();
            if (txtsuppliercity.Text != "")
            {
                DataSet ds = new DataSet();
                DataTable dt = city.GetCityProfile(txtsuppliercity.Text);
                listitems.DataSource = dt;
                listitems.DisplayMember = "City";
                buttonPanels.Location = new Point(103, 180);
                buttonPanels.Show();
                listitems.Location = new Point(103, 205);
                listitems.Show();
                buttonPanels.BringToFront();
                listitems.BringToFront();

                int index = listitems.FindString(this.txtsuppliercity.Text);
                if (0 <= index)
                {
                    listitems.SelectedIndex = index;
                }
            }
            else
            {
                buttonPanels.Visible = false;
                listitems.Visible = false;
            }
        }

        private void txtsupplierarea_TextChanged(object sender, EventArgs e)
        {
            
        }
        #endregion 

        #region leave events
        private void txtsuppliercity_Leave(object sender, EventArgs e)
        {

        }
        private void txtsupplierarea_Leave(object sender, EventArgs e)
        {

        }

        #endregion

        private void txtsupplierid_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void txtsupplierid_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
        }

        

        
    }
}
