﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class Cash_Form : MetroForm
    {
        public string receiptno = "", status = "", total = "", discount = "", paycash = "", change = "", comments = "", subtotal = "";
        string cleantotaltopay = ""; double discountmath = 0.0; 
        public Cash_Form(string _receiptno, string _status, string _total)
        {
            InitializeComponent();
            receiptno = _receiptno;
            status = _status;
            total = _total;
        }

        private void Cash_Form_Load(object sender, EventArgs e)
        {
            if (total != string.Empty)
            {
                lblTotalToPay.Text = total;
                calculateDiscount();
            }
        }

        private void metroToggle1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
             discount = txtDiscount.Text;
            paycash = txtCash.Text;
            change = lblFinalAmount.Text;
            comments = txtComments.Text;
            subtotal = lblTotalToPay.Text;
            status = "OK";
        }

        private void metroToggle3_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            PayCash();
        }

        public void calculateDiscount()
        {
            //Total Amount to Pay
            string totaltopay = lblTotalToPay.Text;
            char[] chartotalpay = { 'R', 's', '.', ' ' };
            cleantotaltopay = totaltopay.Trim(chartotalpay);

            //get 10 percent discount 
            char[] chardiscount = {'R','S','.',' ' };
            string cleandiscount = txtDiscount.Text.Trim(chardiscount);
            discountmath = (Convert.ToDouble(cleandiscount) / 100 * Convert.ToDouble(cleantotaltopay));
            txtDiscount.Text = "RS.  " + discountmath.ToString();
            lblFinalAmount.Text = (Convert.ToDouble(cleantotaltopay) - discountmath).ToString();
        }

        public void PayCash()
        {
            string cashpaid = txtCash.Text;
            char[] charscashpaid = { 'R', 's', '.', ' ' };
            string cleancashpaid = cashpaid.Trim(charscashpaid);

            string totalremain = lblTotalToPay.Text;
            char[] charstotalremain = { 'R', 's', '.', ' ' };
            string cleantotalremain = cashpaid.Trim(charstotalremain);

            if (cleancashpaid == "")
            {
                cleancashpaid = "0";
            }

            if (cleantotaltopay == "")
            {
                cleantotaltopay = "0";
            }

            if (cleantotalremain == "")
            {
                cleantotalremain = "0";
            }

            cleantotalremain = (-Convert.ToInt32(cleancashpaid) - Convert.ToInt32(discountmath) + Convert.ToInt32(cleantotaltopay)).ToString();

            lblFinalAmount.Text = "Rs.  " + cleantotalremain;
        }
    }
}
