﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace POS
{
    public partial class Vouchers : MetroForm
    {
        BusinessLogicLayer.Vouchers _voucherManagement = new BusinessLogicLayer.Vouchers();
        public Vouchers()
        {
            InitializeComponent();
        }

        private void Vouchers_Load(object sender, EventArgs e)
        {
            getvouchers("");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button30_Click(object sender, EventArgs e)
        {

        }

        public void getvouchers(string search)
        {
            DataTable dt = _voucherManagement.GetVouchersReceipt(search);
            metroGrid4.DataSource = dt;
        }

        public void getvouchers(string from, string to)
        {
            DataTable dt = _voucherManagement.GetVouchersReceiptbetweenDates(from, to);
            metroGrid4.DataSource = dt;
        }

        private void button29_Click(object sender, EventArgs e)
        {

        }

        private void dtto_ValueChanged(object sender, EventArgs e)
        {
            if (dtto.Value > dtfrom.Value)
            {
                getvouchers(dtto.Text, dtfrom.Text);
            }
            else
            {
                MessageBox.Show("Date format set according to rule","Error");
            }
        }
    }
}
