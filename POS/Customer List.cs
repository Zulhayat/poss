﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class Customer_List : MetroForm
    {
        public string selected_id ="";
        BusinessLogicLayer.Customers _customerManagement = new BusinessLogicLayer.Customers();
        public Customer_List()
        {
            InitializeComponent();
        }

        private void Customer_List_Load(object sender, EventArgs e)
        {
            DataTable dt = _customerManagement.GetCustomerProfile("");
            metroGrid1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Customers custnew = new Customers("");
            custnew.Show();
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBox1_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = _customerManagement.GetCustomerProfile(metroTextBox1.Text);
            metroGrid1.DataSource = dt;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Customers _customerManagement = new Customers(selected_id);
            _customerManagement.Show();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selected_id = metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void metroGrid1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            selected_id = metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString();
            Customers _customerManagement = new Customers(selected_id);
            _customerManagement.Show();
        }
    }
}
