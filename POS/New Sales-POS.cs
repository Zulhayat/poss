﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class New_Sales_POS : MetroForm
    {
        string comments = "", patient = "", consultant = "", designation = "", hospital = "";
        string rcomments, rpatient, roncsultant, rdesignation, rhospital, receipt; string edit_ID = "";
        BusinessLogicLayer.Customers _customerManagement = new BusinessLogicLayer.Customers();
        BusinessLogicLayer.Product _productManagement = new BusinessLogicLayer.Product();
        BusinessLogicLayer.Sales _saleManagement = new BusinessLogicLayer.Sales();
        Comments _comments = new Comments("","","","","","");

        public static string ProductValue = string.Empty;
        public static string PriceValue = string.Empty;
        public static string productID = string.Empty;
        public static string saleprice = string.Empty;
        public static string message = string.Empty;
        AutoCompleteStringCollection _autocompletestringcollection = new AutoCompleteStringCollection();

        public New_Sales_POS(string _edit_ID)
        {
            InitializeComponent();
            edit_ID = _edit_ID;
        }

        private void New_Sales_POS_Load(object sender, EventArgs e)
        {
            _getHighestID(edit_ID);
            _autoCustomer();
        }

        private void button46_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Customer_List custlist = new Customer_List();
            custlist.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            char[] charstoRemove = { 'Q', 't', 'y', 'R', 's', 'S', '.', ' ' };
            string subtotal = lblTotal.Text.Trim(charstoRemove), tax = "", discount = lblDiscount.Text.Trim(charstoRemove), freight = "", finaltotal = lblTotal.Text.Trim(charstoRemove);
            if (!string.IsNullOrWhiteSpace(lblTotal.Text))
            {
                using (Discount _discount = new Discount(subtotal, tax, discount, freight, finaltotal,lblReceiptNo.Text))
                {
                    _discount.ShowDialog();
                    lblDiscount.Text = _discount._discount;
                    lblTotal.Text = _discount._finaltotal;
                }
            }
            else
            {
                MessageBox.Show("Please select some Product first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void button32_Click(object sender, EventArgs e)
        {
            if (rcomments == "")
            {
                using (Comments _comments = new Comments(comments, txtCustomer.Text, consultant, designation, hospital,lblReceiptNo.Text))
                {
                    _comments.ShowDialog();
                    rcomments = _comments.comments;
                    rpatient = _comments.patient;
                    txtCustomer.Text = _comments.patient;
                    roncsultant = _comments.consultant;
                    rdesignation = _comments.designation;
                    rhospital = _comments.hospital;
                    receipt = _comments.receipt;
                }
            }
            else {
                using (Comments _comments = new Comments(rcomments, txtCustomer.Text, roncsultant, rdesignation, rhospital,lblReceiptNo.Text))
                {
                    _comments.ShowDialog();
                    rcomments = _comments.comments;
                    rpatient = _comments.patient;
                    roncsultant = _comments.consultant;
                    rdesignation = _comments.designation;
                    rhospital = _comments.hospital;
                    receipt = _comments.receipt;
                }
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            txtquantity.Select(txtquantity.Text.Length,6);
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            txtRs.Select(txtRs.Text.Length, 5);
        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            txtTotal.Select(txtTotal.Text.Length, 3);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string quantity = txtquantity.Text;
            char[] charsqtytoTrim = {'Q','t','y','.',' ' };
            string cleanquantity = quantity.Trim(charsqtytoTrim);

            string price = txtRs.Text;
            char[] charspricetoTrim = {'R','s','.',' '};
            string cleanprice = price.Trim(charspricetoTrim);

            string total = txtTotal.Text;
            char[] charstotaltoTrim = { 'Σ', ' ' };
            string cleantotalprice = total.Trim(charstotaltoTrim);

            if (cleantotalprice == "")
            {
                cleantotalprice = "0";
            }

            if (cleanprice == "")
            {
                cleanprice = "0";
            }

            if (cleanquantity == "")
            {
                cleanquantity = "0";
            }

            cleantotalprice = (Convert.ToInt32(cleanprice) * Convert.ToInt32(cleanquantity)).ToString();

            txtTotal.Text = "Σ  " + cleantotalprice;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            string quantity = txtquantity.Text;
            char[] charsqtytoTrim = { 'Q', 't', 'y', '.' };
            string cleanquantity = quantity.Trim(charsqtytoTrim);

            string price = txtRs.Text;
            char[] charspricetoTrim = { 'R', 's', '.' };
            string cleanprice = price.Trim(charspricetoTrim);

            string total = txtTotal.Text;
            char[] charstotaltoTrim = { 'Σ' };
            string cleantotalprice = total.Trim(charstotaltoTrim);

            if (cleantotalprice == "")
            {
                cleantotalprice = "0";
            }

            if (cleanprice == "  ")
            {
                cleanprice = "0";
            }

            if (cleanquantity == "  ")
            {
                cleanquantity = "0";
            }

            cleantotalprice = (Convert.ToInt32(cleanprice) * Convert.ToInt32(cleanquantity)).ToString();

            txtTotal.Text = "Σ  " + cleantotalprice;
        }

        public void _cleartextboxes()
        {
            txtquantity.Text = "Qty.  ";
            txtRs.Text = "Rs.  "; txtProduct.Text = "";
            txtTotal.Text = "Σ  ";
            txtProduct.Focus();
            lblDiscount.Text = ""; lblTotal.Text = ""; txtCustomer.Text = ""; lblqty.Text = ""; 
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (txtProduct.Text != "")
            {
                metroGrid1.Rows.Add(_productManagement.GetProductId(txtProduct.Text), txtProduct.Text, txtquantity.Text, txtRs.Text, "", txtTotal.Text);
                _get_sum_qty_total(); _cleartextboxes(); 
            }
            else
            {
                MessageBox.Show("Choose product first","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        public void _get_sum_qty_total()
        {
            char[] cleancharacters = { 'Q', 't', 'y', '.', ' ', 'Σ' };
            int sum = 0, qty = 0; ;
            for (int i = 0; i < metroGrid1.Rows.Count - 1; ++i)
            {
                string total = metroGrid1.Rows[i].Cells[5].Value.ToString();
                string cleanquantity = total.Trim(cleancharacters);
                sum += Convert.ToInt32(cleanquantity);

                string totalqty = metroGrid1.Rows[i].Cells[2].Value.ToString();
                string cleanqty = totalqty.Trim(cleancharacters);
                qty += Convert.ToInt32(cleanqty);
            }
            lblTotal.Text = sum.ToString();
            lblqty.Text = qty.ToString();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            _get_productlist();
            _check_brand_category_barcode();
            _get_Image(txtProduct.Text);
        }

        public void _get_Image(string description)
        {
            string picture = "";
            if (description != "")
            {
                DataTable dt = _productManagement.GetProductProfile(description);
                if (dt.Rows.Count > 0)
                {
                    picture = dt.Rows[0][21].ToString();
                }
                string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string foldername = Path.Combine(folderpath, "Product Images");
                string searchpath = foldername + @"\" + picture;
                if (File.Exists(searchpath))
                {
                    pictureBox1.Image = Image.FromFile(searchpath);
                }
                else
                {
                    pictureBox1.Image = null;
                }
            }
            else
            {
                pictureBox1.Image = null;
            }
        }

        public void _check_brand_category_barcode()
        {
            if (chkBrand.Checked == true)
            {
                metroGrid2.Columns[4].Visible = true;
                metroGrid2.Columns[5].Visible = true;
            }
            else
            {
                metroGrid2.Columns[4].Visible = false;
                metroGrid2.Columns[5].Visible = false;
            }
            if (chkShowBarCode.Checked == true)
            {
                metroGrid2.Columns[2].Visible = true;
            }
            else
            {
                metroGrid2.Columns[2].Visible = false;
            }
        }

        public void _get_productlist()
        {
            if (txtProduct.Text != string.Empty)
            {
                panel4.Visible = true;
                panel4.Location = new Point(4, 92);
                metroGrid2.DataSource = _productManagement.GetProductProfile(txtProduct.Text);
            }
            else {
                panel4.Visible = false;
            }
        }

        private void txtProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                metroGrid2.Focus();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtRs.Text = "Rs.  "+PriceValue;
                //txtProductCode.Text = productID;
                txtquantity.Focus();
                panel4.Visible = false;
            }
        }

        private void metroGrid2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && (metroGrid2.SelectedCells[0].RowIndex == 0))
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtRs.Text = "Rs.  "+PriceValue;
                //txtProductCode.Text = productID;
                txtquantity.Focus();
                panel4.Visible = false;
            }
            if (e.KeyCode == Keys.Enter)
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtRs.Text = "Rs.  "+PriceValue;
                // = productID;
                txtquantity.Focus();
                panel4.Visible = false;

            }
        }

        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            char[] charstotaltoTrim = { 'Σ', ' ' };
            if (metroGrid1.Rows.Count - 1 > 0)
            {
                if (e.ColumnIndex == 6)// created column index (delete button)
                {
                    metroGrid1.Rows.Remove(metroGrid1.Rows[e.RowIndex]);
                    int sum = 0;
                    for (int i = 0; i < metroGrid1.Rows.Count-1; ++i)
                    {
                        string total = metroGrid1.Rows[i].Cells[5].Value.ToString();
                        string cleanquantity = total.Trim(charstotaltoTrim);
                        sum += Convert.ToInt32(cleanquantity);
                    }
                    lblTotal.Text = sum.ToString();
                }
            }
        }

        private void metroGrid1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            if (e.ColumnIndex == 6)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                var w = Properties.Resources.Delete.Width;
                var h = Properties.Resources.Delete.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.DrawImage(Properties.Resources.Delete, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }

        private void txtquantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRs.Focus();
            }
            if (e.KeyCode == Keys.Back)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void txtRs_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEnter.Focus();
            }
        }

        public void _getHighestID(string givenId)
        {
            if (givenId == "")
            {
                string ID = _saleManagement.GetHighestInvoiceId();
                lblReceiptNo.Text = ID;
            }
            else
            {
                lblReceiptNo.Text = givenId;
            }
        }

        private void txtCustomer_TextChanged(object sender, EventArgs e)
        {
       
        }

        public void _autoCustomer()
        {
            DataTable dt = _customerManagement.GetCustomerProfile("");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _autocompletestringcollection.Add(dt.Rows[i][2].ToString());
                }
            }
            txtCustomer.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtCustomer.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtCustomer.AutoCompleteCustomSource = _autocompletestringcollection;
        }

        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

        private void txtquantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            onlynumwithsinglepoint(sender, e);
        }

        private void txtRs_KeyPress(object sender, KeyPressEventArgs e)
        {
            onlynumwithsinglepoint(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string status = ""; string cashpaid = ""; string change = ""; string subtotal = "";
            Cash_Form cash = new Cash_Form(lblReceiptNo.Text, status, lblTotal.Text);
            {
                cash.ShowDialog();
            }

            //Discount things
            lblDiscount.Text = cash.discount;
            cashpaid = cash.paycash;
            change = cash.change;
            subtotal = cash.subtotal;
            //---------------
            string savedcustomerID = "";
            DataTable dt = _customerManagement.GetCustomerId(txtCustomer.Text);
            if (dt.Rows.Count > 0)
            {
                savedcustomerID = dt.Rows[0][0].ToString();
            }
            bool sale_exist = _saleManagement.CheckInvoiceExist(lblReceiptNo.Text);
            if (sale_exist != true)
            {
                var date = DateTime.Now; var getdate = date.Date;
                if (_customerManagement.CheckCustomerExist(savedcustomerID) == true)
                {
                    _saleManagement.SaveInvoiceMain(lblReceiptNo.Text, savedcustomerID, lblqty.Text, lblDiscount.Text, lblTotal.Text, getdate, txtCustomer.Text, cashpaid, change, subtotal);
                }
                else
                {
                    if (cash.status != "")
                    {
                        _saleManagement.SaveInvoiceMain(lblReceiptNo.Text, "", lblqty.Text, lblDiscount.Text, lblTotal.Text, getdate, txtCustomer.Text, cashpaid, change, subtotal);
                    }
                }
                char[] charsqtytoTrim = { 'Q', 't', 'y', '.', ' ' };
                for (int i = 0; i < metroGrid1.Rows.Count - 1; i++)
                {
                    string pid = metroGrid1.Rows[i].Cells[0].Value.ToString();

                    string totalqty = metroGrid1.Rows[i].Cells[2].Value.ToString();
                    string cleanqty = totalqty.Trim(charsqtytoTrim);

                    string price = metroGrid1.Rows[i].Cells[3].Value.ToString();
                    char[] charspricetoTrim = { 'R', 's', '.' };
                    string cleanprice = price.Trim(charspricetoTrim);

                    string total = metroGrid1.Rows[i].Cells[5].Value.ToString();
                    char[] charstotaltoTrim = { 'Σ' };
                    string cleantotal = total.Trim(charstotaltoTrim);
                    bool Product_exist = _productManagement.CheckProductExist(pid);
                    if (Product_exist == true)
                    {
                        _saleManagement.SaveInvoiceProducts(lblReceiptNo.Text, pid, cleanqty, cleanprice, cleantotal,
                            metroGrid1.Rows[i].Cells[4].Value.ToString(), (Convert.ToInt32(cleanprice) * Convert.ToInt32(cleanqty)).ToString());
                    }
                }
                MessageBox.Show("Invoice has been saved", "Success");
                metroGrid1.Rows.Clear();_getHighestID("");
            }
            else
            {

                if (_customerManagement.CheckCustomerExist(savedcustomerID) == true)
                {
                    _saleManagement.UpdateInvoiceMain(lblReceiptNo.Text, savedcustomerID, lblqty.Text, lblDiscount.Text, lblTotal.Text, DateTime.Now);
                }
                char[] charsqtytoTrim = { 'Q', 't', 'y', '.', ' ' };
                for (int i = 0; i < metroGrid1.Rows.Count - 1; i++)
                {
                    string pid = metroGrid1.Rows[i].Cells[0].Value.ToString();

                    string totalqty = metroGrid1.Rows[i].Cells[2].Value.ToString();
                    string cleanqty = totalqty.Trim(charsqtytoTrim);

                    string price = metroGrid1.Rows[i].Cells[3].Value.ToString();
                    char[] charspricetoTrim = { 'R', 's', '.' };
                    string cleanprice = price.Trim(charspricetoTrim);

                    string total = metroGrid1.Rows[i].Cells[5].Value.ToString();
                    char[] charstotaltoTrim = { 'Σ' };
                    string cleantotal = total.Trim(charstotaltoTrim);
                    bool Product_exist = _productManagement.CheckProductExist(pid);
                    if (Product_exist == true)
                    {
                        _saleManagement.UpdateInvoiceProducts(lblReceiptNo.Text, pid, cleanqty, cleanprice, cleantotal,
                            metroGrid1.Rows[i].Cells[4].Value.ToString(), (Convert.ToInt32(cleanprice) * Convert.ToInt32(cleanqty)).ToString());
                    }
                }
                MessageBox.Show("Invoice has been Updated", "Success");
                metroGrid1.Rows.Clear();
            }

        }

        private void lblReceiptNo_TextChanged(object sender, EventArgs e)
        {
            if (lblReceiptNo.Text != "")
            {
                DataTable dtmainsale = _saleManagement.GetSaleMain_Add(lblReceiptNo.Text);
                if (dtmainsale.Rows.Count > 0)
                {
                    txtCustomer.Text = dtmainsale.Rows[0][1].ToString();
                    lblDiscount.Text = dtmainsale.Rows[0][3].ToString();
                    lblTotal.Text = dtmainsale.Rows[0][4].ToString();
                }

                DataTable dtsubsale = _saleManagement.GetSaleSub_Add(lblReceiptNo.Text);
                for (int i = 0; i < dtsubsale.Rows.Count; i++)
                {
                    metroGrid1.Rows.Add(dtsubsale.Rows[i][0].ToString(), dtsubsale.Rows[i][1].ToString(),
                        dtsubsale.Rows[i][2].ToString(),dtsubsale.Rows[i][3].ToString(),
                        dtsubsale.Rows[i][5].ToString(),dtsubsale.Rows[i][4].ToString());
                }
            }
        }

        private void metroGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int quantity = Convert.ToInt32(metroGrid1.Rows[e.RowIndex].Cells[2].Value.ToString());
            int price = Convert.ToInt32(metroGrid1.Rows[e.RowIndex].Cells[3].Value.ToString());
            metroGrid1.Rows[e.RowIndex].Cells[5].Value = (quantity*price).ToString();
            _get_sum_qty_total();
        }
    }
}
