﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;
using Messaging;

namespace POS
{
    public partial class Supplier_Form : MetroForm
    {
        BusinessLogicLayer.City _cityManagement = new BusinessLogicLayer.City();
        Supplier _supplierManagement = new Supplier();
        public static string supplier_id = "";
        public Supplier_Form(string _supplier)
        {
            InitializeComponent();
            supplier_id = _supplier;
        }

        private void Supplier_Form_Load(object sender, EventArgs e)
        {
            if (supplier_id == "")
            {
                txtSupplierid.Text = _supplierManagement.GetHighestSupplierId();
            }
            else
            {
                txtSupplierid.Text = supplier_id;
                buttonPanels.Visible = false; listitems.Visible = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            AddUpdateSupplier();
            _clear_textboxes();
        }

        private void txtmanualId_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
        }

        private void txtmanualId_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void picsupplier_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            // image filters  
            opd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                lblfilename.Text = opd.SafeFileName;
                picsupplier.Image = new Bitmap(opd.FileName);
                lblImage.Text = opd.FileName;
            }

            string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string foldername = Path.Combine(folderpath, "Product Images");
            if (File.Exists(foldername + @"\" + lblfilename.Text))
            {
                lblAlert.Visible = true;
                lblAlert.Text = "Image Already Exists";
                lblAlert.ForeColor = Color.Red;
                btnSave.Enabled = false;
            }
            else
            {
                lblAlert.Visible = false;
                btnSave.Enabled = true;
            }
        }

        public void AddUpdateSupplier()
        {
            string automaticid = txtSupplierid.Text;
            string manualid = txtmanualId.Text;
            string name = txSupplierName.Text;
            string mobile = txtmobile.Text;
            string phone = txtphone.Text;
            string ntn = txtntn.Text;
            string zipcode = txtzipcode.Text;
            string email = cmbemail.Text;
            string city = cmbcity.Text;
            string company = txtcompany.Text;
            string address = richtextaddress.Text;
            string comments = richtextcomments.Text;
            string message = string.Empty;

            string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string foldername = Path.Combine(folderpath, "Supplier Images");
            System.IO.Directory.CreateDirectory(foldername);

            if (automaticid != string.Empty)
            {
                if (name != string.Empty)
                {
                    if (_supplierManagement.CheckSupplierExist(automaticid))
                    {
                        if (lblfilename.Text != "" && lblImage.Text!="")
                        {
                            File.Copy(@lblImage.Text, foldername + @"\" + lblfilename.Text);
                        }
                        _supplierManagement.UpdateSupplierDetails(automaticid, manualid, name, mobile, phone, ntn, city, zipcode, email, company,
                            address, comments, lblfilename.Text);
                        message = MessageManager.GetMessage("1", name);
                        MetroFramework.MetroMessageBox.Show(this, message, "Supplier Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (lblfilename.Text != "")
                        {
                            File.Copy(@lblImage.Text, foldername + @"\" + lblfilename.Text);
                        }
                        _supplierManagement.CreateNewSupplier(automaticid, manualid, name, mobile, phone, ntn, city, zipcode, email, company, address,
                            comments, lblfilename.Text);
                        message = MessageManager.GetMessage("1", name);
                        MetroFramework.MetroMessageBox.Show(this, message, "Supplier Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", name);
                    MetroFramework.MetroMessageBox.Show(this, message, "Supplier Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", automaticid);
                MetroFramework.MetroMessageBox.Show(this, message, "Supplier iD", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _clear_textboxes()
        {
            this.Controls.Clear();
            this.InitializeComponent();
            txtSupplierid.Text = _supplierManagement.GetHighestSupplierId();
        }

        private void richtextaddress_Leave(object sender, EventArgs e)
        {
            ((RichTextBox)sender).BackColor = Color.White;
            ((RichTextBox)sender).ForeColor = Color.Black;
        }

        private void richtextaddress_Enter(object sender, EventArgs e)
        {
            ((RichTextBox)sender).BackColor = Color.LemonChiffon;
            ((RichTextBox)sender).ForeColor = Color.Blue;
        }

        private void cmbcity_TextChanged(object sender, EventArgs e)
        {
            if (cmbcity.Text != string.Empty)
            {
                buttonPanels.Visible = true; listitems.Visible = true;
                buttonPanels.Location = new Point(464, 167);
                listitems.Location = new Point(464, 195);
                _get_city(cmbcity.Text);
            }
            else
            {
                _get_city("");
            }
        }

        public void _get_city(string city)
        {
            DataTable dt = _cityManagement.GetCityProfile(city);
            listitems.DataSource = dt;
            listitems.DisplayMember = "City";
            listitems.ValueMember = "City";
        }
       
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cmbcity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                listitems.Focus();
            }
        }

        private void listitems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                    cmbcity.Text = listitems.SelectedValue.ToString();
                    cmbcity.Focus();
                listitems.Visible = false;
                buttonPanels.Visible = false;
            }
            if (listitems.SelectedIndex == 0 && e.KeyCode == Keys.Up)
            {
                cmbcity.Focus();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            City _city = new City("", "City");
            _city.Show();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            City _city = new City(listitems.SelectedValue.ToString(), "City");
            _city.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _cityManagement.DeleteCity(listitems.SelectedValue.ToString());
            MessageBox.Show("Deleted, Please Refresh");
        }

        private void txtSupplierid_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = _supplierManagement.GetSupplierProfile(txtSupplierid.Text);
            if (dt.Rows.Count > 0)
            {//Sid as Serial#, userid as ID, Name, mobile, Phone,NTN,City,ZipCode,Email,Company,Address,Comments,Image
                txtSupplierid.Text = dt.Rows[0][0].ToString();
                txtmanualId.Text = dt.Rows[0][1].ToString();
                txSupplierName.Text = dt.Rows[0][2].ToString();
                txtmobile.Text = dt.Rows[0][3].ToString();
                txtphone.Text = dt.Rows[0][4].ToString();
                txtntn.Text = dt.Rows[0][5].ToString();
                cmbcity.Text = dt.Rows[0][6].ToString();
                txtzipcode.Text = dt.Rows[0][7].ToString();
                cmbemail.Text = dt.Rows[0][8].ToString();
                txtcompany.Text = dt.Rows[0][9].ToString();
                richtextaddress.Text = dt.Rows[0][10].ToString();
                richtextcomments.Text = dt.Rows[0][11].ToString();
                string picture = dt.Rows[0][12].ToString();
                string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string foldername = Path.Combine(folderpath, "Supplier Images");
                try
                {
                    picsupplier.Image = Image.FromFile(foldername + @"\" + picture);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex + " Specified Image is not found in given directory");
                }
                lblfilename.Text = picture;
                txtSupplierid.Focus();
            }
        }

        private void Supplier_Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                AddUpdateSupplier();
            }
            if (e.KeyCode == Keys.D && e.Control)
            {
                _delete_supplier();
            }
        }

        #region delete the supplier
        public void _delete_supplier()
        {
            string message = string.Empty;
            if (_supplierManagement.DeleteSupplier(Convert.ToInt32(txtSupplierid.Text)) == true)
            {
                message = MessageManager.GetMessage("6", txSupplierName.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Supplier Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                message = MessageManager.GetMessage("5", txSupplierName.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Supplier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
