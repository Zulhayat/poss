﻿namespace POS
{
    partial class Add_Product
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Product));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAlert = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new MetroFramework.Controls.MetroGrid();
            this.btnDeleteGeneric = new System.Windows.Forms.Button();
            this.btnEditGeneric = new System.Windows.Forms.Button();
            this.btnAddGeneric = new System.Windows.Forms.Button();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.buttonPanels = new System.Windows.Forms.Panel();
            this.listitems = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbGeneric = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtsaleTax = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtcostTax = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtStockLevel = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSupplier = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.chkControlDrug = new System.Windows.Forms.CheckBox();
            this.txtPackSize = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtstocklimit = new System.Windows.Forms.TextBox();
            this.txtcomments = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtunit = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtlocation = new System.Windows.Forms.TextBox();
            this.txtsaleprice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcost = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.txtCategory = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDisease = new System.Windows.Forms.TextBox();
            this.txtpname = new System.Windows.Forms.TextBox();
            this.txtbarcode = new System.Windows.Forms.TextBox();
            this.txtaditionalcode = new System.Windows.Forms.TextBox();
            this.txtautomaticcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.buttonPanels.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Location = new System.Drawing.Point(2, 11);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(936, 34);
            this.panel5.TabIndex = 24;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(892, -1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 32);
            this.button2.TabIndex = 1;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(-1, -1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = " Save (F2)";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblAlert);
            this.panel1.Controls.Add(this.lblfilename);
            this.panel1.Controls.Add(this.lblImage);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.buttonPanels);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.cmbGeneric);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.txtsaleTax);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.txtcostTax);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.txtStockLevel);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txtSupplier);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.txtManufacturer);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.chkControlDrug);
            this.panel1.Controls.Add(this.txtPackSize);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtstocklimit);
            this.panel1.Controls.Add(this.txtcomments);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtunit);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtlocation);
            this.panel1.Controls.Add(this.txtsaleprice);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtcost);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtBrand);
            this.panel1.Controls.Add(this.txtCategory);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.txtDisease);
            this.panel1.Controls.Add(this.txtpname);
            this.panel1.Controls.Add(this.txtbarcode);
            this.panel1.Controls.Add(this.txtaditionalcode);
            this.panel1.Controls.Add(this.txtautomaticcode);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.shapeContainer1);
            this.panel1.Location = new System.Drawing.Point(3, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(937, 620);
            this.panel1.TabIndex = 0;
            // 
            // lblAlert
            // 
            this.lblAlert.AutoSize = true;
            this.lblAlert.Location = new System.Drawing.Point(936, 10);
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.Size = new System.Drawing.Size(35, 13);
            this.lblAlert.TabIndex = 119;
            this.lblAlert.Text = "label6";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblfilename.Location = new System.Drawing.Point(800, 204);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(0, 13);
            this.lblfilename.TabIndex = 118;
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblImage.Location = new System.Drawing.Point(803, 184);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(0, 17);
            this.lblImage.TabIndex = 116;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.btnDeleteGeneric);
            this.panel2.Controls.Add(this.btnEditGeneric);
            this.panel2.Controls.Add(this.btnAddGeneric);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(1075, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(662, 177);
            this.panel2.TabIndex = 115;
            this.panel2.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridView1.Location = new System.Drawing.Point(4, 43);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(653, 129);
            this.dataGridView1.TabIndex = 87;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // btnDeleteGeneric
            // 
            this.btnDeleteGeneric.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDeleteGeneric.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGeneric.Image")));
            this.btnDeleteGeneric.Location = new System.Drawing.Point(216, 5);
            this.btnDeleteGeneric.Name = "btnDeleteGeneric";
            this.btnDeleteGeneric.Size = new System.Drawing.Size(100, 26);
            this.btnDeleteGeneric.TabIndex = 86;
            this.btnDeleteGeneric.Text = "  Delete";
            this.btnDeleteGeneric.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeleteGeneric.UseVisualStyleBackColor = false;
            // 
            // btnEditGeneric
            // 
            this.btnEditGeneric.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnEditGeneric.Image = ((System.Drawing.Image)(resources.GetObject("btnEditGeneric.Image")));
            this.btnEditGeneric.Location = new System.Drawing.Point(110, 5);
            this.btnEditGeneric.Name = "btnEditGeneric";
            this.btnEditGeneric.Size = new System.Drawing.Size(100, 26);
            this.btnEditGeneric.TabIndex = 86;
            this.btnEditGeneric.Text = "  Edit";
            this.btnEditGeneric.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEditGeneric.UseVisualStyleBackColor = false;
            this.btnEditGeneric.Click += new System.EventHandler(this.btnEditGeneric_Click);
            // 
            // btnAddGeneric
            // 
            this.btnAddGeneric.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAddGeneric.Image = ((System.Drawing.Image)(resources.GetObject("btnAddGeneric.Image")));
            this.btnAddGeneric.Location = new System.Drawing.Point(4, 5);
            this.btnAddGeneric.Name = "btnAddGeneric";
            this.btnAddGeneric.Size = new System.Drawing.Size(100, 26);
            this.btnAddGeneric.TabIndex = 86;
            this.btnAddGeneric.Text = "  Add";
            this.btnAddGeneric.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddGeneric.UseVisualStyleBackColor = false;
            this.btnAddGeneric.Click += new System.EventHandler(this.btnAddGeneric_Click);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4});
            this.shapeContainer2.Size = new System.Drawing.Size(660, 175);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape4.BorderWidth = 3;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 2;
            this.lineShape4.X2 = 657;
            this.lineShape4.Y1 = 38;
            this.lineShape4.Y2 = 38;
            // 
            // buttonPanels
            // 
            this.buttonPanels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonPanels.Controls.Add(this.listitems);
            this.buttonPanels.Controls.Add(this.btnAdd);
            this.buttonPanels.Controls.Add(this.btnDelete);
            this.buttonPanels.Controls.Add(this.btnUpdate);
            this.buttonPanels.Controls.Add(this.shapeContainer3);
            this.buttonPanels.Location = new System.Drawing.Point(1067, 409);
            this.buttonPanels.Name = "buttonPanels";
            this.buttonPanels.Size = new System.Drawing.Size(310, 243);
            this.buttonPanels.TabIndex = 86;
            // 
            // listitems
            // 
            this.listitems.BackColor = System.Drawing.Color.LemonChiffon;
            this.listitems.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listitems.FormattingEnabled = true;
            this.listitems.ItemHeight = 20;
            this.listitems.Location = new System.Drawing.Point(3, 34);
            this.listitems.Name = "listitems";
            this.listitems.Size = new System.Drawing.Size(302, 204);
            this.listitems.TabIndex = 85;
            this.listitems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listitems_KeyDown);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(3, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 26);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "  Add";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(206, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 26);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "  Delete";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(105, 0);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 26);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "  Edit";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5});
            this.shapeContainer3.Size = new System.Drawing.Size(308, 241);
            this.shapeContainer3.TabIndex = 86;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape5.BorderWidth = 3;
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 1;
            this.lineShape5.X2 = 306;
            this.lineShape5.Y1 = 29;
            this.lineShape5.Y2 = 29;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Location = new System.Drawing.Point(1065, 187);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(364, 211);
            this.panel3.TabIndex = 48;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(136, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(107, 29);
            this.textBox1.TabIndex = 50;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Location = new System.Drawing.Point(6, 82);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(352, 125);
            this.panel6.TabIndex = 52;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Location = new System.Drawing.Point(2, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(350, 45);
            this.label14.TabIndex = 36;
            this.label14.Text = "Example if a product has a minimum set to 10 and the maximum\r\nto 50, When the sto" +
    "ck is in 5 ,the system will recommend \r\nreplacment of 45\r\n";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label15.Location = new System.Drawing.Point(2, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(324, 60);
            this.label15.TabIndex = 36;
            this.label15.Text = resources.GetString("label15.Text");
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(136, 41);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(107, 29);
            this.textBox2.TabIndex = 51;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(7, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 20);
            this.label20.TabIndex = 48;
            this.label20.Text = "Maximum Stock";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Location = new System.Drawing.Point(7, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 20);
            this.label16.TabIndex = 49;
            this.label16.Text = "Minimum Stock";
            // 
            // cmbGeneric
            // 
            this.cmbGeneric.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGeneric.Location = new System.Drawing.Point(129, 230);
            this.cmbGeneric.Name = "cmbGeneric";
            this.cmbGeneric.Size = new System.Drawing.Size(662, 29);
            this.cmbGeneric.TabIndex = 10;
            this.cmbGeneric.TextChanged += new System.EventHandler(this.cmbGeneric_TextChanged);
            this.cmbGeneric.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGeneric_KeyDown);
            this.cmbGeneric.Leave += new System.EventHandler(this.cmbGeneric_Leave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::POS.Properties.Resources.DefaultImage;
            this.pictureBox1.Location = new System.Drawing.Point(797, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 134);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 114;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Browse Image";
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // txtsaleTax
            // 
            this.txtsaleTax.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaleTax.Location = new System.Drawing.Point(508, 482);
            this.txtsaleTax.Name = "txtsaleTax";
            this.txtsaleTax.Size = new System.Drawing.Size(283, 29);
            this.txtsaleTax.TabIndex = 19;
            this.txtsaleTax.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtsaleTax.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(422, 491);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 20);
            this.label28.TabIndex = 113;
            this.label28.Text = "SP Tax%";
            // 
            // txtcostTax
            // 
            this.txtcostTax.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcostTax.Location = new System.Drawing.Point(508, 444);
            this.txtcostTax.Name = "txtcostTax";
            this.txtcostTax.Size = new System.Drawing.Size(283, 29);
            this.txtcostTax.TabIndex = 17;
            this.txtcostTax.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtcostTax.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(422, 448);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 20);
            this.label29.TabIndex = 112;
            this.label29.Text = "CP Tax%";
            // 
            // txtStockLevel
            // 
            this.txtStockLevel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStockLevel.Location = new System.Drawing.Point(130, 389);
            this.txtStockLevel.Name = "txtStockLevel";
            this.txtStockLevel.Size = new System.Drawing.Size(287, 29);
            this.txtStockLevel.TabIndex = 14;
            this.txtStockLevel.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtStockLevel.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(21, 393);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 20);
            this.label27.TabIndex = 108;
            this.label27.Text = "Stock Level";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(425, 298);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 20);
            this.label26.TabIndex = 107;
            this.label26.Text = "Location";
            // 
            // txtSupplier
            // 
            this.txtSupplier.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplier.Location = new System.Drawing.Point(130, 333);
            this.txtSupplier.Name = "txtSupplier";
            this.txtSupplier.Size = new System.Drawing.Size(283, 29);
            this.txtSupplier.TabIndex = 13;
            this.txtSupplier.TextChanged += new System.EventHandler(this.txtSupplier_TextChanged);
            this.txtSupplier.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplier_KeyDown_1);
            this.txtSupplier.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(21, 339);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 20);
            this.label24.TabIndex = 105;
            this.label24.Text = "Supplier";
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManufacturer.Location = new System.Drawing.Point(130, 297);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(283, 29);
            this.txtManufacturer.TabIndex = 11;
            this.txtManufacturer.TextChanged += new System.EventHandler(this.txtManufacturer_TextChanged);
            this.txtManufacturer.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtManufacturer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtManufacturer_KeyDown);
            this.txtManufacturer.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(21, 303);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(103, 20);
            this.label25.TabIndex = 104;
            this.label25.Text = "Manufacturer";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(21, 234);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 20);
            this.label23.TabIndex = 100;
            this.label23.Text = "Generic";
            // 
            // chkControlDrug
            // 
            this.chkControlDrug.AutoSize = true;
            this.chkControlDrug.FlatAppearance.BorderSize = 0;
            this.chkControlDrug.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkControlDrug.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkControlDrug.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.chkControlDrug.Location = new System.Drawing.Point(651, 192);
            this.chkControlDrug.Name = "chkControlDrug";
            this.chkControlDrug.Size = new System.Drawing.Size(140, 25);
            this.chkControlDrug.TabIndex = 9;
            this.chkControlDrug.Text = "Controled Drug";
            this.chkControlDrug.UseVisualStyleBackColor = true;
            this.chkControlDrug.CheckedChanged += new System.EventHandler(this.chkControlDrug_CheckedChanged);
            // 
            // txtPackSize
            // 
            this.txtPackSize.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackSize.Location = new System.Drawing.Point(130, 153);
            this.txtPackSize.Name = "txtPackSize";
            this.txtPackSize.Size = new System.Drawing.Size(283, 29);
            this.txtPackSize.TabIndex = 6;
            this.txtPackSize.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtPackSize.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(21, 153);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 20);
            this.label22.TabIndex = 97;
            this.label22.Text = "Pack Size";
            // 
            // txtstocklimit
            // 
            this.txtstocklimit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstocklimit.Location = new System.Drawing.Point(508, 389);
            this.txtstocklimit.Name = "txtstocklimit";
            this.txtstocklimit.Size = new System.Drawing.Size(283, 29);
            this.txtstocklimit.TabIndex = 15;
            this.txtstocklimit.Click += new System.EventHandler(this.txtstocklimit_Click);
            this.txtstocklimit.TextChanged += new System.EventHandler(this.txtstocklimit_TextChanged);
            this.txtstocklimit.Enter += new System.EventHandler(this.txtstocklimit_Enter);
            this.txtstocklimit.Leave += new System.EventHandler(this.txtstocklimit_Leave);
            // 
            // txtcomments
            // 
            this.txtcomments.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcomments.Location = new System.Drawing.Point(130, 522);
            this.txtcomments.Multiline = true;
            this.txtcomments.Name = "txtcomments";
            this.txtcomments.Size = new System.Drawing.Size(661, 74);
            this.txtcomments.TabIndex = 20;
            this.txtcomments.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtcomments.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 528);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 20);
            this.label13.TabIndex = 93;
            this.label13.Text = "Comments";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(421, 393);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 20);
            this.label11.TabIndex = 91;
            this.label11.Text = "Stock Limit";
            // 
            // txtunit
            // 
            this.txtunit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtunit.Location = new System.Drawing.Point(498, 152);
            this.txtunit.Name = "txtunit";
            this.txtunit.Size = new System.Drawing.Size(293, 29);
            this.txtunit.TabIndex = 7;
            this.txtunit.TextChanged += new System.EventHandler(this.txtunit_TextChanged_1);
            this.txtunit.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtunit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtunit_KeyDown);
            this.txtunit.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(431, 152);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 20);
            this.label12.TabIndex = 90;
            this.label12.Text = "Unit";
            // 
            // txtlocation
            // 
            this.txtlocation.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlocation.Location = new System.Drawing.Point(508, 298);
            this.txtlocation.Name = "txtlocation";
            this.txtlocation.Size = new System.Drawing.Size(283, 29);
            this.txtlocation.TabIndex = 12;
            this.txtlocation.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtlocation.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtsaleprice
            // 
            this.txtsaleprice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaleprice.Location = new System.Drawing.Point(130, 487);
            this.txtsaleprice.Name = "txtsaleprice";
            this.txtsaleprice.Size = new System.Drawing.Size(283, 29);
            this.txtsaleprice.TabIndex = 18;
            this.txtsaleprice.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtsaleprice.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 20);
            this.label5.TabIndex = 83;
            this.label5.Text = "Sale Price";
            // 
            // txtcost
            // 
            this.txtcost.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcost.Location = new System.Drawing.Point(130, 449);
            this.txtcost.Name = "txtcost";
            this.txtcost.Size = new System.Drawing.Size(283, 29);
            this.txtcost.TabIndex = 16;
            this.txtcost.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtcost.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 453);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 20);
            this.label10.TabIndex = 80;
            this.label10.Text = "Cost Price";
            // 
            // txtBrand
            // 
            this.txtBrand.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrand.Location = new System.Drawing.Point(498, 115);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(293, 29);
            this.txtBrand.TabIndex = 5;
            this.txtBrand.TextChanged += new System.EventHandler(this.txtBrand_TextChanged);
            this.txtBrand.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBrand_KeyDown);
            this.txtBrand.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtCategory
            // 
            this.txtCategory.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.Location = new System.Drawing.Point(130, 116);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(283, 29);
            this.txtCategory.TabIndex = 4;
            this.txtCategory.TextChanged += new System.EventHandler(this.txtCategory_TextChanged);
            this.txtCategory.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCategory_KeyDown);
            this.txtCategory.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(385, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(276, 17);
            this.label19.TabIndex = 3;
            this.label19.Text = "Use Barcode Scaner or type the product code";
            // 
            // txtDisease
            // 
            this.txtDisease.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisease.Location = new System.Drawing.Point(130, 191);
            this.txtDisease.Name = "txtDisease";
            this.txtDisease.Size = new System.Drawing.Size(515, 29);
            this.txtDisease.TabIndex = 8;
            this.txtDisease.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtDisease.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtpname
            // 
            this.txtpname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpname.Location = new System.Drawing.Point(130, 80);
            this.txtpname.Name = "txtpname";
            this.txtpname.Size = new System.Drawing.Size(661, 29);
            this.txtpname.TabIndex = 3;
            this.txtpname.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtpname.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtbarcode
            // 
            this.txtbarcode.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbarcode.Location = new System.Drawing.Point(130, 44);
            this.txtbarcode.Name = "txtbarcode";
            this.txtbarcode.Size = new System.Drawing.Size(248, 29);
            this.txtbarcode.TabIndex = 2;
            this.txtbarcode.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtbarcode.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtaditionalcode
            // 
            this.txtaditionalcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaditionalcode.Location = new System.Drawing.Point(508, 10);
            this.txtaditionalcode.Name = "txtaditionalcode";
            this.txtaditionalcode.Size = new System.Drawing.Size(283, 27);
            this.txtaditionalcode.TabIndex = 1;
            this.txtaditionalcode.Enter += new System.EventHandler(this.txtaditionalcode_Enter);
            this.txtaditionalcode.Leave += new System.EventHandler(this.txtaditionalcode_Leave);
            // 
            // txtautomaticcode
            // 
            this.txtautomaticcode.Enabled = false;
            this.txtautomaticcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtautomaticcode.Location = new System.Drawing.Point(130, 10);
            this.txtautomaticcode.Name = "txtautomaticcode";
            this.txtautomaticcode.Size = new System.Drawing.Size(248, 27);
            this.txtautomaticcode.TabIndex = 0;
            this.txtautomaticcode.TextChanged += new System.EventHandler(this.txtautomaticcode_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 20);
            this.label1.TabIndex = 29;
            this.label1.Text = "Code";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(384, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 20);
            this.label17.TabIndex = 34;
            this.label17.Text = "Additional Code";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(21, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 20);
            this.label18.TabIndex = 33;
            this.label18.Text = "SKU/Barcode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "Category";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(419, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Brand";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 20);
            this.label9.TabIndex = 31;
            this.label9.Text = "Disease";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(933, 616);
            this.shapeContainer1.TabIndex = 101;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lineShape3.BorderWidth = 3;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 8;
            this.lineShape3.X2 = 1049;
            this.lineShape3.Y1 = 431;
            this.lineShape3.Y2 = 431;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lineShape2.BorderWidth = 3;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 8;
            this.lineShape2.X2 = 1049;
            this.lineShape2.Y1 = 376;
            this.lineShape2.Y2 = 376;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lineShape1.BorderWidth = 3;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 9;
            this.lineShape1.X2 = 1050;
            this.lineShape1.Y1 = 280;
            this.lineShape1.Y2 = 280;
            // 
            // Add_Product
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(937, 665);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.KeyPreview = true;
            this.Name = "Add_Product";
            this.Load += new System.EventHandler(this.Add_Product_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Add_Product_KeyDown);
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.buttonPanels.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.TextBox txtCategory;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDisease;
        private System.Windows.Forms.TextBox txtpname;
        private System.Windows.Forms.TextBox txtbarcode;
        private System.Windows.Forms.TextBox txtaditionalcode;
        private System.Windows.Forms.TextBox txtautomaticcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtsaleprice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcost;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listitems;
        private System.Windows.Forms.TextBox txtlocation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtunit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcomments;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtstocklimit;
        private System.Windows.Forms.CheckBox chkControlDrug;
        private System.Windows.Forms.TextBox txtPackSize;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSupplier;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.Label label25;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label26;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.TextBox txtsaleTax;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtcostTax;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtStockLevel;
        private System.Windows.Forms.Label label27;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox cmbGeneric;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.Button btnEditGeneric;
        private System.Windows.Forms.Button btnAddGeneric;
        private System.Windows.Forms.Button btnDeleteGeneric;
        private MetroFramework.Controls.MetroGrid dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel buttonPanels;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblAlert;

    }
}