﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class MainForm : MetroFramework.Forms.MetroForm
    {
        string message = "";
        public MainForm()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Products";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnlogoff_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 frm1 = new Form1();
            frm1.Show();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Supplier";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Customer";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnPurchases_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Purchasing";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnSales_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Selling";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Accounts";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Settings";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "User";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            this.Hide();
            message = "Reports";
            MainFormMain main = new MainFormMain(message);
            main.Show();
        }

    }
}
