﻿namespace POS
{
    partial class Supplier_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Supplier_Form));
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listitems = new System.Windows.Forms.ListBox();
            this.buttonPanels = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbemail = new System.Windows.Forms.TextBox();
            this.cmbcity = new System.Windows.Forms.TextBox();
            this.picsupplier = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtzipcode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmanualId = new System.Windows.Forms.TextBox();
            this.txtcompany = new System.Windows.Forms.TextBox();
            this.txtntn = new System.Windows.Forms.TextBox();
            this.richtextcomments = new System.Windows.Forms.RichTextBox();
            this.richtextaddress = new System.Windows.Forms.RichTextBox();
            this.txtmobile = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txSupplierName = new System.Windows.Forms.TextBox();
            this.txtSupplierid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.lblAlert = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.buttonPanels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picsupplier)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Location = new System.Drawing.Point(0, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(939, 34);
            this.panel5.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(899, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(36, 30);
            this.btnClose.TabIndex = 18;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(-1, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = " Save (F2)";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.groupBox1.Controls.Add(this.lblAlert);
            this.groupBox1.Controls.Add(this.lblfilename);
            this.groupBox1.Controls.Add(this.lblImage);
            this.groupBox1.Controls.Add(this.listitems);
            this.groupBox1.Controls.Add(this.buttonPanels);
            this.groupBox1.Controls.Add(this.cmbemail);
            this.groupBox1.Controls.Add(this.cmbcity);
            this.groupBox1.Controls.Add(this.picsupplier);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtzipcode);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtmanualId);
            this.groupBox1.Controls.Add(this.txtcompany);
            this.groupBox1.Controls.Add(this.txtntn);
            this.groupBox1.Controls.Add(this.richtextcomments);
            this.groupBox1.Controls.Add(this.richtextaddress);
            this.groupBox1.Controls.Add(this.txtmobile);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.txtphone);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txSupplierName);
            this.groupBox1.Controls.Add(this.txtSupplierid);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1248, 508);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // listitems
            // 
            this.listitems.BackColor = System.Drawing.Color.LemonChiffon;
            this.listitems.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listitems.FormattingEnabled = true;
            this.listitems.ItemHeight = 20;
            this.listitems.Location = new System.Drawing.Point(943, 273);
            this.listitems.Name = "listitems";
            this.listitems.Size = new System.Drawing.Size(298, 144);
            this.listitems.TabIndex = 80;
            this.listitems.Visible = false;
            this.listitems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listitems_KeyDown);
            // 
            // buttonPanels
            // 
            this.buttonPanels.Controls.Add(this.btnDelete);
            this.buttonPanels.Controls.Add(this.btnUpdate);
            this.buttonPanels.Controls.Add(this.btnAdd);
            this.buttonPanels.Location = new System.Drawing.Point(943, 238);
            this.buttonPanels.Name = "buttonPanels";
            this.buttonPanels.Size = new System.Drawing.Size(299, 33);
            this.buttonPanels.TabIndex = 81;
            this.buttonPanels.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(198, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 26);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "  Delete";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(99, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 26);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "  Edit";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(0, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 26);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "  Add";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbemail
            // 
            this.cmbemail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbemail.Location = new System.Drawing.Point(464, 174);
            this.cmbemail.Name = "cmbemail";
            this.cmbemail.Size = new System.Drawing.Size(283, 29);
            this.cmbemail.TabIndex = 8;
            this.cmbemail.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.cmbemail.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // cmbcity
            // 
            this.cmbcity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcity.Location = new System.Drawing.Point(464, 138);
            this.cmbcity.Name = "cmbcity";
            this.cmbcity.Size = new System.Drawing.Size(283, 29);
            this.cmbcity.TabIndex = 6;
            this.cmbcity.TextChanged += new System.EventHandler(this.cmbcity_TextChanged);
            this.cmbcity.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.cmbcity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbcity_KeyDown);
            this.cmbcity.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // picsupplier
            // 
            this.picsupplier.BackColor = System.Drawing.Color.White;
            this.picsupplier.Image = ((System.Drawing.Image)(resources.GetObject("picsupplier.Image")));
            this.picsupplier.Location = new System.Drawing.Point(753, 13);
            this.picsupplier.Name = "picsupplier";
            this.picsupplier.Size = new System.Drawing.Size(140, 150);
            this.picsupplier.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picsupplier.TabIndex = 40;
            this.picsupplier.TabStop = false;
            this.picsupplier.Click += new System.EventHandler(this.picsupplier_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(400, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 20);
            this.label11.TabIndex = 38;
            this.label11.Text = "Email";
            // 
            // txtzipcode
            // 
            this.txtzipcode.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtzipcode.Location = new System.Drawing.Point(117, 174);
            this.txtzipcode.Name = "txtzipcode";
            this.txtzipcode.Size = new System.Drawing.Size(277, 29);
            this.txtzipcode.TabIndex = 7;
            this.txtzipcode.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtzipcode.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 20);
            this.label12.TabIndex = 36;
            this.label12.Text = "Zip Code";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 20);
            this.label7.TabIndex = 35;
            this.label7.Text = "Company";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(400, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 20);
            this.label8.TabIndex = 33;
            this.label8.Text = "City";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(464, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "User Can Set this ID by self";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(371, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 30;
            this.label5.Text = "Custom ID";
            // 
            // txtmanualId
            // 
            this.txtmanualId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmanualId.Location = new System.Drawing.Point(464, 14);
            this.txtmanualId.Name = "txtmanualId";
            this.txtmanualId.Size = new System.Drawing.Size(283, 29);
            this.txtmanualId.TabIndex = 1;
            this.txtmanualId.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtmanualId.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // txtcompany
            // 
            this.txtcompany.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcompany.Location = new System.Drawing.Point(117, 220);
            this.txtcompany.Name = "txtcompany";
            this.txtcompany.Size = new System.Drawing.Size(630, 29);
            this.txtcompany.TabIndex = 9;
            this.txtcompany.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtcompany.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // txtntn
            // 
            this.txtntn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtntn.Location = new System.Drawing.Point(117, 139);
            this.txtntn.Name = "txtntn";
            this.txtntn.Size = new System.Drawing.Size(277, 29);
            this.txtntn.TabIndex = 5;
            this.txtntn.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtntn.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // richtextcomments
            // 
            this.richtextcomments.Location = new System.Drawing.Point(117, 312);
            this.richtextcomments.Name = "richtextcomments";
            this.richtextcomments.Size = new System.Drawing.Size(630, 65);
            this.richtextcomments.TabIndex = 11;
            this.richtextcomments.Text = "";
            this.richtextcomments.Enter += new System.EventHandler(this.richtextaddress_Enter);
            this.richtextcomments.Leave += new System.EventHandler(this.richtextaddress_Leave);
            // 
            // richtextaddress
            // 
            this.richtextaddress.Location = new System.Drawing.Point(117, 256);
            this.richtextaddress.Name = "richtextaddress";
            this.richtextaddress.Size = new System.Drawing.Size(630, 50);
            this.richtextaddress.TabIndex = 10;
            this.richtextaddress.Text = "";
            this.richtextaddress.Enter += new System.EventHandler(this.richtextaddress_Enter);
            this.richtextaddress.Leave += new System.EventHandler(this.richtextaddress_Leave);
            // 
            // txtmobile
            // 
            this.txtmobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobile.Location = new System.Drawing.Point(117, 103);
            this.txtmobile.Name = "txtmobile";
            this.txtmobile.Size = new System.Drawing.Size(277, 29);
            this.txtmobile.TabIndex = 3;
            this.txtmobile.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtmobile.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.checkBox1.Location = new System.Drawing.Point(760, 178);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(106, 21);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "Block Supplier";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtphone
            // 
            this.txtphone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(464, 103);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(283, 29);
            this.txtphone.TabIndex = 4;
            this.txtphone.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txtphone.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(13, 143);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 20);
            this.label21.TabIndex = 24;
            this.label21.Text = "NTN #";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(116, 47);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 15);
            this.label19.TabIndex = 19;
            this.label19.Text = "Generate Automatic ID";
            // 
            // txSupplierName
            // 
            this.txSupplierName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txSupplierName.Location = new System.Drawing.Point(117, 67);
            this.txSupplierName.Name = "txSupplierName";
            this.txSupplierName.Size = new System.Drawing.Size(630, 29);
            this.txSupplierName.TabIndex = 2;
            this.txSupplierName.Enter += new System.EventHandler(this.txtmanualId_Enter);
            this.txSupplierName.Leave += new System.EventHandler(this.txtmanualId_Leave);
            // 
            // txtSupplierid
            // 
            this.txtSupplierid.Enabled = false;
            this.txtSupplierid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplierid.Location = new System.Drawing.Point(117, 14);
            this.txtSupplierid.Name = "txtSupplierid";
            this.txtSupplierid.Size = new System.Drawing.Size(248, 29);
            this.txtSupplierid.TabIndex = 0;
            this.txtSupplierid.TextChanged += new System.EventHandler(this.txtSupplierid_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Supplier Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Name ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mobile";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(400, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Phone";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Comments";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 262);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Address";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblfilename.Location = new System.Drawing.Point(935, 151);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(0, 13);
            this.lblfilename.TabIndex = 120;
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblImage.Location = new System.Drawing.Point(938, 131);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(0, 17);
            this.lblImage.TabIndex = 119;
            // 
            // lblAlert
            // 
            this.lblAlert.AutoSize = true;
            this.lblAlert.Location = new System.Drawing.Point(902, 14);
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.Size = new System.Drawing.Size(35, 13);
            this.lblAlert.TabIndex = 121;
            this.lblAlert.Text = "label6";
            // 
            // Supplier_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 547);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.Name = "Supplier_Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Supplier_Form_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Supplier_Form_KeyDown);
            this.panel5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.buttonPanels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picsupplier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmanualId;
        private System.Windows.Forms.TextBox txtntn;
        private System.Windows.Forms.RichTextBox richtextaddress;
        private System.Windows.Forms.TextBox txtmobile;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txSupplierName;
        private System.Windows.Forms.TextBox txtSupplierid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtcompany;
        private System.Windows.Forms.RichTextBox richtextcomments;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtzipcode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox picsupplier;
        private System.Windows.Forms.TextBox cmbcity;
        private System.Windows.Forms.TextBox cmbemail;
        private System.Windows.Forms.Panel buttonPanels;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listitems;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label lblAlert;
    }
}