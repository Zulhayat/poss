﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class Return_Product : MetroForm
    {
        public static string _productcode; public static string _productname; public static string _customername; public string _quantity; public string _latestquantiy;
        public Return_Product(string productcode, string productname, string customername, string quantity)
        {
            InitializeComponent();
            _productcode = productcode;
            _productname = productname;
            _customername = customername;
            _quantity = quantity;
        }

        private void Return_Product_Load(object sender, EventArgs e)
        {
            lblCustomerName.Text = _customername;
            lblQuantity.Text = "Total Quantity: " + _quantity;
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _latestquantiy = txtquantity.Text;
            this.Hide();
        }
    }
}
