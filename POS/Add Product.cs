﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Drawing.Imaging;
using Messaging;
using BusinessLogicLayer;

namespace POS
{
    public partial class Add_Product : MetroForm
    {
        Brand _brandManagement = new Brand();
        Unit _unitManagement = new Unit();
        BusinessLogicLayer.Supplier _supplierManagement = new Supplier();
        BusinessLogicLayer.Manufacturer _manManagement = new Manufacturer();
        BusinessLogicLayer.City _cityManagement = new BusinessLogicLayer.City();
        BusinessLogicLayer.Product _productManagement = new BusinessLogicLayer.Product();
        Generic _genericManagement = new Generic(); public static string GenericValue = string.Empty;
        string message_focus = "";
        string picture = "";
        string productID = string.Empty;
        BusinessLogicLayer.Category _categoryManagement = new BusinessLogicLayer.Category();
        public Add_Product(string _product)
        {
            InitializeComponent();
            productID = _product;
        }

        #region Click Events
        private void button1_Click(object sender, EventArgs e)
        {
            _save_products();
        }

        private void Add_Product_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                _save_products();
            }
            if (e.KeyCode == Keys.R && e.Control)
            {
                if (productID == string.Empty)
                {
                    txtautomaticcode.Text = _productManagement.GetHighestProductId();
                }
                else if (productID != string.Empty)
                {
                    txtautomaticcode.Text = productID;
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Please Provide Product id. For further Details Contact with Admin.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        #region save events
        public void _save_products()
        {
            bool controldrug = false;
            if (chkControlDrug.Checked == true)
            {
                controldrug = true;
            }
            string message = string.Empty;
            if (txtautomaticcode.Text != string.Empty)
            {
                string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string foldername = Path.Combine(folderpath, "Product Images");
                System.IO.Directory.CreateDirectory(foldername);

                if (txtpname.Text != String.Empty)
                {
                    if (_productManagement.CheckProductExist(txtautomaticcode.Text) != true)
                    {

                        _productManagement.CreateNewProduct(txtautomaticcode.Text, txtaditionalcode.Text, txtbarcode.Text, txtpname.Text,_brandManagement.BrandID(txtBrand.Text),
                            _categoryManagement.GetCategoryID(txtCategory.Text), txtsaleprice.Text, txtDisease.Text, txtPackSize.Text, txtcost.Text,_unitManagement.getUnitID(txtunit.Text),
                            txtlocation.Text, txtstocklimit.Text, txtcomments.Text,_genericManagement.GetGenericID(cmbGeneric.Text) , _manManagement.GetManID(txtManufacturer.Text),_supplierManagement.GetSupplierId(txtSupplier.Text),
                            txtStockLevel.Text, txtcostTax.Text, txtsaleTax.Text, lblfilename.Text, controldrug);
                        if (lblfilename.Text != "")
                        {
                            File.Copy(@lblImage.Text, foldername + @"\" + lblfilename.Text);
                        }
                        message = MessageManager.GetMessage("1", txtpname.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        _Clear();
                        txtautomaticcode.Text = _productManagement.GetHighestProductId();
                    }
                    else
                    {
                        _productManagement.UpdateProductDetails(txtautomaticcode.Text, txtaditionalcode.Text, txtbarcode.Text, txtpname.Text, txtBrand.Text, txtCategory.Text,
                            txtsaleprice.Text, txtDisease.Text, txtPackSize.Text, txtcost.Text,_unitManagement.getUnitID(txtunit.Text) , txtlocation.Text, txtunit.Text, txtstocklimit.Text, txtcomments.Text,
                            txtDisease.Text, cmbGeneric.Text, txtManufacturer.Text, txtSupplier.Text, txtStockLevel.Text, txtcostTax.Text, txtsaleTax.Text, controldrug, lblfilename.Text);
                        if (File.Exists(@lblImage.Text))
                        {
                            File.Copy(@lblImage.Text, foldername + @"\" + lblfilename.Text);
                        }
                        message = MessageManager.GetMessage("4", txtpname.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", "Product Name");
                    MetroFramework.MetroMessageBox.Show(this, message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtautomaticcode.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Product", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion


        #region Enter Events
        #endregion

        #region Leave Events
        #endregion

        #region TextChanged Events
        private void txtCategory_TextChanged(object sender, EventArgs e)
        {
            message_focus = "Category";
            buttonPanels.Visible = true;
            listitems.Visible = true;
            if (txtCategory.Text != String.Empty)
            {
                buttonPanels.Location = new Point(125, 146);
                DataTable dt = _categoryManagement.GetCategoryProfile(txtCategory.Text);
                listitems.DataSource = dt;
                listitems.ValueMember = "Category";
                listitems.DisplayMember = "Category";

            }
            else
            {
                DataTable dt = _categoryManagement.GetCategoryProfile("");
                listitems.DataSource = dt;
                listitems.ValueMember = "Category";
                listitems.DisplayMember = "Category";
            }
        }

        private void txtBrand_TextChanged(object sender, EventArgs e)
        {
            message_focus = "Brand";
            if (txtBrand.Text != String.Empty)
            {
                buttonPanels.Visible = true;
                listitems.Visible = true;
                buttonPanels.Location = new Point(494, 145);
                DataTable dt = _brandManagement.GetBrandProfile(txtBrand.Text);
                listitems.DataSource = dt;
                listitems.ValueMember = "Brand";
                listitems.DisplayMember = "Brnad";

            }
            else
            {
                DataTable dt = _brandManagement.GetBrandProfile("");
                listitems.DataSource = dt;
                listitems.ValueMember = "Brand";
                listitems.DisplayMember = "Brnad";
            }
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            _makestocklimit();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _makestocklimit();
        }

        #endregion

        #region KeyDown Events
        private void txtCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                listitems.Focus();
                message_focus = "Category";
            }
        }

        private void listitems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (message_focus == "Brand")
                {
                    txtBrand.Text = listitems.SelectedValue.ToString();
                    txtBrand.Focus();
                    listitems.Visible = false;
                }
                if (message_focus == "Supplier")
                {
                    txtSupplier.Text = listitems.SelectedValue.ToString();
                    txtSupplier.Focus();
                }
                else if (message_focus == "Category")
                {
                    txtCategory.Text = listitems.SelectedValue.ToString();
                    txtCategory.Focus();
                }
                else if (message_focus == "Unit")
                {
                    txtunit.Text = listitems.SelectedValue.ToString();
                    txtunit.Focus();
                }
                else if (message_focus == "Manufacturer")
                {
                    txtManufacturer.Text = listitems.SelectedValue.ToString();
                    txtManufacturer.Focus();
                }
                listitems.Visible = false;
                buttonPanels.Visible = false;
            }
            if (e.KeyCode == Keys.Up && message_focus == "Brand" && listitems.SelectedIndex == 0)
            {
                txtBrand.Focus();
            }
            if (e.KeyCode == Keys.Up && message_focus == "Supplier" && listitems.SelectedIndex == 0)
            {
                txtSupplier.Focus();
            }
            else if (e.KeyCode == Keys.Up && message_focus == "Category" && listitems.SelectedIndex == 0)
            {
                txtCategory.Focus();
            }
            else if (e.KeyCode == Keys.Up && message_focus == "Unit" && listitems.SelectedIndex == 0)
            {
                txtunit.Focus();
            }
            else if (e.KeyCode == Keys.Up && message_focus == "Manufacturer" && listitems.SelectedIndex == 0)
            {
                txtManufacturer.Focus();
            }
        }
        private void txtBrand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && listitems.Visible == true)
            {
                listitems.Focus();
                message_focus = "Brand";
            }
        }

        private void txtunit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && listitems.Visible == true)
            {
                listitems.Focus();
                message_focus = "Unit";
            }
        }

        #endregion

        #region Click Events
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Title = "Select an Image";
            opd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*jpg; *.jpeg; *.gif; *.bmp";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                lblfilename.Text = opd.SafeFileName;
                pictureBox1.Image = new Bitmap(opd.FileName);
                lblImage.Text = opd.FileName;
            }

            string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string foldername = Path.Combine(folderpath, "Product Images");
            if (File.Exists(foldername + @"\" + lblfilename.Text))
            {
                lblAlert.Visible = true;
                lblAlert.Text = "Image Already Exists";
                lblAlert.ForeColor = Color.Red;
                btnSave.Enabled = false;
            }
            else
            {
                lblAlert.Visible = false;
                btnSave.Enabled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (message_focus == "Brand")
            {
                City _brandrecord = new City("", "Brand");
                _brandrecord.Show();
            }
            else if (message_focus == "Category")
            {
                City _categoryrecord = new City("", "Category");
                _categoryrecord.Show();
            }
            else if (message_focus == "Unit")
            {
                City _unitManagement = new City("", "Unit");
                _unitManagement.Show();
            }
            else if (message_focus == "Manufacturer")
            {
                City _unitManagement = new City("", "Manufacturer");
                _unitManagement.Show();
            }
            else if (message_focus == "Supplier")
            {
                City _unitManagement = new City("", "Supplier");
                _unitManagement.Show();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (message_focus == "Brand")
            {
                City _brandrecord = new City(listitems.SelectedValue.ToString(), "Brand");
                _brandrecord.Show();
            }
            else if (message_focus == "Category")
            {
                City _categoryrecord = new City(listitems.SelectedValue.ToString(), "Category");
                _categoryrecord.Show();
            }
            else if (message_focus == "Unit")
            {
                City _unitrecord = new City(listitems.SelectedValue.ToString(), "Unit");
                _unitrecord.Show();
            }
            else if (message_focus == "Manufacturer")
            {
                City _unitrecord = new City(listitems.SelectedValue.ToString(), "Manufacturer");
                _unitrecord.Show();
            }
            else if (message_focus == "Supplier")
            {
                City _unitrecord = new City(listitems.SelectedValue.ToString(), "Supplier");
                _unitrecord.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Normal Functions
        public void _makestocklimit()
        {
            string max, min = "";
            if (textBox1.Text != "")
            {
                max = "Max=" + textBox1.Text;
            }
            else { max = ""; }
            if (textBox2.Text != "")
            {
                min = "Min=" + textBox2.Text;
            }
            else
            {
                min = "";
            }

            txtstocklimit.Text = max + "," + min;
            if (txtstocklimit.Text == ",")
            {
                txtstocklimit.Text = "";
            }
        }
        #endregion


        private void Add_Product_Load(object sender, EventArgs e)
        {
            if (productID == string.Empty)
            {
                txtautomaticcode.Text = _productManagement.GetHighestProductId();
            }
            else if (productID != string.Empty)
            {
                txtautomaticcode.Text = productID;
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this,"Please Provide Product id. For further Details Contact with Admin.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void txtaditionalcode_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void txtaditionalcode_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
            

        }

        private void txtunit_TextChanged_1(object sender, EventArgs e)
        {
            message_focus = "Unit";
            buttonPanels.Visible = true;
            listitems.Visible = true;
            if (txtunit.Text != String.Empty)
            {
                buttonPanels.Location = new Point(495, 181);
                DataTable dt = _unitManagement.GetUnitProfile(txtunit.Text);
                listitems.DataSource = dt;
                listitems.ValueMember = "Unit";
                listitems.DisplayMember = "Unit";

            }
            else
            {
                DataTable dt = _unitManagement.GetUnitProfile("");
                listitems.DataSource = dt;
                listitems.ValueMember = "Unit";
                listitems.DisplayMember = "Unit";
            }
        }

        private void txtstocklimit_TextChanged(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel3.Location = new Point(508, 173);
        }

        private void txtstocklimit_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel3.Location = new Point(508, 173);
        }

        private void cmbGeneric_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void cmbGeneric_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
        }

        private void txtManufacturer_TextChanged(object sender, EventArgs e)
        {
            message_focus = "Manufacturer";
            buttonPanels.Visible = true;
            listitems.Visible = true;
            if (txtManufacturer.Text != String.Empty)
            {
                buttonPanels.Location = new Point(125, 326);
                DataTable dt = _manManagement.GetManProfile(txtManufacturer.Text);
                listitems.DataSource = dt;
                listitems.ValueMember = "Man";
                listitems.DisplayMember = "Man";

            }
            else
            {
                buttonPanels.Location = new Point(125, 326);
                DataTable dt = _manManagement.GetManProfile("");
                listitems.DataSource = dt;
                listitems.ValueMember = "Man";
                listitems.DisplayMember = "Man";
            }
        }

        private void txtSupplier_TextChanged(object sender, EventArgs e)
        {
            message_focus = "Supplier";
            buttonPanels.Visible = true;
            listitems.Visible = true;
            if (txtSupplier.Text != String.Empty)
            {
                buttonPanels.Location = new Point(127, 362);
                DataTable dt = _supplierManagement.GetSupplierProfile(txtSupplier.Text);
                listitems.DataSource = dt;
                listitems.ValueMember = "name";
                listitems.DisplayMember = "name";

            }
            else
            {
                DataTable dt = _supplierManagement.GetSupplierProfile("");
                listitems.DataSource = dt;
                listitems.ValueMember = "name";
                listitems.DisplayMember = "name";
            }
        }

        private void txtManufacturer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && listitems.Visible == true)
            {
                listitems.Focus();
                message_focus = "Manufacturer";
            }
        }

        private void txtSupplier_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && listitems.Visible == true)
            {
                listitems.Focus();
                message_focus = "Supplier";
            }
        }

        private void txtstocklimit_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
            textBox1.Focus();
            panel3.Visible = true;
            panel3.Location = new Point(508, 173);
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
            panel3.Visible = false;
            txtcost.Focus();
        }

        private void txtstocklimit_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
            panel3.Visible = false;
        }

        private void cmbGeneric_TextChanged(object sender, EventArgs e)
        {
            message_focus = "Generic";
            panel2.Visible = true;
            if (cmbGeneric.Text != String.Empty)
            {
                panel2.Location = new Point(129, 259);
                DataTable dt = _genericManagement.GetGenericProfile(cmbGeneric.Text);
                dataGridView1.DataSource = dt;
                dataGridView1.Columns[0].Width = 100;
                dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.SteelBlue;

            }
            else
            {
                panel2.Visible = false;
            }
        }

        private void btnAddGeneric_Click(object sender, EventArgs e)
        {
            if (message_focus == "Generic")
            {
                City _brandrecord = new City("", "Generic");
                _brandrecord.Show();
            }
        }

        private void btnEditGeneric_Click(object sender, EventArgs e)
        {
            if (message_focus == "Generic")
            {
                City _brandrecord = new City(GenericValue, "Generic");
                _brandrecord.Show();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                GenericValue = Convert.ToString(selectedRow.Cells["Generic"].Value);
            }
        }

        private void cmbGeneric_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && panel2.Visible == true)
            {
                dataGridView1.Focus();
                message_focus = "Generic";
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && (dataGridView1.SelectedCells[0].RowIndex == 0))
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                GenericValue = Convert.ToString(selectedRow.Cells["Generic"].Value);
                cmbGeneric.Focus();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                GenericValue = Convert.ToString(selectedRow.Cells["Generic"].Value);
                cmbGeneric.Text = GenericValue;
                panel2.Visible = false;
                txtManufacturer.Focus();
            }
        }

        #region Clear textboxes
        public void _Clear()
        {
            this.Controls.Clear();
            this.InitializeComponent();
        }
        #endregion

        private void txtautomaticcode_TextChanged(object sender, EventArgs e)
        {
            if (txtautomaticcode.Text == productID)
            {
                DataTable dt = _productManagement.GetProductProfile(productID);
                if (dt.Rows.Count > 0)
                {
                    txtaditionalcode.Text = dt.Rows[0][1].ToString();
                    txtbarcode.Text = dt.Rows[0][2].ToString();
                    txtpname.Text = dt.Rows[0][3].ToString();
                    txtBrand.Text =_brandManagement.BrandName(dt.Rows[0][4].ToString());
                    txtCategory.Text = _categoryManagement.GetCategoryName(dt.Rows[0][5].ToString());
                    txtsaleprice.Text = dt.Rows[0][6].ToString();
                    txtcost.Text = dt.Rows[0][7].ToString();
                    txtlocation.Text = dt.Rows[0][8].ToString();
                    cmbGeneric.Text = _genericManagement.GetGenericName(dt.Rows[0][9].ToString());
                    txtunit.Text = _unitManagement.getUnitName(dt.Rows[0][10].ToString());
                    txtstocklimit.Text = dt.Rows[0][11].ToString();
                    txtcomments.Text = dt.Rows[0][12].ToString();
                    txtPackSize.Text = dt.Rows[0][13].ToString();
                    txtDisease.Text = dt.Rows[0][14].ToString();
                    txtManufacturer.Text = _manManagement.GetManName(dt.Rows[0][15].ToString());
                    txtSupplier.Text = _supplierManagement.GetSupplierName(dt.Rows[0][16].ToString());
                    txtStockLevel.Text = dt.Rows[0][17].ToString();
                    txtcostTax.Text = dt.Rows[0][18].ToString();
                    txtsaleTax.Text = dt.Rows[0][19].ToString();
                    string chekvalue = dt.Rows[0][20].ToString();
                    picture = dt.Rows[0][22].ToString();
                    string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    string foldername = Path.Combine(folderpath, "Product Images");
                    if (picture != "")
                    {
                        pictureBox1.Image = Image.FromFile(foldername + @"\" + picture);
                    }
                    lblfilename.Text = picture;
                    if (chekvalue == "True")
                    {
                        chkControlDrug.Checked = true;
                    }
                    else
                    {
                        chkControlDrug.Checked = false;
                    }
                    buttonPanels.Visible = false;
                    panel3.Visible = false;
                }
            }
        }

        private void chkControlDrug_CheckedChanged(object sender, EventArgs e)
        {
            _Change_Control_Drug_Color();
        }

        #region Change Control Drug Check box
        public void _Change_Control_Drug_Color()
        {
            if (chkControlDrug.Checked == true)
            {
                chkControlDrug.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                chkControlDrug.ForeColor = System.Drawing.Color.FromArgb(51, 153, 255);
            }
        }
        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
            
        }
    }
}
