﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Messaging;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class Purchases_Dashboard : MetroForm
    {
        BusinessLogicLayer.Purchase _purchaseManagement = new BusinessLogicLayer.Purchase();
        UserControl usercontrol = new UserControl();
        public string invoice_no = "";
        public Purchases_Dashboard()
        {
            InitializeComponent();
            metroGrid2.RowTemplate.Height = 50;
            DataGridViewComboBoxColumn _datagridviewcombobox = new DataGridViewComboBoxColumn();
        }

        #region get the detailed transaction Record
        public void _getDetailedTransaction()
        {
            string invoice = "", date = "";
            DataTable dt = _purchaseManagement.GetDetailedTransaction("");
            metroGrid2.DataSource = dt;
            this.metroGrid2.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 11);
            this.metroGrid2.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
        }
        public void _getDetailedInventory(string search)
        {
            string invoice = "", date = "";
            DataTable dt = _purchaseManagement.GetDetailedInventory(search);
            gridinventory.DataSource = dt;
            this.gridinventory.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 11);
            this.gridinventory.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            gridinventory.Columns[1].Width = 200;
            gridinventory.Columns[0].Width = 80;
            double sumtotalamount = 0.0;
            double sumtotalpaid = 0.0;
            double sumtotalbalance = 0.0;
            for (int i = 0; i < gridinventory.Rows.Count - 1; i++)
            {
                string cell6 = string.IsNullOrEmpty(gridinventory.Rows[i].Cells[6].Value.ToString()) ? "0" : gridinventory.Rows[i].Cells[6].Value.ToString();
                string cell7 = string.IsNullOrEmpty(gridinventory.Rows[i].Cells[7].Value.ToString()) ? "0" : gridinventory.Rows[i].Cells[7].Value.ToString();
                string cell8 = string.IsNullOrEmpty(gridinventory.Rows[i].Cells[8].Value.ToString()) ? "0" : gridinventory.Rows[i].Cells[8].Value.ToString();
                string cell9 = string.IsNullOrEmpty(gridinventory.Rows[i].Cells[9].Value.ToString()) ? "0" : gridinventory.Rows[i].Cells[9].Value.ToString();
                string cell10 = string.IsNullOrEmpty(gridinventory.Rows[i].Cells[10].Value.ToString()) ? "0" : gridinventory.Rows[i].Cells[10].Value.ToString();
                gridinventory.Rows[i].Cells[10].Value = Convert.ToInt64(cell6) - Convert.ToInt64(cell7) + Convert.ToInt64(cell8) - Convert.ToInt64(cell9);
                sumtotalamount += Convert.ToInt64(cell6) - Convert.ToInt64(cell7) + Convert.ToInt64(cell8);
                sumtotalpaid += Convert.ToInt64(cell9);
                sumtotalbalance += Convert.ToInt64(cell6) - Convert.ToInt64(cell7) + Convert.ToInt64(cell8) - Convert.ToInt64(cell9);
            }
            lbltotalamount.Text = sumtotalamount.ToString();
            lblpaidamount.Text = sumtotalpaid.ToString();
            lblBalance.Text = sumtotalbalance.ToString();
        }
        public void _getStockInHand()
        {
            DataTable dt = _purchaseManagement.GetStockinHand();
            gridStock.DataSource = dt;
            this.gridStock.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 11);
            this.gridStock.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            int stock, retail, cost;
            for (int i = 0; i < gridStock.Rows.Count-1; i++)
            {
                if (int.TryParse(gridStock.Rows[i].Cells[5].Value.ToString(), out stock) && int.TryParse(gridStock.Rows[i].Cells[6].Value.ToString(), out retail))
                {
                    int stockretail = stock * retail;
                    gridStock.Rows[i].Cells[10].Value = stockretail.ToString();
                }
                if (int.TryParse(gridStock.Rows[i].Cells[5].Value.ToString(), out stock) && int.TryParse(gridStock.Rows[i].Cells[7].Value.ToString(), out cost))
                {
                    int stockcost = stock * cost;
                    gridStock.Rows[i].Cells[11].Value = stockcost.ToString();
                }
            } 
            
        }
        public void _getTotalPurchase()
        {
            string totalpurchase = _purchaseManagement.GetTotalPurchase();
            if (totalpurchase != "")
            {
                label27.Text = "RS. " + totalpurchase;
            }
            else
            {
                label27.Text = "Rs. 00";
            }
        }
        public void _getTotalStock()
        {
            string totalstock = _purchaseManagement.GetTotalStock();
            if (totalstock != "")
            {
                label8.Text = "RS. " + totalstock;
            }
            else
            {
                label8.Text = "Rs. 00";
            }
        }
        #endregion

        private void Purchases_Dashboard_Load(object sender, EventArgs e)
        {
            _getDetailedTransaction();
            _getTotalPurchase();
            _getTotalStock();
            _getStockInHand();
            _getDetailedInventory("");
        }

        private void button22_Click(object sender, EventArgs e)
        {
            New_Purchases_POP purchaseform = new New_Purchases_POP();
            purchaseform.Show();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            
        }

        private void metroGrid2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                DataGridViewComboBoxCell combo = new DataGridViewComboBoxCell();
                if (metroGrid2.Columns[e.ColumnIndex].Name.Contains("Items"))
                {
                    metroGrid2[e.ColumnIndex, e.RowIndex] = combo;
                    combo.DataSource = null;
                    combo.ValueMember = "Items";
                    combo.DisplayMember = "Items";
                }
            }
        }

        private void LoadSerial(DataGridView grid)
        {
            foreach (DataGridViewRow row in grid.Rows)
            {
                grid.Rows[row.Index].HeaderCell.Value = String.Format("{0} ", row.Index + 1);
            }
        }

        private void metroGrid2_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            LoadSerial(metroGrid2);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void metroGrid2_SelectionChanged(object sender, EventArgs e)
        {
            if (metroGrid2.SelectedCells.Count > 0)
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                invoice_no = Convert.ToString(selectedRow.Cells["Invoice"].Value);
            }
        }

        private void btnCancelTransaction_Click(object sender, EventArgs e)
        {
            if (invoice_no != "")
            {
                bool result = _purchaseManagement.DeleteInvoice(Convert.ToInt32(invoice_no));
                if (result == true)
                {
                    MetroFramework.MetroMessageBox.Show(this, "Your Invoice has been Deleted", "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _getDetailedTransaction();
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Your Invoice cannot be deleted", "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Please Select Invoice First", "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroGrid2_DoubleClick(object sender, EventArgs e)
        {
            if (metroGrid2.SelectedRows.Count < 1)
            {
                MessageBox.Show("Please select a client");
                return;
            }
            else
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                invoice_no = Convert.ToString(selectedRow.Cells["Invoice"].Value);
                AlertBox alertbox = new AlertBox(invoice_no);
                alertbox.ShowDialog();
            }
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            _Export_pdf(metroGrid2);
        }

        public void _Export_pdf(DataGridView grid)
        {
            if (grid.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf)|*.pdf";
                sfd.FileName = "Output.pdf";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            PdfPTable pdfTable = new PdfPTable(grid.Columns.Count);
                            pdfTable.DefaultCell.Padding = 3;
                            pdfTable.WidthPercentage = 100;
                            pdfTable.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in grid.Columns)
                            {
                                PdfPCell cell = new PdfPCell(new iTextSharp.text.Phrase(column.HeaderText));
                                pdfTable.AddCell(cell);
                            }

                            foreach (DataGridViewRow row in grid.Rows)
                            {
                                foreach (DataGridViewCell cell in row.Cells)
                                {
                                    pdfTable.AddCell(cell.Value.ToString());
                                }
                            }

                            using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                            {
                                Document pdfDoc = new Document(PageSize.A4, 10f, 20f, 20f, 10f);
                                PdfWriter.GetInstance(pdfDoc, stream);
                                pdfDoc.Open();
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                                stream.Close();
                            }

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }

        private void btnDoneTransaction_Click(object sender, EventArgs e)
        {

        }

        private void button21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridStock_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metrosearch_TextChanged(object sender, EventArgs e)
        {
            string search = string.IsNullOrEmpty(metrosearch.Text) ? "" : metrosearch.Text;
            _getDetailedInventory(search);
        }

        private void button30_Click(object sender, EventArgs e)
        {
            _Export_pdf(gridinventory);
        }

        private void button39_Click(object sender, EventArgs e)
        {
            _Export_pdf(gridStock);
        }
    }
}
