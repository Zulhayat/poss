﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class Sales : MetroForm
    {
        int row = 0; string customer;
        BusinessLogicLayer.Sales _salesManagement = new BusinessLogicLayer.Sales();
        public Sales()
        {
            InitializeComponent();
        }

        private void button46_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Sales_Load(object sender, EventArgs e)
        {
            _sum_sales("");
        }

        public void _sum_sales(string search)
        {
            lblTotalSales.Text = "Rs.  " + _salesManagement.GetTotalSales();
            lblTotalReceipts.Text = _salesManagement.GetTotalInvoices();
            lblSubtotal.Text = "Rs.  " + _salesManagement.GetTotalSubTotal();
            lblDiscount.Text = "Rs. " + _salesManagement.GetTotalDiscount();
            DataTable dt = _salesManagement.GetSaleInvoice(search);
            metroGrid2.DataSource = dt;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            _Export_pdf(metroGrid2);
        }

        public void _Export_pdf(MetroFramework.Controls.MetroGrid grid)
        {
            if (grid.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf)|*.pdf";
                sfd.FileName = "Output.pdf";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            PdfPTable pdfTable = new PdfPTable(grid.Columns.Count);
                            pdfTable.DefaultCell.Padding = 3;
                            pdfTable.WidthPercentage = 100;
                            pdfTable.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in grid.Columns)
                            {
                                PdfPCell cell = new PdfPCell(new iTextSharp.text.Phrase(column.HeaderText));
                                pdfTable.AddCell(cell);
                            }

                            foreach (DataGridViewRow row in grid.Rows)
                            {
                                foreach (DataGridViewCell cell in row.Cells)
                                {
                                    pdfTable.AddCell(cell.Value.ToString());
                                }
                            }

                            using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                            {
                                Document pdfDoc = new Document(PageSize.A4, 10f, 20f, 20f, 10f);
                                PdfWriter.GetInstance(pdfDoc, stream);
                                pdfDoc.Open();
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                                stream.Close();
                            }

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }

        private void metroTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (metroTextBox1.Text != "")
            {
                _sum_sales(metroTextBox1.Text);
            }
            else
            {
                _sum_sales("");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            New_Sales_POS _newsale = new New_Sales_POS("");
            _newsale.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            sale_Return _sale_return = new sale_Return(row.ToString(),customer);
            _sale_return.Show();
        }

        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           row = Convert.ToInt32(metroGrid2.Rows[e.RowIndex].Cells[1].Value.ToString());
           customer = metroGrid2.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            New_Sales_POS _newsale = new New_Sales_POS(row.ToString());
            _newsale.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (row != 0)
            {
                _salesManagement.DeleteInvoice(row);
                MessageBox.Show("Invoice has been deleted", "Success", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Give Invoice number","Error",MessageBoxButtons.OK);
            }
        }
    }
}
