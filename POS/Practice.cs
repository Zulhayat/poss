﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class Practice : Form
    {
        BusinessLogicLayer.Purchase _purchase = new Purchase();
        BusinessLogicLayer.Manufacturer _manManagement = new Manufacturer();
        BusinessLogicLayer.Supplier _supplierManagement = new Supplier();
        public static string msg = "";
        string[] collections;
        
        public Practice(string _msg)
        {
            InitializeComponent();
            msg = _msg;
            collections = new string[] { "Pakistan", "Sri Lanka", "India", "Haiderabad", "China", "Australia", "Japan", "France", "Italy", "Haiti", "Colombia" };
        }

        private void Practice_Load(object sender, EventArgs e)
        {
            //DataTable dt = _purchase.GetPurchaseProductList(msg);
            //listBox1.DataSource = dt;
            //listBox1.ValueMember = "Description";
            //listBox1.DisplayMember = "Description";
            comboBox1.Items.AddRange(collections);
            comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = listBox1.SelectedItem;
            listBox1.Visible = false;
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            // get the keyword to search
            string textToSearch = comboBox1.Text.ToLower();
            listBox1.Visible = false; // hide the listbox, see below for why doing that
            if (String.IsNullOrEmpty(textToSearch))
                return; // return with listbox hidden if the keyword is empty
            //search
            string[] result = (from i in collections
                               where i.ToLower().Contains(textToSearch)
                               select i).ToArray();
            if (result.Length == 0)
                return; // return with listbox hidden if nothing found

            listBox1.Items.Clear(); // remember to Clear before Add
            listBox1.Items.AddRange(result);
            listBox1.Visible = true; // show the listbox again
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string man = _manManagement.GetManName(textBox1.Text);
            textBox2.Text = man;
        }
    }
}
