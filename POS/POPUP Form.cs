﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using BusinessLogicLayer;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS
{
    public partial class Category : Form
    {
        BusinessLogicLayer.City _cityManagement = new BusinessLogicLayer.City();
        public static string _message = string.Empty;
        public static string _control = string.Empty;

        public Category(string message, string control,int point1, int point2)
        {
            InitializeComponent();
            _message = message;
            _control = control;
            this.Location = new Point(point1, point2);
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public void _POPUP_form(string message, string control, string value)
        {
            if (message == "City")
            {
                DataTable dt = _cityManagement.GetCityProfile(value);
                listBox1.DataSource = dt;
                listBox1.DisplayMember = "City";
                listBox1.ValueMember = "City";
            }
        }

        private void Category_Load(object sender, EventArgs e)
        {
            _POPUP_form(_message, _control, "");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_message == "City")
            {
                City _city = new City("",_message);
                _city.Show();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (_message == "City")
            {
                City _city = new City("", _message);
                _city.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_message == "City")
            {
                City _city = new City("", _message);
                _city.Show();
            }
        }
    }
}
