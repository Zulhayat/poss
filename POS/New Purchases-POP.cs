﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;
using Messaging;

namespace POS
{
    public partial class New_Purchases_POP : MetroForm
    {
        Purchase _purchaseManagement = new Purchase();
        BusinessLogicLayer.Supplier _supplierManagement = new Supplier();
        BusinessLogicLayer.Product _productManagement = new Product();
        
        Add_Supplier _addSupplier = new Add_Supplier();
        Comments _comments = new Comments("","","","","","");
        public static string ProductValue = string.Empty;
        public static string PriceValue = string.Empty;
        public static string productID = string.Empty;
        public static string saleprice = string.Empty;
        public static string message = string.Empty;
        AutoCompleteStringCollection _autocompletestringcollection = new AutoCompleteStringCollection();

        public New_Purchases_POP()
        {
            InitializeComponent();
        }

        #region cell painting
        private void metroGrid1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            if (e.ColumnIndex == 11)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                var w = Properties.Resources.Delete.Width;
                var h = Properties.Resources.Delete.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.DrawImage(Properties.Resources.Delete, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        #endregion

        #region Down Key Events
        private void txtProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                metroGrid2.Focus();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtPrice.Text = PriceValue;
                txtProductCode.Text = productID;
                txtQty.Focus();
                panel4.Visible = false;
            }
        }

        private void metroGrid2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && (metroGrid2.SelectedCells[0].RowIndex == 0))
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);                
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtPrice.Text = PriceValue;
                txtProductCode.Text = productID;
                txtQty.Focus();
                panel4.Visible = false;
            }
            if (e.KeyCode == Keys.Enter)
            {
                int selectedrowindex = metroGrid2.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = metroGrid2.Rows[selectedrowindex];
                ProductValue = Convert.ToString(selectedRow.Cells["Description"].Value);
                PriceValue = Convert.ToString(selectedRow.Cells["Cost"].Value);
                productID = Convert.ToString(selectedRow.Cells["Sr#"].Value);
                saleprice = Convert.ToString(selectedRow.Cells["Sale Price"].Value);
                txtProduct.Text = ProductValue;
                txtPrice.Text = PriceValue;
                txtProductCode.Text = productID;
                txtQty.Focus();
                panel4.Visible = false;
            }
        }
        #endregion

        #region cell end edit
        private void metroGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            double discount = 0.0, costprice = 0.0, qty = 0.0, tax = 0.0, exactvalue = 0.0;
            if (metroGrid1.Rows.Count - 1 > 0)
            {
                if (e.ColumnIndex == 6)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[6].Value)))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[3].Value)))
                        {
                            qty = Convert.ToDouble(metroGrid1.SelectedCells[3].Value.ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[6].Value)))
                        {
                            discount = Convert.ToDouble(metroGrid1.SelectedCells[6].Value.ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[2].Value)))
                        {
                            costprice = Convert.ToDouble(metroGrid1.SelectedCells[2].Value.ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[8].Value)))
                        {
                            tax = Convert.ToDouble(metroGrid1.SelectedCells[8].Value.ToString());
                        }
                        exactvalue = (qty * costprice) - discount + tax;
                        metroGrid1.SelectedCells[9].Value = exactvalue.ToString();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[3].Value)))
                        {
                            qty = Convert.ToDouble(metroGrid1.SelectedCells[3].Value.ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[2].Value)))
                        {
                            costprice = Convert.ToDouble(metroGrid1.SelectedCells[2].Value.ToString());
                        }
                        exactvalue = (qty * costprice) + tax;
                        metroGrid1.SelectedCells[9].Value = exactvalue.ToString();
                    }
                }
                if (e.ColumnIndex == 8)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[e.RowIndex].Cells[8].Value)))
                    {
                        qty = Convert.ToDouble(metroGrid1.SelectedCells[3].Value.ToString());
                        tax = Convert.ToDouble(metroGrid1.SelectedCells[7].Value.ToString());
                        costprice = Convert.ToDouble(metroGrid1.SelectedCells[2].Value.ToString());
                        discount = Convert.ToDouble(metroGrid1.SelectedCells[6].Value.ToString());
                        exactvalue = (qty * costprice) + tax - discount;
                        metroGrid1.SelectedCells[9].Value = exactvalue.ToString();
                    }
                    else
                    {
                        qty = Convert.ToDouble(metroGrid1.SelectedCells[3].Value.ToString());
                        tax = Convert.ToDouble(metroGrid1.SelectedCells[7].Value.ToString());
                        costprice = Convert.ToDouble(metroGrid1.SelectedCells[2].Value.ToString());
                        discount = Convert.ToDouble(metroGrid1.SelectedCells[6].Value.ToString());
                        exactvalue = (qty * costprice) - discount + tax;
                        metroGrid1.SelectedCells[9].Value = exactvalue.ToString();
                    }
                }
            }
            else
            {

            }
        }

        #endregion

        #region Click Events

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            _addSupplier.Show();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            string subtotal = txtSubTotal.Text, tax = txttax.Text, discount = txtDiscount.Text, freight = txtFreight.Text, finaltotal = txtTotal.Text;
            if (!string.IsNullOrWhiteSpace(txtSubTotal.Text))
            {
                using (Discount _discount = new Discount(subtotal, tax, discount, freight, finaltotal,txtInvoiceID.Text))
                {
                    _discount.ShowDialog();
                    txtFreight.Text = _discount._freight;
                    txtDiscount.Text = _discount._discount;
                    txttax.Text = _discount._tax;
                    txtSubTotal.Text = _discount._subtotal;
                    txtGrandTotal.Text = _discount._finaltotal;
                }
            }
            else
            {
                MessageBox.Show("Please select some Product first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void btnComments_Click(object sender, EventArgs e)
        {
            _comments.ShowDialog();
        }

        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGrid1.Rows.Count - 1 > 0)
            {
                if (e.ColumnIndex == 11)// created column index (delete button)
                {
                    metroGrid1.Rows.Remove(metroGrid1.Rows[e.RowIndex]);
                    int sum = 0;
                    for (int i = 0; i < metroGrid1.Rows.Count; ++i)
                    {
                        sum += Convert.ToInt32(metroGrid1.Rows[i].Cells[2].Value);
                    }
                    lblQty.Text = sum.ToString();
                }
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (txtProductCode.Text != string.Empty)
            {
                metroGrid1.Rows.Add(txtProductCode.Text, txtProduct.Text, txtPrice.Text, txtQty.Text, txtbatch.Text,dtExpire.Text,"","","", txtTotal.Text, "");
                _clear_textboxes();
                _getSubTotal();
                txtProduct.Focus();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Please give Product ID First", "Product", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        #endregion

        #region textchange events
        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            _get_qty_priceMultiply();
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            _get_qty_priceMultiply();
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            _get_productlist();
            _check_brand_category_barcode();
            _get_Image(txtProduct.Text);
        }
        #endregion

        #region normal Function events

        public void _autoSupplier()
        {
            DataTable dt = _supplierManagement.GetSupplierProfile("");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _autocompletestringcollection.Add(dt.Rows[i][2].ToString());
                }
            }
            txtSupplier.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtSupplier.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtSupplier.AutoCompleteCustomSource = _autocompletestringcollection;
        }

        public void _clear_textboxes()
        {
            txtProduct.Text = "";
            txtQty.Text = "";
            txtPrice.Text = "";
            txtTotal.Text = "";
            txtbatch.Text = "";
            panel4.Visible = false;
        }

        public void _get_productlist()
        {
            panel4.Visible = true;
            panel4.Location = new Point(55, 84);
            if (txtProduct.Text != string.Empty)
            {
                metroGrid2.DataSource = _productManagement.GetProductSalePurchase(txtProduct.Text);
            }
            else
            {
                metroGrid2.DataSource = _productManagement.GetProductSalePurchase("");
            }
        }
        #endregion

        private void New_Purchases_POP_Load(object sender, EventArgs e)
        {
            txtInvoiceID.Text = _purchaseManagement.GetHighestInvoiceId();
            metroGrid1.RowTemplate.Height = 60;
            _autoSupplier();
        }

        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

        #region price qty multiplication
        public void _get_qty_priceMultiply()
        {
            double qty = 0.0, total = 0.0;
            if (txtQty.Text != "")
            {
                double price = 0.0;
                qty = Convert.ToDouble(txtQty.Text);
                if (txtPrice.Text != "")
                {
                    price = Convert.ToDouble(txtPrice.Text);
                }
                else
                {
                    price = 0.0;
                }
                total = qty * price;
                txtTotal.Text = total.ToString();
            }
            else
            {
                qty = 0.0; double price = 0.0;
                if (txtPrice.Text != "")
                {
                    price = Convert.ToDouble(txtPrice.Text);
                }
                else
                {
                    price = 0.0;
                }
                total = qty * price;
                txtTotal.Text = total.ToString();
            }
        }
        #endregion

        #region Get the sub total
        public void _getSubTotal()
        {
            int sum = 0;
            for (int i = 0; i < metroGrid1.Rows.Count; ++i)
            {
                sum += Convert.ToInt32(metroGrid1.Rows[i].Cells[9].Value);
            }
            txtSubTotal.Text = sum.ToString();
            string subtotal = txtSubTotal.Text != "" ? subtotal = txtSubTotal.Text : txtSubTotal.Text = "0";
            string tax = txttax.Text != "" ? tax = txttax.Text : txttax.Text = "0";
            string discount = txtDiscount.Text != "" ? discount = txtDiscount.Text : txtDiscount.Text = "0";
            string freight = txtFreight.Text != "" ? freight = txtFreight.Text : txtFreight.Text = "0";
            int sumqty = 0;
            for (int i = 0; i < metroGrid1.Rows.Count; ++i)
            {
                sumqty += Convert.ToInt32(metroGrid1.Rows[i].Cells[3].Value);
            }
            lblQty.Text = sumqty.ToString();
            txtGrandTotal.Text = (Convert.ToDouble(subtotal) + Convert.ToDouble(tax) - Convert.ToDouble(discount) + Convert.ToDouble(freight)).ToString();
        }
        #endregion

        private void txtProduct_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void txtProduct_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
            ////panel4.Visible = false;
        }

        

        private void New_Purchases_POP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                _save_invoice();
            }
            else if (e.KeyCode == Keys.F5)
            {
                _addSupplier.Show();
            }
            else if (e.KeyCode == Keys.F3)
            {
                string subtotal = txtSubTotal.Text, tax = txttax.Text, discount = txtDiscount.Text, freight = txtFreight.Text, finaltotal = txtTotal.Text;
                Discount _discount = new Discount(subtotal,tax,discount,freight,finaltotal,txtInvoiceID.Text);
                _discount.Show();
            }
            else if (e.KeyCode == Keys.F4)
            {
                _comments.Show();
            }
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _save_invoice();
        }

        #region save the invoice
        public void _save_invoice()
        {
            if (txtInvoiceID.Text != string.Empty)
            {
                string supplierid = "";
                DataTable dt = _supplierManagement.GetSupplierProfile(txtSupplier.Text);
                if (dt.Rows.Count > 0)
                {
                    supplierid = dt.Rows[0][0].ToString();
                }
                _purchaseManagement.SaveInvoiceMain(txtInvoiceID.Text, txtInvoiceNo.Text,supplierid, txtSubTotal.Text, txttax.Text, txtDiscount.Text, txtFreight.Text, txtGrandTotal.Text,dtInvoicedate.Text,lblQty.Text);

                if (metroGrid1.Rows.Count > 0)
                {
                    for (int i = 0; i < metroGrid1.Rows.Count - 1; i++)
                    {
                        _purchaseManagement.SaveInvoiceProducts(txtInvoiceID.Text,
                            metroGrid1.Rows[i].Cells[0].Value.ToString(), //code
                            metroGrid1.Rows[i].Cells[2].Value.ToString(), //qty
                            metroGrid1.Rows[i].Cells[3].Value.ToString(), //retailprice
                            metroGrid1.Rows[i].Cells[4].Value.ToString(), //costprice
                            metroGrid1.Rows[i].Cells[5].Value.ToString(), //total amount
                            metroGrid1.Rows[i].Cells[6].Value.ToString(), //discount
                            metroGrid1.Rows[i].Cells[7].Value.ToString(), //margin
                            metroGrid1.Rows[i].Cells[8].Value.ToString(), //tax
                            metroGrid1.Rows[i].Cells[9].Value.ToString(), //batch
                            metroGrid1.Rows[i].Cells[10].Value.ToString()); //expiry
                    }
                }
                message = MessageManager.GetMessage("1", txtInvoiceID.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                _clear_textboxes();
                metroGrid1.DataSource = null;
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Please make sure you have Invoice no", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void chkBrand_CheckedChanged(object sender, EventArgs e)
        {
            _check_brand_category_barcode();
        }

        #region get the image of the product
        public void _get_Image(string description)
        {
            string picture = "";
            if (description != "")
            {
                DataTable dt = _productManagement.GetProductProfile(description);
                if (dt.Rows.Count > 0)
                {
                    picture = dt.Rows[0][21].ToString();
                }
                string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string foldername = Path.Combine(folderpath, "Product Images");
                string searchpath = foldername + @"\" + picture;
                if (File.Exists(searchpath))
                {
                    pictureBox1.Image = Image.FromFile(searchpath);
                }
                else
                {
                    pictureBox1.Image = null;
                }
            }
            else
            {
                pictureBox1.Image = null;
            }
        }
        #endregion

        #region show brand and category in the list of products
        public void _check_brand_category_barcode()
        {
            if (chkBrand.Checked == true)
            {
                metroGrid2.Columns[4].Visible = true;
                metroGrid2.Columns[5].Visible = true;
            }
            else
            {
                metroGrid2.Columns[4].Visible = false;
                metroGrid2.Columns[5].Visible = false;
            }
                if (chkShowBarCode.Checked == true)
                {
                    metroGrid2.Columns[2].Visible = true;
                }
                else
                {
                    metroGrid2.Columns[2].Visible = false;
                }
        }
        #endregion

        private void chkShowBarCode_CheckedChanged(object sender, EventArgs e)
        {
            _check_brand_category_barcode();
        }

        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string description = metroGrid2.Rows[e.RowIndex].Cells[3].Value.ToString();
            _get_Image(description);
        }

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            onlynumwithsinglepoint(sender, e);
        }

        private void metroGrid1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            _Sum_Metrogrid1();
        }

        #region get sum of through metrogrid
        public void _Sum_Metrogrid1()
        {
            double sumqty = 0; double sumdiscount = 0; double sumsubtotal = 0; double retailprice = 0;
            for (int i = 0; i <metroGrid1.Rows.Count - 1; ++i)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[i].Cells[2].Value)))
                {
                    sumqty += Convert.ToDouble(metroGrid1.Rows[i].Cells[2].Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[i].Cells[6].Value)))
                {
                    sumdiscount += Convert.ToDouble(metroGrid1.Rows[i].Cells[6].Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[i].Cells[3].Value)))
                {
                    retailprice += Convert.ToDouble(metroGrid1.Rows[i].Cells[3].Value);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[i].Cells[5].Value)))
                {
                    metroGrid1.Rows[i].Cells[5].Value = (Convert.ToDouble(metroGrid1.Rows[i].Cells[2].Value) * Convert.ToDouble(metroGrid1.Rows[i].Cells[4].Value)).ToString();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(metroGrid1.Rows[i].Cells[5].Value)))
                {
                    sumsubtotal += Convert.ToDouble(metroGrid1.Rows[i].Cells[5].Value);
                }
            }
            lblQty.Text = sumqty.ToString(); txtDiscount.Text = txtDiscount.Text; txtSubTotal.Text = sumsubtotal.ToString();

            string subtotal = txtSubTotal.Text != "" ? subtotal = txtSubTotal.Text : txtSubTotal.Text = "0";
            string tax = txttax.Text != "" ? tax = txttax.Text : txttax.Text = "0";
            string discount = txtDiscount.Text != "" ? discount = txtDiscount.Text : txtDiscount.Text = "0";
            string freight = txtFreight.Text != "" ? freight = txtFreight.Text : txtFreight.Text = "0";
            txtGrandTotal.Text = (Convert.ToDouble(subtotal) + Convert.ToDouble(tax) - Convert.ToDouble(discount) + Convert.ToDouble(freight)).ToString();



        }
        #endregion

        private void txtSupplier_TextChanged(object sender, EventArgs e)
        {

        }

        private void metroGrid2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            metroGrid2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridViewRow row = this.metroGrid2.Rows[e.RowIndex];
            string productvalue = row.Cells[3].Value.ToString();
            _get_Image(productvalue);
        }
    }
}