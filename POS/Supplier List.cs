﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class Supplier_List : MetroForm
    {
        Supplier _supplierManagement = new Supplier();
        string _message_id = "";
        public Supplier_List()
        {
            InitializeComponent();
        }

        private void Supplier_List_Load(object sender, EventArgs e)
        {
            _get_supplier("");
        }


        #region get the supplier list
        public void _get_supplier(string value)
        {
            DataTable dt = _supplierManagement.GetSupplierProfile(value);
            metroGrid1.DataSource = dt;
            this.metroGrid1.DefaultCellStyle.Font = new Font("Tahoma", 11);
        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Supplier_Form _supplier_Add = new Supplier_Form("");
            _supplier_Add.Show();
            
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void metroTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (metroTextBox1.Text != "")
            {
                _get_supplier(metroTextBox1.Text);
            }
            else
            {
                _get_supplier("");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Supplier_Form _supplier_Add = new Supplier_Form(_message_id);
            _supplier_Add.Show();
        }

        private void metroGrid1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            _message_id = metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString();
            Supplier_Form _supplier_Add = new Supplier_Form(_message_id);
            _supplier_Add.Show();
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
