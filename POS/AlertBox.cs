﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogicLayer;

namespace POS
{
    public partial class AlertBox : Form
    {
        BusinessLogicLayer.Purchase _purchase = new Purchase();
        public static string _alert = "";
        public AlertBox(string alert)
        {
            InitializeComponent();
            _alert = alert;
        }

        private void btnPicClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AlertBox_Load(object sender, EventArgs e)
        {
            DataTable dt = _purchase.GetPurchaseProductList(_alert);
            listBox1.DataSource = dt;
            listBox1.ValueMember = "Description";
            listBox1.DisplayMember = "Description";
        }
    }
}
