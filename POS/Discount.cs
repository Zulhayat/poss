﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Messaging;
using BusinessLogicLayer;

namespace POS
{
    public partial class Discount : MetroForm
    {
        discount _discountManagement = new discount();
        public string _get_pos_id = "";
        private bool isDelPressed = false;
        public string _subtotal, _tax, _discount, _freight, _finaltotal;
        public Discount(string subtotal, string tax, string discount, string freight, string finaltotal, string get_pos_id)
        {
            InitializeComponent();
            _subtotal = subtotal;
            _tax = tax;
            _discount = discount;
            _freight = freight;
            _finaltotal = finaltotal;
            _get_pos_id = get_pos_id;
        }

        private void Discount_Load(object sender, EventArgs e)
        {
            if (_subtotal != string.Empty)
            {
                txtSubtotal.Text = _subtotal;
            }
            if(_tax != string.Empty)
            {
                txttax.Text = _tax;
            }
            if (_discount != string.Empty)
            {
                txtdiscount1.Text = _discount;
            }
            if (_freight != string.Empty)
            {
                txtfreight.Text = _freight;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _freight = txtfreight.Text;
            _subtotal = txtSubtotal.Text;
            _discount = txtdiscount1.Text;
            _tax = txttax.Text;
            _finaltotal = txttotalamount.Text;
            this.Hide();
            AddupdateDiscount();
        }

        public void Clear()
        {
            txtSubtotal.Clear();
            txtdiscount1.Clear();
            txtdiscountPercentage1.Clear();
            txttax.Clear();
            txttaxpercentage.Clear();
            txtfreight.Clear();
            txtadjustment.Clear();
            txttotalamount.Clear();
        }

        public void AddupdateDiscount()
        {
            string subtotal = txtSubtotal.Text.Trim();
            string discount = txtdiscount1.Text.Trim();
            string discount_per = txtdiscountPercentage1.Text.Trim();
            string tax = txttax.Text.Trim();
            string taxpercentage = txttaxpercentage.Text.Trim();
            string freight = txtfreight.Text.Trim();
            string adjustment = txtadjustment.Text.Trim();
            string total = txttotalamount.Text.Trim();
            string message = string.Empty;


            if (subtotal == string.Empty)
            {
                message = MessageManager.GetMessage("33");
                MetroFramework.MetroMessageBox.Show(this,message,"Sub Total",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            if (!_discountManagement.discountExist(_get_pos_id))
            {
                if (_discountManagement.AddNewDiscount(_get_pos_id, _discountManagement.GetDiscountHighestId(), subtotal, discount, discount_per, tax, taxpercentage, freight, adjustment, total))
                {
                    message = MessageManager.GetMessage("38", discount);
                    MetroFramework.MetroMessageBox.Show(this, message, "Discount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clear();
                }
            }
            else
            {
                message = MessageManager.GetMessage("37", discount);
                MetroFramework.MetroMessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }
        


        private void txtdiscount_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }
        #region mathematics
        public void mathematics()
        {
            if (txtadjustment.Text != "")
            {
                string extractsymbol = "";
                string exactstring = "0";
                double d = 0;

                double discount = 0.0;
                if (txtdiscount1.Text != "")
                {
                    discount = double.Parse(txtdiscount1.Text);
                }

                double b = double.Parse(txtSubtotal.Text);
                double discount_percentage = 0.0;
                if (txtdiscountPercentage1.Text != "")
                {
                    discount_percentage = double.Parse(txtdiscountPercentage1.Text);
                }
                double f = 0.0;
                if (txttax.Text != "")
                {
                    d = double.Parse(txttax.Text);
                }
                double c = 0;
                string adjustment = txtadjustment.Text;
                if (adjustment.ToLower().Contains('-'))
                {
                    extractsymbol = adjustment;

                    exactstring = extractsymbol.Substring(1);
                    if (exactstring != "")
                    {
                        double g = double.Parse(exactstring);
                        c = (b - discount) + (d / 100 * b) + f - g;
                    }

                    //txtnetgross.Text = c.ToString();
                    txttotalamount.Text = c.ToString();
                }
                else if (adjustment.ToLower().Contains('+'))
                {
                    extractsymbol = adjustment;
                    exactstring = extractsymbol.Substring(1);
                    if (exactstring != "")
                    {
                        double g = double.Parse(exactstring);
                        c = (b - discount) + (d / 100 * b) + f + g;
                    }
                    txttotalamount.Text = c.ToString();
                }
                else
                {
                    double adjust = 0.0;
                    if (txtadjustment.Text != "")
                    {
                        adjust = double.Parse(txtadjustment.Text);
                    }
                    double grosstotal = 0.0;
                    if (txtSubtotal.Text != "")
                    {
                        grosstotal = double.Parse(txtSubtotal.Text);
                    }
                    double tax = 0.0;
                    if (txttax.Text != "")
                    {
                        tax = double.Parse(txttax.Text);
                    }
                    double loading = 0.0;
                    if (txtfreight.Text != "")
                    {
                        loading = double.Parse(txtfreight.Text);
                    }
                    double discount_ = 0.0;
                    if (txtdiscount1.Text != "")
                    {
                        discount_ = double.Parse(txtdiscount1.Text);
                    }
                    double result = grosstotal + ((tax / 100) * grosstotal) + loading - discount_ + adjust;
                    txttotalamount.Text = result.ToString();
                }
            }
            else
            {
                double grosstotal = 0.0;
                if (!string.IsNullOrWhiteSpace(txtSubtotal.Text) && txtSubtotal.Text!=string.Empty)
                {
                    grosstotal = double.Parse(txtSubtotal.Text);
                }
                double tax = 0.0;
                if (!string.IsNullOrWhiteSpace(txttax.Text) && txttax.Text!=string.Empty)
                {
                    tax = double.Parse(txttax.Text);
                }
                double loading = 0.0;
                if (!string.IsNullOrWhiteSpace(txtfreight.Text) && txtfreight.Text != string.Empty)
                {
                    loading = double.Parse(txtfreight.Text);
                }
                double discount = 0.0;
                if (!string.IsNullOrWhiteSpace(txtdiscount1.Text) && txtdiscount1.Text != string.Empty)
                {
                    discount = double.Parse(txtdiscount1.Text);
                }
                //double result = grosstotal + ((tax / 100) * grosstotal) + loading - discount;
                double result = grosstotal + tax + loading - discount;
                if (discount != 0)
                {
                    txtdiscountPercentage1.Text = (( discount / grosstotal) * 100).ToString();
                }
                else
                {
                    txtdiscountPercentage1.Text = "0";
                }
                txttaxpercentage.Text = ((tax / 100) * (grosstotal-discount)).ToString();
                txttotalamount.Text = result.ToString();
            }
        }
        #endregion
       


        private void txtSubtotal_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }

        private void txtdiscountPercentage_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }

        private void txttaxpercentage_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtdiscount_Leave(object sender, EventArgs e)
        {
            
        }

        private void txtdiscountPercentage_Leave(object sender, EventArgs e)
        {
            
        }

        private void txtSubtotal_Leave(object sender, EventArgs e)
        {
        }

        private void txttax_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }

        private void txttax_Leave(object sender, EventArgs e)
        {
            
        }

        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

        private void txtSubtotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            onlynumwithsinglepoint(sender,e);
        }

        private void txtfreight_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }

        private void txtadjustment_TextChanged(object sender, EventArgs e)
        {
            mathematics();
        }

        private void txtadjustment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '+' && txtadjustment.Text.Contains("+")) || (e.KeyChar == '-' && txtadjustment.Text.Contains("-")))
            {
                e.Handled = true;
            }
            if (char.IsDigit(e.KeyChar) || e.KeyChar == '+' || e.KeyChar == '-' || isDelPressed)
            {

            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtadjustment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                isDelPressed = true;
            }
        }

        private void txttotalamount_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Discount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Hide();
            }
            if (e.KeyCode == Keys.F2)
            {
                _freight = txtfreight.Text;
                _subtotal = txtSubtotal.Text;
                _discount = txtdiscount1.Text;
                _tax = txttax.Text;
                _finaltotal = txttotalamount.Text;
                this.Hide();
                AddupdateDiscount();
            }
        }
    }
}
