﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MetroFramework.Forms;

namespace POS
{
    public partial class frmWelcom : MetroForm
    {
        
        public frmWelcom()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Welcome_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            int intX = Screen.PrimaryScreen.Bounds.Width;
            int intY = Screen.PrimaryScreen.Bounds.Height;
            if (intX < this.Width)
                this.Width = intX;
            if (intY < this.Height)
                this.Height = intY;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label4.Visible = false;
            pictureBox2.Width += 1;
            label5.Text = (pictureBox2.Width).ToString() + "%";
            if (pictureBox2.Width >= 100)
            {
                timer1.Stop();
                this.Hide();
                Form1 from = new Form1();
                from.Show();
            }
            label4.Visible = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
