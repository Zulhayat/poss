﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
using System.IO;
using BusinessLogicLayer;
using Messaging;


namespace POS
{
    public partial class Customers : MetroForm
    {
        BusinessLogicLayer.Customers _customerManagement = new BusinessLogicLayer.Customers();
        BusinessLogicLayer.City _cityManagement = new BusinessLogicLayer.City();
        string message_focus = string.Empty; public static string _ID = string.Empty;
        public static string _customerid = "";
        public Customers(string customerId)
        {
            InitializeComponent();
            _customerid = customerId;
        }

        private void Customers_Load(object sender, EventArgs e)
        {
            if (_customerid == "")
            {
                txtautomaticid.Text = _customerManagement.GetHighestUserId();
                txtrefrenceid.Text = _customerManagement.GetHighestRefId();
            }
            else
            {
                txtautomaticid.Text = _customerid;
            }
        }

        #region get the list of city
        public void _get_city(string city)
        {
           DataTable dt = _cityManagement.GetCityProfile(city);
           listitems.DataSource = dt;
           listitems.DisplayMember = "City";
           listitems.ValueMember = "City";
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (txtcustomercity.Text != "" && txtcustomercity.Focus()==true)
            {
                listitems.Visible = true;
                buttonPanels.Visible = true;
                buttonPanels.Location = new Point(116, 170);
                listitems.Location = new Point(116, 198);
                _get_city(txtcustomercity.Text);
            }
            else
            {
                _get_city("");
            }
        }

        private void txtcustomercity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                listitems.Focus();
            }
            
        }

        private void txtcustomercity_Leave(object sender, EventArgs e)
        {
            
        }

        private void listitems_Click(object sender, EventArgs e)
        {
            
        }

        private void listitems_DoubleClick(object sender, EventArgs e)
        {
            
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            Update_save_Customer();
            ClearTextBoxes();
        }

        public void Update_save_Customer()
        {
            string message = string.Empty;
            if (txtautomaticid.Text != string.Empty)
            {
                if (txtcustomername.Text != String.Empty)
                {
                    if (_customerManagement.CheckCustomerExist(txtautomaticid.Text) != true)
                    {
                        string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        string foldername = Path.Combine(folderpath, "Customer Images");
                        System.IO.Directory.CreateDirectory(foldername);
                        _customerManagement.CreateNewCustomer(txtautomaticid.Text, txtmanualid.Text, txtcustomername.Text, dtdateofbirth.Text, txtcustomercity.Text, txtphone.Text,
                            txtmobile.Text, txtcnic.Text, txtaddress.Text,lblfilename.Text, cmbgender.Text);
                        if (lblfilename.Text != "")
                        {
                            File.Copy(@lblImage.Text, foldername + @"\" + lblfilename.Text);
                        }
                        message = MessageManager.GetMessage("1", txtcustomername.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        _customerManagement.UpdateNewCustomer(txtautomaticid.Text, txtmanualid.Text, txtcustomername.Text, dtdateofbirth.Text, txtcustomercity.Text, txtphone.Text,
                                txtmobile.Text, txtcnic.Text, txtaddress.Text,lblfilename.Text, cmbgender.Text);
                        message = MessageManager.GetMessage("5", txtcustomername.Text);
                        MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    if (_customerManagement.CheckRefCustomerExist(txtautomaticid.Text) != true)
                    {
                        string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        string foldername = Path.Combine(folderpath, "RefCustomer Images");
                        System.IO.Directory.CreateDirectory(foldername);
                        _customerManagement.CreateNewReferencedCustomer(txtrefrenceid.Text, txtautomaticid.Text, txtnamerefrence.Text, dtdaterefrence.Text, txtreferencecity.Text, txtphonereference.Text,
                            txtmobilereference.Text, txtcnicreference.Text, txtrefaddress.Text, lblreffilename.Text, cmbgender.Text);
                        if (lblfilename.Text != "" && lblreffilename.Text!="")
                        {
                            File.Copy(@label25.Text, foldername + @"\" + lblreffilename.Text);
                        }
                    }
                    else
                    {
                        _customerManagement.UpdateNewReferencedCustomer(txtrefrenceid.Text, txtautomaticid.Text, txtnamerefrence.Text, dtdaterefrence.Text, txtreferencecity.Text, txtphonereference.Text,
                            txtmobilereference.Text, txtcnicreference.Text, txtrefaddress.Text, lblreffilename.Text, cmbgender.Text);
                    }
                }
                else
                {
                    message = MessageManager.GetMessage("3", "Customer Name");
                    MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                message = MessageManager.GetMessage("2", txtcustomername.Text);
                MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string _getphotoarraystring(PictureBox _image)
        {
            string bytearray = string.Empty;
            MemoryStream ms = new MemoryStream();
            _image.Image.Save(ms, piccustomer.Image.RawFormat);
            byte[] photoarraypic1 = ms.GetBuffer();
            string ASCII = BitConverter.ToString(photoarraypic1);//Encoding.ASCII.GetString(photoarraypic1,0,photoarraypic1.Length);
            return ASCII;
        }


        private void txtreferencecity_TextChanged(object sender, EventArgs e)
        {
            listitems.Visible = true;
            buttonPanels.Visible = true;
            if (txtreferencecity.Text != "" && txtreferencecity.Focus()==true)
            {
                buttonPanels.Location = new Point(116, 408);
                listitems.Location = new Point(116, 440);
                _get_city(txtreferencecity.Text);
            }
            else
            {
                _get_city("");
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            City _city = new City("","City");
            _city.Show();
        }

        private void txtreferencecity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                message_focus = "RefCity";
                listitems.Focus();
            }
        }

        private void txtreferencecity_Leave(object sender, EventArgs e)
        {
           
        }

        private void piccustomer_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            // image filters  
            opd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                lblfilename.Text = opd.SafeFileName;
                piccustomer.Image = new Bitmap(opd.FileName);
                lblImage.Text = opd.FileName;
            }

            string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string foldername = Path.Combine(folderpath, "Customer Images");
            if (File.Exists(foldername + @"\" + lblfilename.Text))
            {
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
            }
        }

        private void picreference_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Title = "Select an Image";
            opd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*jpg; *.jpeg; *.gif; *.bmp";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                lblreffilename.Text = opd.SafeFileName;
                picreference.Image = new Bitmap(opd.FileName);
                label25.Text = opd.FileName;
            }

            string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string foldername = Path.Combine(folderpath, "RefCustomer Images");
            if (File.Exists(foldername + @"\" + lblreffilename.Text))
            {
                
                btnSave.Enabled = false;
            }
            else
            {
                
                btnSave.Enabled = true;
            }
        }

        private void txtautomaticid_TextChanged(object sender, EventArgs e)
        {
            if (_customerManagement.CheckCustomerExist(txtautomaticid.Text) == true)
            {
                DataTable dt = _customerManagement.GetCustomerProfile(txtautomaticid.Text);
                DataTable dt1 = _customerManagement.GetRefCustomerProfile(txtautomaticid.Text);
                if (dt.Rows.Count > 0)
                {
                    txtmanualid.Text = dt.Rows[0][1].ToString();
                    txtcustomername.Text = dt.Rows[0][2].ToString();
                    dtdateofbirth.Text = dt.Rows[0][3].ToString();
                    cmbgender.Text = dt.Rows[0][10].ToString();
                    txtcustomercity.Text = dt.Rows[0][4].ToString();
                    txtphone.Text = dt.Rows[0][5].ToString();
                    txtmobile.Text = dt.Rows[0][6].ToString();
                    txtcnic.Text = dt.Rows[0][7].ToString();
                    txtaddress.Text = dt.Rows[0][8].ToString();
                    cmbgender.Text = dt.Rows[0][9].ToString();
                    string picture = dt.Rows[0][10].ToString();
                    string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    string foldername = Path.Combine(folderpath, "Customer Images");
                    if (picture != "")
                    {
                        piccustomer.Image = Image.FromFile(foldername + @"\" + picture);
                    }
                    lblfilename.Text = picture;
                }
                if (dt1.Rows.Count > 0)
                { 
                    txtrefrenceid.Text = dt1.Rows[0][1].ToString();
                    txtnamerefrence.Text = dt1.Rows[0][2].ToString();
                    dtdaterefrence.Text = dt1.Rows[0][3].ToString();
                    txtreferencecity.Text = dt1.Rows[0][4].ToString();
                    txtphonereference.Text = dt1.Rows[0][5].ToString();
                    txtmobilereference.Text = dt1.Rows[0][6].ToString();
                    txtcnicreference.Text = dt1.Rows[0][7].ToString();
                    txtrefaddress.Text = dt1.Rows[0][8].ToString();
                    cmbgenderreference.Text = dt1.Rows[0][9].ToString();
                    string picture = dt1.Rows[0][10].ToString();
                    string folderpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    string foldername = Path.Combine(folderpath, "RefCustomer Images");
                    if (picture != "")
                    {
                        picreference.Image = Image.FromFile(foldername + @"\" + picture);
                    }
                    lblreffilename.Text = picture;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            City _city = new City(listitems.SelectedValue.ToString(),"City");
            _city.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _cityManagement.DeleteCity(listitems.SelectedValue.ToString());
            MessageBox.Show("Deleted, Please Refresh");
        }

        private void listitems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (message_focus == "RefCity")
                {
                    txtreferencecity.Text = listitems.SelectedValue.ToString();
                    txtreferencecity.Focus();
                }
                else
                {
                    txtcustomercity.Text = listitems.SelectedValue.ToString();
                    txtcustomercity.Focus();
                }
                listitems.Visible = false;
                buttonPanels.Visible = false;
            }
        }

        private void txtmanualid_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.LemonChiffon;
            ((TextBox)sender).ForeColor = Color.Blue;
        }

        private void txtmanualid_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = Color.White;
            ((TextBox)sender).ForeColor = Color.Black;
        }

        private void Customers_KeyDown(object sender, KeyEventArgs e)
        {
            string message = string.Empty;
            if (e.KeyCode == Keys.F2)
            {
                Update_save_Customer();
                ClearTextBoxes();
            }
            if (e.Control && e.KeyCode == Keys.D)
            {
                if (_customerManagement.DeleteCustomer(Convert.ToInt32(txtautomaticid.Text)) == true)
                {
                    message = MessageManager.GetMessage("5", "Customer Name");
                    MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    message = MessageManager.GetMessage("6", "Customer Name");
                    MetroFramework.MetroMessageBox.Show(this, message, "Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #region clear textboxes
        private void ClearTextBoxes()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
            txtautomaticid.Text = _customerManagement.GetHighestUserId();
            txtrefrenceid.Text = _customerManagement.GetHighestRefId();
        }
        #endregion
    }
}
