﻿namespace POS
{
    partial class Customers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customers));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtreferencecity = new System.Windows.Forms.TextBox();
            this.txtoldvalue = new System.Windows.Forms.TextBox();
            this.buttonPanels = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.listitems = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtcnicreference = new System.Windows.Forms.TextBox();
            this.txtrefaddress = new System.Windows.Forms.RichTextBox();
            this.dtdaterefrence = new MetroFramework.Controls.MetroDateTime();
            this.txtmobilereference = new System.Windows.Forms.TextBox();
            this.txtphonereference = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.picreference = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbgenderreference = new System.Windows.Forms.ComboBox();
            this.txtnamerefrence = new System.Windows.Forms.TextBox();
            this.txtrefrenceid = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtcustomercity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmanualid = new System.Windows.Forms.TextBox();
            this.txtcnic = new System.Windows.Forms.TextBox();
            this.txtaddress = new System.Windows.Forms.RichTextBox();
            this.dtdateofbirth = new MetroFramework.Controls.MetroDateTime();
            this.txtmobile = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.piccustomer = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbgender = new System.Windows.Forms.ComboBox();
            this.txtcustomername = new System.Windows.Forms.TextBox();
            this.txtautomaticid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.lblreffilename = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.buttonPanels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picreference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piccustomer)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.groupBox1.Controls.Add(this.lblreffilename);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.lblfilename);
            this.groupBox1.Controls.Add(this.lblImage);
            this.groupBox1.Controls.Add(this.txtreferencecity);
            this.groupBox1.Controls.Add(this.txtoldvalue);
            this.groupBox1.Controls.Add(this.buttonPanels);
            this.groupBox1.Controls.Add(this.listitems);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtcnicreference);
            this.groupBox1.Controls.Add(this.txtrefaddress);
            this.groupBox1.Controls.Add(this.dtdaterefrence);
            this.groupBox1.Controls.Add(this.txtmobilereference);
            this.groupBox1.Controls.Add(this.txtphonereference);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.picreference);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cmbgenderreference);
            this.groupBox1.Controls.Add(this.txtnamerefrence);
            this.groupBox1.Controls.Add(this.txtrefrenceid);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.txtcustomercity);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtmanualid);
            this.groupBox1.Controls.Add(this.txtcnic);
            this.groupBox1.Controls.Add(this.txtaddress);
            this.groupBox1.Controls.Add(this.dtdateofbirth);
            this.groupBox1.Controls.Add(this.txtmobile);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.txtphone);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.piccustomer);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.cmbgender);
            this.groupBox1.Controls.Add(this.txtcustomername);
            this.groupBox1.Controls.Add(this.txtautomaticid);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(937, 508);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtreferencecity
            // 
            this.txtreferencecity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreferencecity.Location = new System.Drawing.Point(116, 381);
            this.txtreferencecity.Name = "txtreferencecity";
            this.txtreferencecity.Size = new System.Drawing.Size(272, 29);
            this.txtreferencecity.TabIndex = 16;
            this.txtreferencecity.TextChanged += new System.EventHandler(this.txtreferencecity_TextChanged);
            this.txtreferencecity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtreferencecity_KeyDown);
            this.txtreferencecity.Leave += new System.EventHandler(this.txtreferencecity_Leave);
            // 
            // txtoldvalue
            // 
            this.txtoldvalue.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoldvalue.Location = new System.Drawing.Point(858, 257);
            this.txtoldvalue.Name = "txtoldvalue";
            this.txtoldvalue.Size = new System.Drawing.Size(35, 29);
            this.txtoldvalue.TabIndex = 81;
            // 
            // buttonPanels
            // 
            this.buttonPanels.Controls.Add(this.btnDelete);
            this.buttonPanels.Controls.Add(this.btnUpdate);
            this.buttonPanels.Controls.Add(this.btnAdd);
            this.buttonPanels.Location = new System.Drawing.Point(950, 243);
            this.buttonPanels.Name = "buttonPanels";
            this.buttonPanels.Size = new System.Drawing.Size(299, 33);
            this.buttonPanels.TabIndex = 80;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(198, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 26);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "  Delete";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(99, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 26);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "  Edit";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(0, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 26);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "  Add";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // listitems
            // 
            this.listitems.BackColor = System.Drawing.Color.LemonChiffon;
            this.listitems.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listitems.FormattingEnabled = true;
            this.listitems.ItemHeight = 20;
            this.listitems.Location = new System.Drawing.Point(953, 277);
            this.listitems.Name = "listitems";
            this.listitems.Size = new System.Drawing.Size(298, 144);
            this.listitems.TabIndex = 79;
            this.listitems.Click += new System.EventHandler(this.listitems_Click);
            this.listitems.DoubleClick += new System.EventHandler(this.listitems_DoubleClick);
            this.listitems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listitems_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label12.Location = new System.Drawing.Point(6, 243);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(150, 21);
            this.label12.TabIndex = 0;
            this.label12.Text = "Referenced Detail:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 385);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 20);
            this.label11.TabIndex = 76;
            this.label11.Text = "City";
            // 
            // txtcnicreference
            // 
            this.txtcnicreference.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcnicreference.Location = new System.Drawing.Point(464, 452);
            this.txtcnicreference.Name = "txtcnicreference";
            this.txtcnicreference.Size = new System.Drawing.Size(283, 29);
            this.txtcnicreference.TabIndex = 20;
            // 
            // txtrefaddress
            // 
            this.txtrefaddress.Location = new System.Drawing.Point(116, 417);
            this.txtrefaddress.Name = "txtrefaddress";
            this.txtrefaddress.Size = new System.Drawing.Size(272, 65);
            this.txtrefaddress.TabIndex = 18;
            this.txtrefaddress.Text = "";
            // 
            // dtdaterefrence
            // 
            this.dtdaterefrence.CustomFormat = "yyyy-MM-dd";
            this.dtdaterefrence.DisplayFocus = true;
            this.dtdaterefrence.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtdaterefrence.Location = new System.Drawing.Point(116, 347);
            this.dtdaterefrence.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtdaterefrence.Name = "dtdaterefrence";
            this.dtdaterefrence.Size = new System.Drawing.Size(272, 29);
            this.dtdaterefrence.TabIndex = 14;
            // 
            // txtmobilereference
            // 
            this.txtmobilereference.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobilereference.Location = new System.Drawing.Point(464, 417);
            this.txtmobilereference.Name = "txtmobilereference";
            this.txtmobilereference.Size = new System.Drawing.Size(283, 29);
            this.txtmobilereference.TabIndex = 19;
            // 
            // txtphonereference
            // 
            this.txtphonereference.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphonereference.Location = new System.Drawing.Point(464, 382);
            this.txtphonereference.Name = "txtphonereference";
            this.txtphonereference.Size = new System.Drawing.Size(283, 29);
            this.txtphonereference.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(400, 462);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 20);
            this.label14.TabIndex = 72;
            this.label14.Text = "CNIC";
            // 
            // picreference
            // 
            this.picreference.BackColor = System.Drawing.Color.White;
            this.picreference.Image = ((System.Drawing.Image)(resources.GetObject("picreference.Image")));
            this.picreference.Location = new System.Drawing.Point(760, 312);
            this.picreference.Name = "picreference";
            this.picreference.Size = new System.Drawing.Size(170, 169);
            this.picreference.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picreference.TabIndex = 71;
            this.picreference.TabStop = false;
            this.picreference.Click += new System.EventHandler(this.picreference_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(395, 285);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 15);
            this.label15.TabIndex = 70;
            this.label15.Text = "Generate Automatic ID";
            // 
            // cmbgenderreference
            // 
            this.cmbgenderreference.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbgenderreference.FormattingEnabled = true;
            this.cmbgenderreference.Items.AddRange(new object[] {
            "Male ",
            "Female"});
            this.cmbgenderreference.Location = new System.Drawing.Point(464, 347);
            this.cmbgenderreference.Name = "cmbgenderreference";
            this.cmbgenderreference.Size = new System.Drawing.Size(283, 29);
            this.cmbgenderreference.TabIndex = 15;
            // 
            // txtnamerefrence
            // 
            this.txtnamerefrence.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnamerefrence.Location = new System.Drawing.Point(117, 312);
            this.txtnamerefrence.Name = "txtnamerefrence";
            this.txtnamerefrence.Size = new System.Drawing.Size(630, 29);
            this.txtnamerefrence.TabIndex = 13;
            // 
            // txtrefrenceid
            // 
            this.txtrefrenceid.Enabled = false;
            this.txtrefrenceid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrefrenceid.Location = new System.Drawing.Point(117, 277);
            this.txtrefrenceid.Name = "txtrefrenceid";
            this.txtrefrenceid.Size = new System.Drawing.Size(272, 29);
            this.txtrefrenceid.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 281);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 20);
            this.label16.TabIndex = 61;
            this.label16.Text = "Ref. Id";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 314);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 20);
            this.label17.TabIndex = 60;
            this.label17.Text = "Name ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(400, 425);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 20);
            this.label18.TabIndex = 59;
            this.label18.Text = "Mobile";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(400, 388);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 20);
            this.label20.TabIndex = 58;
            this.label20.Text = "Phone";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(12, 349);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 20);
            this.label22.TabIndex = 63;
            this.label22.Text = "Date of Birth";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 421);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 20);
            this.label23.TabIndex = 64;
            this.label23.Text = "Address";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(400, 351);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 20);
            this.label24.TabIndex = 62;
            this.label24.Text = "Gender";
            // 
            // txtcustomercity
            // 
            this.txtcustomercity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcustomercity.Location = new System.Drawing.Point(116, 139);
            this.txtcustomercity.Name = "txtcustomercity";
            this.txtcustomercity.Size = new System.Drawing.Size(273, 29);
            this.txtcustomercity.TabIndex = 5;
            this.txtcustomercity.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtcustomercity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcustomercity_KeyDown);
            this.txtcustomercity.Leave += new System.EventHandler(this.txtcustomercity_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 20);
            this.label8.TabIndex = 33;
            this.label8.Text = "City";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.FlatAppearance.BorderSize = 0;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.checkBox2.Location = new System.Drawing.Point(760, 219);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(150, 21);
            this.checkBox2.TabIndex = 11;
            this.checkBox2.Text = "Referenced Customer";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(464, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "User Can Set this ID by self";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(371, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 30;
            this.label5.Text = "Custom ID";
            // 
            // txtmanualid
            // 
            this.txtmanualid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmanualid.Location = new System.Drawing.Point(464, 14);
            this.txtmanualid.Name = "txtmanualid";
            this.txtmanualid.Size = new System.Drawing.Size(283, 29);
            this.txtmanualid.TabIndex = 1;
            this.txtmanualid.Enter += new System.EventHandler(this.txtmanualid_Enter);
            this.txtmanualid.Leave += new System.EventHandler(this.txtmanualid_Leave);
            // 
            // txtcnic
            // 
            this.txtcnic.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcnic.Location = new System.Drawing.Point(464, 211);
            this.txtcnic.Name = "txtcnic";
            this.txtcnic.Size = new System.Drawing.Size(283, 29);
            this.txtcnic.TabIndex = 9;
            // 
            // txtaddress
            // 
            this.txtaddress.Location = new System.Drawing.Point(117, 175);
            this.txtaddress.Name = "txtaddress";
            this.txtaddress.Size = new System.Drawing.Size(272, 65);
            this.txtaddress.TabIndex = 7;
            this.txtaddress.Text = "";
            // 
            // dtdateofbirth
            // 
            this.dtdateofbirth.CustomFormat = "yyyy-MM-dd";
            this.dtdateofbirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtdateofbirth.Location = new System.Drawing.Point(117, 103);
            this.dtdateofbirth.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtdateofbirth.Name = "dtdateofbirth";
            this.dtdateofbirth.Size = new System.Drawing.Size(272, 29);
            this.dtdateofbirth.TabIndex = 3;
            // 
            // txtmobile
            // 
            this.txtmobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobile.Location = new System.Drawing.Point(464, 175);
            this.txtmobile.Name = "txtmobile";
            this.txtmobile.Size = new System.Drawing.Size(283, 29);
            this.txtmobile.TabIndex = 8;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.checkBox1.Location = new System.Drawing.Point(760, 178);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(144, 21);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Deactivate Customer";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtphone
            // 
            this.txtphone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(464, 139);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(283, 29);
            this.txtphone.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(400, 219);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 20);
            this.label21.TabIndex = 24;
            this.label21.Text = "CNIC";
            // 
            // piccustomer
            // 
            this.piccustomer.BackColor = System.Drawing.Color.White;
            this.piccustomer.Image = ((System.Drawing.Image)(resources.GetObject("piccustomer.Image")));
            this.piccustomer.Location = new System.Drawing.Point(760, 14);
            this.piccustomer.Name = "piccustomer";
            this.piccustomer.Size = new System.Drawing.Size(170, 154);
            this.piccustomer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.piccustomer.TabIndex = 22;
            this.piccustomer.TabStop = false;
            this.piccustomer.Click += new System.EventHandler(this.piccustomer_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(116, 47);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 15);
            this.label19.TabIndex = 19;
            this.label19.Text = "Generate Automatic ID";
            // 
            // cmbgender
            // 
            this.cmbgender.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbgender.FormattingEnabled = true;
            this.cmbgender.Items.AddRange(new object[] {
            "Male ",
            "Female"});
            this.cmbgender.Location = new System.Drawing.Point(464, 103);
            this.cmbgender.Name = "cmbgender";
            this.cmbgender.Size = new System.Drawing.Size(283, 29);
            this.cmbgender.TabIndex = 4;
            // 
            // txtcustomername
            // 
            this.txtcustomername.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcustomername.Location = new System.Drawing.Point(117, 67);
            this.txtcustomername.Name = "txtcustomername";
            this.txtcustomername.Size = new System.Drawing.Size(630, 29);
            this.txtcustomername.TabIndex = 2;
            // 
            // txtautomaticid
            // 
            this.txtautomaticid.Enabled = false;
            this.txtautomaticid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtautomaticid.Location = new System.Drawing.Point(117, 14);
            this.txtautomaticid.Name = "txtautomaticid";
            this.txtautomaticid.Size = new System.Drawing.Size(248, 29);
            this.txtautomaticid.TabIndex = 0;
            this.txtautomaticid.TextChanged += new System.EventHandler(this.txtautomaticid_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Customer Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Name ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(400, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mobile";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(400, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Phone";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Date of Birth";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(400, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Gender";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Location = new System.Drawing.Point(3, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(934, 34);
            this.panel5.TabIndex = 25;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(890, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 30);
            this.button2.TabIndex = 1;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(-1, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = " Save (F2)";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblfilename.Location = new System.Drawing.Point(617, 270);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(0, 13);
            this.lblfilename.TabIndex = 120;
            this.lblfilename.Visible = false;
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblImage.Location = new System.Drawing.Point(620, 250);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(0, 17);
            this.lblImage.TabIndex = 119;
            this.lblImage.Visible = false;
            // 
            // lblreffilename
            // 
            this.lblreffilename.AutoSize = true;
            this.lblreffilename.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblreffilename.Location = new System.Drawing.Point(750, 487);
            this.lblreffilename.Name = "lblreffilename";
            this.lblreffilename.Size = new System.Drawing.Size(0, 13);
            this.lblreffilename.TabIndex = 122;
            this.lblreffilename.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label25.Location = new System.Drawing.Point(753, 467);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(0, 17);
            this.label25.TabIndex = 121;
            this.label25.Visible = false;
            // 
            // Customers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 552);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.KeyPreview = true;
            this.Name = "Customers";
            this.Load += new System.EventHandler(this.Customers_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Customers_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.buttonPanels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picreference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piccustomer)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox piccustomer;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.ComboBox cmbgender;
        private System.Windows.Forms.TextBox txtcustomername;
        private System.Windows.Forms.TextBox txtautomaticid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroDateTime dtdateofbirth;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtmobile;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmanualid;
        private System.Windows.Forms.TextBox txtcnic;
        private System.Windows.Forms.RichTextBox txtaddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcustomercity;
        private System.Windows.Forms.Panel buttonPanels;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listitems;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtcnicreference;
        private System.Windows.Forms.RichTextBox txtrefaddress;
        private MetroFramework.Controls.MetroDateTime dtdaterefrence;
        private System.Windows.Forms.TextBox txtmobilereference;
        private System.Windows.Forms.TextBox txtphonereference;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox picreference;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbgenderreference;
        private System.Windows.Forms.TextBox txtnamerefrence;
        private System.Windows.Forms.TextBox txtrefrenceid;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtoldvalue;
        private System.Windows.Forms.TextBox txtreferencecity;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label lblreffilename;
        private System.Windows.Forms.Label label25;
    }
}