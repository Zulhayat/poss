﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace POS
{
    public partial class sale_Return : MetroForm
    {
        public static string _receipno; public static string _customer;
        BusinessLogicLayer.SaleReturn _salereturnManagement = new BusinessLogicLayer.SaleReturn();

        public sale_Return(string receiptno, string customer)
        {
            InitializeComponent();
            _receipno = receiptno;
            _customer = customer;
        }

        private void sale_Return_Load(object sender, EventArgs e)
        {
            lblReceiptNo.Text = _receipno;
            lblCustomer.Text = _customer;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblReceiptNo_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = _salereturnManagement.GetSaleInvoice(lblReceiptNo.Text);
            metroGrid1.DataSource = dt;
        }

        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGrid1.Rows.Count - 1 > 0)
            {
                if (e.ColumnIndex == 0)
                {
                    using (Return_Product _returnproduct = new Return_Product(metroGrid1.Rows[e.RowIndex].Cells[2].Value.ToString(), metroGrid1.Rows[e.RowIndex].Cells[3].Value.ToString(), lblCustomer.Text, metroGrid1.Rows[e.RowIndex].Cells[4].Value.ToString()))
                    {
                        _returnproduct.ShowDialog();
                        metroGrid1.Rows[e.RowIndex].Cells[9].Value = _returnproduct._latestquantiy;
                        metroGrid1.Rows[e.RowIndex].Cells[10].Value = Convert.ToInt32(metroGrid1.Rows[e.RowIndex].Cells[5].Value.ToString()) * Convert.ToInt32(_returnproduct._latestquantiy);
                    }
                
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (lblCustomer.Text != "")
            {
                for (int i = 0; i < metroGrid1.Rows.Count - 1; i++)
                {
                    _salereturnManagement.SaveInvoiceReturnProducts(lblReceiptNo.Text, metroGrid1.Rows[i].Cells[2].Value.ToString(),
                        metroGrid1.Rows[i].Cells[4].Value.ToString(), metroGrid1.Rows[i].Cells[5].Value.ToString(),
                        metroGrid1.Rows[i].Cells[6].Value.ToString(), metroGrid1.Rows[i].Cells[7].Value.ToString(),
                        metroGrid1.Rows[i].Cells[8].Value.ToString(), metroGrid1.Rows[i].Cells[9].Value.ToString(),
                        metroGrid1.Rows[i].Cells[10].Value.ToString());
                }
            }
            else
            {
                MessageBox.Show("Select Customer Name","Error");
            }
        }
    }
}
