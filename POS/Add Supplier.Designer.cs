﻿namespace POS
{
    partial class Add_Supplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Supplier));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtoldvalue = new System.Windows.Forms.TextBox();
            this.txtsupplierarea = new System.Windows.Forms.TextBox();
            this.txtsuppliercity = new System.Windows.Forms.TextBox();
            this.buttonPanels = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.listitems = new System.Windows.Forms.ListBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtsuppliercomment = new System.Windows.Forms.RichTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsupplierntn = new System.Windows.Forms.TextBox();
            this.txtsupplierphone = new System.Windows.Forms.TextBox();
            this.txtsuppliermobile = new System.Windows.Forms.TextBox();
            this.txtsupplieremail = new System.Windows.Forms.TextBox();
            this.txtSuppliername = new System.Windows.Forms.TextBox();
            this.txtsupplierid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.buttonPanels.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.groupBox1.Controls.Add(this.txtoldvalue);
            this.groupBox1.Controls.Add(this.txtsupplierarea);
            this.groupBox1.Controls.Add(this.txtsuppliercity);
            this.groupBox1.Controls.Add(this.buttonPanels);
            this.groupBox1.Controls.Add(this.listitems);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtsuppliercomment);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtsupplierntn);
            this.groupBox1.Controls.Add(this.txtsupplierphone);
            this.groupBox1.Controls.Add(this.txtsuppliermobile);
            this.groupBox1.Controls.Add(this.txtsupplieremail);
            this.groupBox1.Controls.Add(this.txtSuppliername);
            this.groupBox1.Controls.Add(this.txtsupplierid);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(978, 471);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtoldvalue
            // 
            this.txtoldvalue.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoldvalue.Location = new System.Drawing.Point(924, 287);
            this.txtoldvalue.Name = "txtoldvalue";
            this.txtoldvalue.Size = new System.Drawing.Size(32, 29);
            this.txtoldvalue.TabIndex = 58;
            // 
            // txtsupplierarea
            // 
            this.txtsupplierarea.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsupplierarea.Location = new System.Drawing.Point(447, 149);
            this.txtsupplierarea.Name = "txtsupplierarea";
            this.txtsupplierarea.Size = new System.Drawing.Size(283, 29);
            this.txtsupplierarea.TabIndex = 7;
            this.txtsupplierarea.TextChanged += new System.EventHandler(this.txtsupplierarea_TextChanged);
            this.txtsupplierarea.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsupplierarea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsupplierarea_KeyDown);
            this.txtsupplierarea.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtsuppliercity
            // 
            this.txtsuppliercity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsuppliercity.Location = new System.Drawing.Point(104, 151);
            this.txtsuppliercity.Name = "txtsuppliercity";
            this.txtsuppliercity.Size = new System.Drawing.Size(283, 29);
            this.txtsuppliercity.TabIndex = 6;
            this.txtsuppliercity.TextChanged += new System.EventHandler(this.txtsuppliercity_TextChanged);
            this.txtsuppliercity.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsuppliercity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsuppliercity_KeyDown);
            this.txtsuppliercity.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // buttonPanels
            // 
            this.buttonPanels.Controls.Add(this.button3);
            this.buttonPanels.Controls.Add(this.button4);
            this.buttonPanels.Controls.Add(this.btnAdd);
            this.buttonPanels.Location = new System.Drawing.Point(593, 287);
            this.buttonPanels.Name = "buttonPanels";
            this.buttonPanels.Size = new System.Drawing.Size(299, 33);
            this.buttonPanels.TabIndex = 55;
            this.buttonPanels.Visible = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(198, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 26);
            this.button3.TabIndex = 1;
            this.button3.Text = "  Delete";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(99, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 26);
            this.button4.TabIndex = 2;
            this.button4.Text = "  Edit";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(0, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 26);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "  Add";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // listitems
            // 
            this.listitems.BackColor = System.Drawing.Color.LemonChiffon;
            this.listitems.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listitems.FormattingEnabled = true;
            this.listitems.ItemHeight = 20;
            this.listitems.Location = new System.Drawing.Point(593, 321);
            this.listitems.Name = "listitems";
            this.listitems.Size = new System.Drawing.Size(298, 144);
            this.listitems.TabIndex = 54;
            this.listitems.Visible = false;
            this.listitems.Click += new System.EventHandler(this.listitems_Click);
            this.listitems.DoubleClick += new System.EventHandler(this.listitems_DoubleClick);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.label19.Location = new System.Drawing.Point(394, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(178, 17);
            this.label19.TabIndex = 25;
            this.label19.Text = "System Generate Automatic";
            // 
            // txtsuppliercomment
            // 
            this.txtsuppliercomment.Location = new System.Drawing.Point(104, 186);
            this.txtsuppliercomment.Name = "txtsuppliercomment";
            this.txtsuppliercomment.Size = new System.Drawing.Size(626, 77);
            this.txtsuppliercomment.TabIndex = 8;
            this.txtsuppliercomment.Text = "";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 187);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 20);
            this.label20.TabIndex = 23;
            this.label20.Text = "Comment";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(393, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Area";
            // 
            // txtsupplierntn
            // 
            this.txtsupplierntn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsupplierntn.Location = new System.Drawing.Point(449, 114);
            this.txtsupplierntn.Name = "txtsupplierntn";
            this.txtsupplierntn.Size = new System.Drawing.Size(281, 29);
            this.txtsupplierntn.TabIndex = 5;
            this.txtsupplierntn.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsupplierntn.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtsupplierphone
            // 
            this.txtsupplierphone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsupplierphone.Location = new System.Drawing.Point(449, 79);
            this.txtsupplierphone.Name = "txtsupplierphone";
            this.txtsupplierphone.Size = new System.Drawing.Size(281, 29);
            this.txtsupplierphone.TabIndex = 3;
            this.txtsupplierphone.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsupplierphone.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtsuppliermobile
            // 
            this.txtsuppliermobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsuppliermobile.Location = new System.Drawing.Point(104, 79);
            this.txtsuppliermobile.Name = "txtsuppliermobile";
            this.txtsuppliermobile.Size = new System.Drawing.Size(283, 29);
            this.txtsuppliermobile.TabIndex = 2;
            this.txtsuppliermobile.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsuppliermobile.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtsupplieremail
            // 
            this.txtsupplieremail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsupplieremail.Location = new System.Drawing.Point(104, 114);
            this.txtsupplieremail.Name = "txtsupplieremail";
            this.txtsupplieremail.Size = new System.Drawing.Size(283, 29);
            this.txtsupplieremail.TabIndex = 4;
            this.txtsupplieremail.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsupplieremail.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtSuppliername
            // 
            this.txtSuppliername.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSuppliername.Location = new System.Drawing.Point(104, 44);
            this.txtSuppliername.Name = "txtSuppliername";
            this.txtSuppliername.Size = new System.Drawing.Size(626, 29);
            this.txtSuppliername.TabIndex = 1;
            this.txtSuppliername.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtSuppliername.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // txtsupplierid
            // 
            this.txtsupplierid.Enabled = false;
            this.txtsupplierid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsupplierid.Location = new System.Drawing.Point(104, 9);
            this.txtsupplierid.Name = "txtsupplierid";
            this.txtsupplierid.Size = new System.Drawing.Size(283, 29);
            this.txtsupplierid.TabIndex = 0;
            this.txtsupplierid.Enter += new System.EventHandler(this.txtsupplierid_Enter);
            this.txtsupplierid.Leave += new System.EventHandler(this.txtsupplierid_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(10, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 20);
            this.label18.TabIndex = 7;
            this.label18.Text = "Supplier ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mobile #";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(393, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Phone";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Email";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "City";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(393, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "NTN #";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Location = new System.Drawing.Point(0, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(977, 44);
            this.panel5.TabIndex = 26;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 39);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = " Save (F2)";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(109, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(108, 39);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close ";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Add_Supplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 519);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.KeyPreview = true;
            this.Name = "Add_Supplier";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Add_Supplier_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Add_Supplier_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.buttonPanels.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtsupplieremail;
        private System.Windows.Forms.TextBox txtSuppliername;
        private System.Windows.Forms.TextBox txtsupplierid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtsupplierphone;
        private System.Windows.Forms.TextBox txtsuppliermobile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsupplierntn;
        private System.Windows.Forms.RichTextBox txtsuppliercomment;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtsupplierarea;
        private System.Windows.Forms.TextBox txtsuppliercity;
        private System.Windows.Forms.Panel buttonPanels;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listitems;
        private System.Windows.Forms.TextBox txtoldvalue;
    }
}