﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class Hospital
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewHospital(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Hid", cid));
            paramCollection.Add(new DBParameter("@Hospital", city));
            
            Query = "insert into Hospital_Add" +
                "(Hid, Hospital) values (" +
                "@Hid, @Hospital" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateHospitalDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Hid", cid));
            paramCollection.Add(new DBParameter("@Hospital", city));

            Query = "UPDATE Hospital_Add SET " +
            "Hospital = @Hospital " +
            "WHERE Hid = @Hid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckHospitalExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Hospital", city));

            Query = "SELECT COUNT(*) FROM Hospital_Add where Hospital=@Hospital";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(Hid),0)+1 from Hospital_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        


        public bool DeleteHospital(string userID)
        {
            string Query = "Delete Hospital_Add where Hospital='" + userID.ToString() +"'"; 
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetHospitalProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Hid, Hospital from Hospital_Add where Hospital like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT hospital from Hospital_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
