﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Generic
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewGeneric(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Generic", city));

            Query = "insert into Generic_Add" +
                "(Cid, Generic) values (" +
                "@Cid, @Generic" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateGenericDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Generic", city));

            Query = "UPDATE Generic_Add SET " +
            "Generic = @Generic " +
            "WHERE Cid = @Cid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckGenericExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Generic", city));

            Query = "SELECT COUNT(*) FROM Generic_Add where Generic=@Generic";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from Generic_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetGenericID(string search)
        {
            string Query = string.Empty;
            Query = "SELECT Cid from Generic_Add where Generic = '" + search + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetGenericName(string search)
        {
            string Query = string.Empty;
            if (search != "")
            {
                Query = "SELECT Generic from Generic_Add where Cid = '" + search + "'";
                return _dbHelper.ExecuteScalar(Query).ToString();
            }
            else
            {
                return "Nill";
            }
            
        }

        public bool DeleteGeneric(string userID)
        {
            string Query = "Delete Generic_Add where Generic='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetGenericProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid as 'Sr#', Generic from Generic_Add where Generic like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT CId as 'Sr#', Generic from Generic_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
