﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Vouchers
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();

        public DataTable GetVouchersReceipt(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select ROW_NUMBER() over (order by Rvid) as Sr#, RVid as 'Receipt Voucher ID', " +
                        "Date as 'Creration Date',amount as 'Total Receipt Amount' from ReceiptVoucher where Rvid like '%"+userID+"%'";
            }
            else
            {
                Query = "select ROW_NUMBER() over (order by Rvid) as Sr#, RVid as 'Receipt Voucher ID', " +
                        "Date as 'Creration Date',amount as 'Total Receipt Amount' from ReceiptVoucher";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetVouchersReceiptbetweenDates(string from, string to)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (from != string.Empty && to !=string.Empty)
            {
                Query = "select ROW_NUMBER() over (order by Rvid) as Sr#, RVid as 'Receipt Voucher ID', " +
                        "Date as 'Creration Date',amount as 'Total Receipt Amount' from ReceiptVoucher where date between '" + from + "' and '" + to + "'";
            }
            else
            {
                Query = "select ROW_NUMBER() over (order by Rvid) as Sr#, RVid as 'Receipt Voucher ID', " +
                        "Date as 'Creration Date',amount as 'Total Receipt Amount' from ReceiptVoucher";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
