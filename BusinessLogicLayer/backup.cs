﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
   public class backup
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

       public void _make_backup(string Directory, string dbnamedot, string dbname)
       {
           string query = @"BACKUP DATABASE [" + dbname + "] TO  DISK = '" + Directory + "' WITH  INIT ,  NOUNLOAD ,  NAME = N'" + dbnamedot + "',  NOSKIP ,  STATS = 10,  NOFORMAT";
           _dbHelper.ExecuteNonQuery(query);
       }
    }
}
