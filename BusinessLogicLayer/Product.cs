﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessLogicLayer;
using DataAccessLayer;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Product
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

        string connectionstring = ConfigurationManager.ConnectionStrings["sqlServerCon"].ConnectionString;

        public bool CreateNewProduct(string aut_customerid, string user_customerid, string barcode, string description, string brand, string category,
            string saleprice, string disease, string packsize, string cost, string unit,string location,string stocklimit,string comments,
            string generic,string manufacturer,string supplier,string stocklevel,string costtax,string saletax,string ImageDirectory, bool ControlDrug)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@PCode", aut_customerid));
            paramCollection.Add(new DBParameter("@Adcode", user_customerid));
            paramCollection.Add(new DBParameter("@barcode", barcode));
            paramCollection.Add(new DBParameter("@description", description));
            paramCollection.Add(new DBParameter("@brand", brand));
            paramCollection.Add(new DBParameter("@category", category));
            paramCollection.Add(new DBParameter("@Saleprice", saleprice));
            paramCollection.Add(new DBParameter("@packsize", packsize));//
            paramCollection.Add(new DBParameter("@Cost", cost));
            paramCollection.Add(new DBParameter("@location", location));
            paramCollection.Add(new DBParameter("@unit", unit));
            paramCollection.Add(new DBParameter("@stocklimit", stocklimit));
            paramCollection.Add(new DBParameter("@comments", comments));
            paramCollection.Add(new DBParameter("@disease", disease));
            paramCollection.Add(new DBParameter("@generic", generic));
            paramCollection.Add(new DBParameter("@manufacturer", manufacturer));
            paramCollection.Add(new DBParameter("@supplier", supplier)); 
            paramCollection.Add(new DBParameter("@stocklevel", stocklevel));
            paramCollection.Add(new DBParameter("@costpercentage", costtax));
            paramCollection.Add(new DBParameter("@salepercentage", saletax));
            paramCollection.Add(new DBParameter("@ControlDrug", ControlDrug));
            paramCollection.Add(new DBParameter("@image", ImageDirectory));
            paramCollection.Add(new DBParameter("@isactive", 1));
            Query = "insert into Product_Add" +
                "(PCode,Adcode,barcode,description,brand,category,Saleprice,Cost,location,unit,stocklimit,comments,"+
                " packsize,disease,manufacturer,supplier,stocklevel,costpercentage,salepercentage,image,ControlDrug,isactive,generic)values(" +
                " @PCode,@Adcode,@barcode,@description,@brand,@category,@Saleprice,@Cost,@location,@unit,@stocklimit,@comments,"+
                " @packsize,@disease,@manufacturer,@supplier,@stocklevel,@costpercentage,@salepercentage,@image,@ControlDrug,@isactive,@generic" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateProductDetails(string aut_customerid, string user_customerid, string barcode, string description, string brand, string category,
            string saleprice, string ingredient, string packsize, string cost, string qty, string location, string unit, string stocklimit, string comments,
            string disease,string generic,string manufacturer,string supplier,string stocklevel,string costtax,string saletax, bool ControlDrug, string image)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@PCode", aut_customerid));
            paramCollection.Add(new DBParameter("@Adcode", user_customerid));
            paramCollection.Add(new DBParameter("@barcode", barcode));
            paramCollection.Add(new DBParameter("@description", description));
            paramCollection.Add(new DBParameter("@brand", brand));
            paramCollection.Add(new DBParameter("@category", category));
            paramCollection.Add(new DBParameter("@Saleprice", saleprice));
            paramCollection.Add(new DBParameter("@packsize", packsize));//
            paramCollection.Add(new DBParameter("@Cost", cost));
            paramCollection.Add(new DBParameter("@location", location));
            paramCollection.Add(new DBParameter("@unit", unit));
            paramCollection.Add(new DBParameter("@stocklimit", stocklimit));
            paramCollection.Add(new DBParameter("@comments", comments));
            paramCollection.Add(new DBParameter("@disease", disease));
            paramCollection.Add(new DBParameter("@generic", generic));
            paramCollection.Add(new DBParameter("@manufacturer", manufacturer));
            paramCollection.Add(new DBParameter("@supplier", supplier));
            paramCollection.Add(new DBParameter("@stocklevel", stocklevel));
            paramCollection.Add(new DBParameter("@costpercentage", costtax));
            paramCollection.Add(new DBParameter("@salepercentage", saletax));
            paramCollection.Add(new DBParameter("@ControlDrug",ControlDrug));
            paramCollection.Add(new DBParameter("@Image", image));
            Query = "UPDATE Product_Add SET " +
            "Adcode=@Adcode,barcode=@barcode,description=@description,brand=@brand,category=@category,Saleprice=@Saleprice,"+
            " Cost=@Cost,location=@location,unit = @unit,stocklimit=@stocklimit,comments=@comments,packsize=@packsize,disease=@disease,manufacturer=@manufacturer, " +
            " supplier=@supplier,stocklevel=@stocklevel,costpercentage=@costpercentage,salepercentage=@salepercentage, Image=@Image, ControlDrug = @ControlDrug WHERE PCode = @PCode";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckProductExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@PCode", Id));

            Query = "SELECT COUNT(*) FROM Product_Add where PCode=@PCode";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool DeleteProduct(int userID)
        {
            string Query = "Delete from  Product_Add where PCode=" + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public bool _activeDeactiveProduct(int userID, bool status)
        {
            string Query = "Update Product_Add set IsActive ='"+status+"' where PCode=" + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public string _checkactivationProduct(int userID)
        {
            string Query = "Select Isactive from  Product_Add where PCode=" + userID.ToString();
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetProductId(string userName)
        {
            string Query = "SELECT Pcode from Product_Add where description like '%"+userName+"%'";

            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetHighestProductId()
        {
            string query = "select ISNULL(MAX(Pcode),0)+1 from Product_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public DataTable GetProductProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select PCode as 'Sr#',Adcode as 'Id',Barcode,Description,Brand,Category,Saleprice as 'Sale Price',Cost,location,Generic,"+
                    " unit,stocklimit,comments,packsize,disease,manufacturer,supplier,stocklevel,costpercentage,salepercentage,ControlDrug,Isactive as'Is Active', image from Product_Add where Pcode like '%"+userID.ToString()+"%'or Description like '%"+userID.ToString()+"%'";
            }
            else
            {
                Query = "select PCode as 'Sr#',Adcode as 'Id',Barcode,Description,Brand,Category,Saleprice as 'Sale Price',Cost,location,Generic,"
                    +" unit,stocklimit,comments,packsize,disease,manufacturer,supplier,stocklevel,costpercentage,salepercentage,ControlDrug,Isactive as 'Is Active',Image from Product_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetProductSalePurchase(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select PCode as 'Sr#',Adcode as 'Id',Barcode,Description,Brand,Category,Saleprice as 'Sale Price',Cost,location," +
                    " packsize,ControlDrug,Isactive as'Is Active', image from Product_Add where Pcode like '%" + userID.ToString() + "%'or Description like '%" + userID.ToString() + "%'";
            }
            else
            {
                Query = "select PCode as 'Sr#',Adcode as 'Id',Barcode,Description,Brand,Category,Saleprice as 'Sale Price',Cost,location"
                    + " packsize,ControlDrug,Isactive as 'Is Active',Image from Product_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
