﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Brand
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewBrand(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Brand", city));

            Query = "insert into Brand_Add" +
                "(Cid, Brand) values (" +
                "@Cid, @Brand" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateBrandDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Brand", city));

            Query = "UPDATE Brand_Add SET " +
            "Brand = @Brand " +
            "WHERE Cid = @Cid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckBrandExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Brand", city));

            Query = "SELECT COUNT(*) FROM Brand_Add where Brand=@Brand";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from Brand_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string BrandID(string search)
        {
            string Query = string.Empty;
            Query = "select Cid from Brand_add where Brand = '" + search + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string BrandName(string search)
        {
            string Query = string.Empty;
            Query = "select Brand from Brand_add where Cid = '" + search + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public bool DeleteBrand(string userID)
        {
            string Query = "Delete Brand_Add where Brand='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetBrandProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid, Brand from Brand_Add where Brand like '%" + City.ToString() + "%' or CID like '%"+City.ToString()+"%'";
            }
            else
            {
                Query = "SELECT Brand from Brand_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
