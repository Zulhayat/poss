﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Unit
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

        public bool CreateNewUnit(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@CId", cid));
            paramCollection.Add(new DBParameter("@Unit", city));

            Query = "insert into Unit_Add" +
                "(CId, Unit) values (" +
                "@CId, @Unit" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateUnitDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@CId", cid));
            paramCollection.Add(new DBParameter("@Unit", city));

            Query = "UPDATE Unit_Add SET " +
            "Unit = @Unit " +
            "WHERE CId = @CId";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckUnitExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Unit", city));

            Query = "SELECT COUNT(*) FROM Unit_Add where Unit=@Unit";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from Unit_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string getUnitID(string search)
        {
            string Query = string.Empty;
            Query = "select Cid from Unit_add where Unit = '"+search+"'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string getUnitName(string search)
        {
            string Query = string.Empty;
            Query = "select Unit from Unit_add where CId = '" + search + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public bool DeleteUnit(string userID)
        {
            string Query = "Delete Unit_Add where Unit='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetUnitProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid, Unit from Unit_Add where Unit like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT Unit from Unit_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
