﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessLogicLayer;
using DataAccessLayer;
using System.Configuration;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Class_ReceiptVoucher
    {

        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

        string connectionstring = ConfigurationManager.ConnectionStrings["sqlServerCon"].ConnectionString;

        public bool CreateNewVoucher(string Rvid, string date, string CRVNo,string Sid,string amount, string balance,string comments)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@RVid", Rvid));
            paramCollection.Add(new DBParameter("@Date", date));
            paramCollection.Add(new DBParameter("@CRVNo", CRVNo));
            paramCollection.Add(new DBParameter("@Sid", Sid));
            paramCollection.Add(new DBParameter("@amount", amount));
            paramCollection.Add(new DBParameter("@balance", balance));
            paramCollection.Add(new DBParameter("@comments", comments));
            Query = "insert into ReceiptVoucher" +
                "(RVid,Date,CRVNo,Sid,amount,balance,comments" +
                ")values(" +
                " @RVid,@Date,@CRVNo,@Sid,@amount,@balance,@comments" +
                ") insert into PurchaseMain_Add (Invoice,Date,Invoice_no,Sid,paid,balance)values(@RVid,@Date,@CRVNo,@Sid,@amount,@balance)"+
                "insert into PurchaseSUB_Add(Invoice)values(@RVid)";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        public bool UpdateVoucherDetails(string Rvid, string date, string CRVNo, string Sid, string amount, string balance, string comments)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@RVid", Rvid));
            paramCollection.Add(new DBParameter("@Date", date));
            paramCollection.Add(new DBParameter("@CRVNo", CRVNo));
            paramCollection.Add(new DBParameter("@Sid", Sid));
            paramCollection.Add(new DBParameter("@amount", amount));
            paramCollection.Add(new DBParameter("@balance", balance));
            paramCollection.Add(new DBParameter("@comments", comments));
            Query = "UPDATE ReceiptVoucher SET " +
            "Date=@Date,CRVNo=@CRVNo,Sid=@Sid,Amount=@amount,balance=@Balance," +
            " Comments=@comments" +
            " WHERE Rvid=@RVid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckVoucherExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@RVid", Id));

            Query = "SELECT COUNT(*) FROM ReceiptVoucher where RVid=@RVid";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool DeleteVoucher(int userID)
        {
            string Query = "Delete from  ReceiptVoucher where RVid=" + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public string GetHighestVoucherId()
        {
            string query = "select ISNULL(MAX(Invoice),0)+1 from PurchaseMain_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public DataTable GetVoucherProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            Query = "select rvid, date, crvno, name as 'Supplier', Amount as Debit, Balance, ReceiptVoucher.Comments as 'Comments' from ReceiptVoucher" +
                    " join supplier_Add on ReceiptVoucher.Sid = Supplier_Add.Sid where ReceiptVoucher.Sid = '"+userID+"' order by RVID ASC";
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public string Getlatestpayment(string partyid)
        {
            string query = "select ISNULL(SUM(grandtotal),0) - ISNULL(SUM(paid),0) as Balance from PurchaseMain_Add where Sid='"+partyid+"'";
            return _dbHelper.ExecuteScalar(query).ToString();
        }
    }
}
