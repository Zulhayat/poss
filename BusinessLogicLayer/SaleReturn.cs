﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class SaleReturn
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

        public DataTable GetSaleInvoice(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Sid as 'Return Code',Pcode as 'Additional Code',Description,qty as 'Quantity', " +
                        "retailprice as 'Retail Price',Discount,'' as Tax,final as Total, '' as 'Return Quantity','' as 'Return Amount'" +
                        " from SaleSub_Add join Product_Add on saleSub_Add.Pid = Product_Add.PCode where Sid = '"+userID+"'";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public bool SaveInvoiceReturnProducts(string Invoice, string AdCode, string qty, string retailprice,string discount, 
                                              string tax, string total, string returnqty, string returnamount)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@RID", Invoice));
            paramCollection.Add(new DBParameter("@AdCode", AdCode));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@rprice", retailprice));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@tax", tax));
            paramCollection.Add(new DBParameter("@total", total));
            paramCollection.Add(new DBParameter("@returnqty", returnqty));
            paramCollection.Add(new DBParameter("@returnamount", returnamount));

            Query = "insert into SaleReturn" +
                "(RID,AdCode, qty, rprice,discount,tax,total,returnqty,returnamount)values(" +
                "@RID,@AdCode, @qty, @rprice,@discount,@tax,@total,@returnqty,@returnamount" +
                ") if exists (select Pid from Stock_Manage where pid = @AdCode)" +
                " begin update Stock_Manage set qty = qty + @qty, rprice=@rprice where Pid = @AdCode" +
                " end else begin insert into Stock_Manage (Pid, qty, rprice)values(@AdCode,@qty,@rprice) end ";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }
    }
}
