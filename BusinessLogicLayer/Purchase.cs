﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Purchase
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool SaveInvoiceProducts(string Invoice,string PId,string qty,string retailprice,
            string costprice,string ttlamount,string discount,string margin,string tax,string batch,string expire)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Invoice));
            paramCollection.Add(new DBParameter("@PId", PId));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@retailprice", retailprice));
            paramCollection.Add(new DBParameter("@costprice", costprice));
            paramCollection.Add(new DBParameter("@ttlamount", ttlamount));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@margin", margin));
            paramCollection.Add(new DBParameter("@tax", tax));
            paramCollection.Add(new DBParameter("@batch#", batch));
            paramCollection.Add(new DBParameter("@expire", expire));

            Query = "insert into PurchaseSUB_Add" +
                "(Invoice,PId,qty,retailprice,costprice,ttlamount,discount,margin,tax,batch#,expire) values (" +
                "@Invoice,@PId,@qty,@retailprice,@costprice,@ttlamount,@discount,@margin,@tax,@batch#,@expire" +
                ") if exists (select Pid from Stock_Manage where pid = @PId)" + 
                " begin update Stock_Manage set qty = qty + @qty, rprice=@retailprice,cprice=@costprice,expire=@expire,batch#=@batch# where Pid = @PId" + 
                " end else begin insert into Stock_Manage (Pid, qty, rprice,cprice,expire,batch#)values(@PId,@qty,@retailprice,@costprice,@expire,@batch#) end ";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool SaveInvoiceMain(string Invoice,string Invoice_no,string Sid,string SubTotal,string Tax,string Discount,string Freight,string Grandtotal, string date, string qty)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Invoice));
            paramCollection.Add(new DBParameter("@Invoice_no", Invoice_no));
            paramCollection.Add(new DBParameter("@Sid", Sid));
            paramCollection.Add(new DBParameter("@SubTotal", SubTotal));
            paramCollection.Add(new DBParameter("@Tax", Tax));
            paramCollection.Add(new DBParameter("@Discount", Discount));
            paramCollection.Add(new DBParameter("@Freight", Freight));
            paramCollection.Add(new DBParameter("@Grandtotal", Grandtotal));
            paramCollection.Add(new DBParameter("@Date",date));
            paramCollection.Add(new DBParameter("@Qty", qty));

            Query = "insert into PurchaseMAIN_Add" +
                "(Invoice,Invoice_no,Sid,SubTotal,Tax,Discount,Freight,Grandtotal,Date,Qty) values (" +
                "@Invoice,@Invoice_no,@Sid,@SubTotal,@Tax,@Discount,@Freight,@Grandtotal,@Date,@Qty" +
                ")";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        public bool CheckInvoiceExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Id));

            Query = "SELECT COUNT(*) FROM PurchaseMain_Add where Invoice=@Invoice";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        

        public bool DeleteInvoice(int userID)
        {
            string Query = "Delete PurchaseMain_Add where Invoice=" + userID.ToString() + "Delete PurchaseSUB_Add where Invoice = " + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        
        public string GetHighestInvoiceId()
        {
            string query = "select ISNULL(MAX(Invoice),0)+1 from PurchaseMAIN_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalPurchase()
        {
            string query = "select SUM(GrandTotal) from purchasemain_add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalStock()
        {
            string query = "select SUM(cprice*qty) from Stock_Manage";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public DataTable GetProductInvoice(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Invoice,PId,qty,retailprice,costprice,ttlamount,discount,margin,tax,batch#,expire from ProductSUB_Add where Ac_id like '%" + userID + "%' or C_id like '%" + userID + "%' or name like '%" + userID + "%'";
            }
            else
            {
                Query = "select Invoice,PId,qty,retailprice,costprice,ttlamount,discount,margin,tax,batch#,expire from CustomerSUB_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public bool UpdateInvoiceMain(string Invoice, string Invoice_no, string Sid, string SubTotal, string Tax, string Discount, string Freight, string Grandtotal)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Invoice));
            paramCollection.Add(new DBParameter("@Invoice_no", Invoice_no));
            paramCollection.Add(new DBParameter("@Sid", Sid));
            paramCollection.Add(new DBParameter("@SubTotal", SubTotal));
            paramCollection.Add(new DBParameter("@Tax", Tax));
            paramCollection.Add(new DBParameter("@Discount", Discount));
            paramCollection.Add(new DBParameter("@Freight", Freight));
            paramCollection.Add(new DBParameter("@Grandtotal", Grandtotal));

            Query = "Update PurchaseMAIN_Add" +
                "Invoice_no=@Invoice_no,Sid=@Sid,SubTotal=@SubTotal,Tax=@Tax,Discount=@Discount,Freight=@Freight,Grandtotal=@Grandtotal " +
                "Where @Invoice = Invoice";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateInvoiceProducts(string Invoice, string PId, string Sid, string qty, string retailprice,
    string costprice, string ttlamount, string discount, string margin, string tax, string batch, string expire)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Invoice));
            paramCollection.Add(new DBParameter("@PId", PId));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@retailprice", retailprice));
            paramCollection.Add(new DBParameter("@costprice", costprice));
            paramCollection.Add(new DBParameter("@ttlamount", ttlamount));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@margin", margin));
            paramCollection.Add(new DBParameter("@tax", tax));
            paramCollection.Add(new DBParameter("@batch#", batch));
            paramCollection.Add(new DBParameter("@expire", expire));

            Query = "Update PurchaseSUB_Add Set " +
                "PId=@PId,qty=@qty,retailprice=@retailprice,costprice=@costprice,ttlamount=@ttlamount,discount=@discount,margin=@margin,tax=@tax,batch# = @batch#,expire=@expire" +
                " Where Invoice=@Invoice";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public DataTable GetDetailedTransaction(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select 'Purchase' as 'Transaction', Invoice,Date,Name,SubTotal,Tax,Discount,Freight,Grandtotal from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid";
            }
            else
            {
                Query = "select 'Purchase' as 'Transaction', Invoice,Date,Name,SubTotal,Tax,Discount,Freight,Grandtotal from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid where paid is null";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetDetailedInventory(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Invoice,Supplier_Add.name, Date as 'Invoice Date', Date as 'Purchase Date',Invoice_no as 'Invoice No', Qty, SubTotal, Discount, Freight,paid as 'Paid Amount',Balance from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid where name like '%"+userID+"%' or Invoice like '%"+userID+"%'";
            }
            else
            {
                Query = "select Invoice,Supplier_Add.name, Date as 'Invoice Date', Date as 'Purchase Date',Invoice_no as 'Invoice No', Qty, SubTotal, Discount, Freight,paid as 'Paid Amount',Balance from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetPurchaseProductList(string userID)
        {
            string query = "select Row_number() over (Order by Pid) as [S.N], PId as 'ID', Product_Add.description as 'Description' from purchaseSub_Add join Product_Add on Product_Add.PCode = PurchaseSUB_Add.PId where Invoice = '"+userID+"'";
            return _dbHelper.ExecuteDataTable(query);
        }

        public DataTable GetStockinHand()
        {
            string query = "select ROW_NUMBER() over (order by (select 1)) as Sr#, " +
                           " Sid, stock_manage.Pid as 'Product Code',Adcode as 'Aditional Code',description as 'Description', " +
                           " stock_manage.qty as 'Stock in Hand', rprice as 'Retail Price', cprice as 'Cost Price', " +
                           " location as 'Location', expire as 'Expiry Date', '' as 'Amount(R.P)', " +
                           " '' as 'Amount(C.P)' from stock_manage " +
                           " join Product_Add on Product_Add.PCode = Stock_Manage.Pid ";
            return _dbHelper.ExecuteDataTable(query);
        }
    }
}
