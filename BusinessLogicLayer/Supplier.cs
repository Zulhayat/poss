﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Supplier
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewSupplier(string aut_customerid, string user_customerid, string name, string mobile, string phone, string ntn,
            string city, string zipcode, string email, string company, string address, string comments, string image)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", aut_customerid));
            paramCollection.Add(new DBParameter("@userid", user_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@ntn", ntn));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@zipcode", zipcode));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@company", company));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@comments", comments));
            paramCollection.Add(new DBParameter("@image", image));

            Query = "insert into Supplier_Add" +
                "(Sid, userid, name, mobile, phone, ntn, city, zipcode, email, company, address, comments,image)values (" +
                "@Sid, @userid, @name, @mobile, @phone, @ntn, @city, @zipcode, @email, @company, @address, @comments,@image" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CreateNewSupplier(string aut_customerid, string name)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", aut_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            

            Query = "insert into Supplier_Add" +
                "(Sid, name)values (" +
                "@Sid, @name" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateSupplierDetails(string aut_supplierid, string user_supplierid, string name, string mobile, string phone, string ntn,
            string city, string zipcode, string email, string company, string address, string comments, string image)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", aut_supplierid));
            paramCollection.Add(new DBParameter("@userid", user_supplierid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@ntn", ntn));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@zipcode", zipcode));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@company", company));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@comments", comments));
            paramCollection.Add(new DBParameter("@image", image));

            Query = "UPDATE Supplier_Add SET " +
            "userid=@userid, name=@name, mobile=@mobile, phone=@phone, ntn=@ntn, city=@city, zipcode=@zipcode, email=@email, company=@company, address=@address, comments=@comments,image=@image " +
            "WHERE Sid = @Sid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateSupplierDetails(string aut_supplierid, string name)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", aut_supplierid));
            paramCollection.Add(new DBParameter("@name", name));

            Query = "UPDATE Supplier_Add SET " +
            "name=@name " +
            "WHERE Sid = @Sid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckSupplierExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", Id));

            Query = "SELECT COUNT(*) FROM Supplier_Add where Sid=@Sid";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool DeleteSupplier(int userID)
        {
            string Query = "Delete Supplier_Add where SId=" + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public string GetSupplierId(string userName)
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            string Query = "SELECT SID from supplier_Add where name='"+userName+"'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetSupplierName(string userName)
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            string Query = "SELECT Name from supplier_Add where SID = '" + userName + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetHighestSupplierId()
        {
            string query = "select ISNULL(MAX(Sid),0)+1 from Supplier_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }


        public DataTable GetSupplierProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Sid as Serial#, userid as ID, Name, mobile, Phone,NTN,City,ZipCode,Email,Company,Address,Comments,Image from Supplier_Add where Sid like '%" + userID + "%' or Name like '%" + userID + "%' ";
            }
            else
            {
                Query = "select Sid as Serial#, userid as ID, Name, mobile, Phone,NTN,City,ZipCode,Email,Company,Address,Comments,Image from Supplier_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public bool UpdateSupplierProfile(int userID, string firstName, string lastName, string email, string mobile)
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@firstName", firstName));
            paramCollection.Add(new DBParameter("@lastName", lastName));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@userId", userID));

            string Query = "UPDATE User_Info SET First_Name=@firstName, " +
                "Last_Name=@lastName, Email = @email, Mobile = @mobile " +
                "WHERE User_Id=@userId";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        public bool UpdateUserProfile(int userID, string firstName, string lastName, string password, string email, string mobile)
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@firstName", firstName));
            paramCollection.Add(new DBParameter("@lastName", lastName));
            paramCollection.Add(new DBParameter("@password", _securityProvider.Encrypt(password)));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@userId", userID));

            string Query = "UPDATE User_Info SET First_Name=@firstName, Last_Name=@lastName, " +
                "Pwd=@password , Password_Change_Date='" + DateTime.Now.ToString() + "', " +
                "Email=@email , Mobile=@mobile" +
                " WHERE User_Id=@userId";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }
    }
}
