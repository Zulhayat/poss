﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.SqlClient;

namespace BusinessLogicLayer
{
   public class Customers
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewCustomer(string aut_customerid, string user_customerid, string name, string dob, string city, string phone,
            string mobile, string cnic, string address,string imagename, string gender)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@AC_id", aut_customerid));
            paramCollection.Add(new DBParameter("@C_id", user_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@dob", dob));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@cnic", cnic));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@gender", gender));
            paramCollection.Add(new DBParameter("@image", imagename));

            Query = "insert into Customer_Add" +
                "(AC_id, C_id, name, dob, city, phone, mobile, cnic, address,gender,image) values (" +
                "@AC_id, @C_id, @name, @dob, @city, @phone, @mobile, @cnic, @address, @gender,@image" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CreateNewReferencedCustomer(string aut_customerid, string user_customerid, string name, string dob, string city, string phone,
             string mobile, string cnic, string address, string image, string gender)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@AC_id", aut_customerid));
            paramCollection.Add(new DBParameter("@C_id", user_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@dob", dob));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@cnic", cnic));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@image", image));
            paramCollection.Add(new DBParameter("@gender", gender));

            Query = "insert into Customer_Add_Ref" +
                "(AC_id, C_id, name, dob, city, phone, mobile, cnic, address,image,gender) values (" +
                "@AC_id, @C_id, @name, @dob, @city, @phone, @mobile, @cnic, @address, @image,@gender" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateCustomerDetails(string customerid, string customername)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@C_id", customerid));
            paramCollection.Add(new DBParameter("@C_name", customername));

            Query = "UPDATE Customer_Add SET " +
            "C_Name = @C_name " +
            "WHERE C_id = @C_id";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckCustomerExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@AC_id", Id));

            Query = "SELECT COUNT(*) FROM Customer_Add where AC_id=@AC_id";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool CheckRefCustomerExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@C_id", Id));

            Query = "SELECT COUNT(*) FROM Customer_Add_Ref where C_id=@C_id";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool DeleteCustomer(int userID)
        {
            string Query = "Delete Customer_Add where Ac_id=" + userID.ToString() + "Delete Customer_Add_Ref where C_id = " + userID.ToString();
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetCustomerId(string userName)
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@name", userName));
            string Query = "select Ac_id from customer_Add where name like @name";
            return _dbHelper.ExecuteDataTable(Query,paramCollection);
        }

        public DataTable GetCountingCustomers()
        {
            DBParameterCollection paramCollection = new DBParameterCollection();
            string Query = "select Count(*) from customer_Add";
            return _dbHelper.ExecuteDataTable(Query, paramCollection);
        }

        public string GetHighestUserId()
        {
            string query = "select ISNULL(MAX(AC_id),0)+1 from Customer_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetHighestRefId()
        {
            string query = "select ISNULL(MAX(AC_id),0)+1 from Customer_Add_Ref";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetUserName(int userID)
        {
            string Query = "SELECT User_Name from User_Info where User_Id=" + userID.ToString();

            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public bool IsActiveUser(int userID)
        {
            string Query = "SELECT IsActive from User_Info where User_Id=" + userID.ToString();

            if (_dbHelper.ExecuteScalar(Query).ToString().Equals("1"))
                return true;
            else
                return false;

        }

        public DataTable GetCustomerProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Ac_id as 'Sr#', C_id as 'ID', name, dob, city, phone, mobile, cnic, address,gender, image from Customer_Add where Ac_id like '%"+userID+"%' or C_id like '%"+userID+"%' or name like '%"+userID+"%'";
            }
            else
            {
                Query = "select Ac_id as 'Sr#', C_id as 'ID', name, dob, city, phone, mobile, cnic, address, gender, image from Customer_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetRefCustomerProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select AC_id, C_id, name, dob, city, phone, mobile, cnic, address,gender, image from Customer_Add_Ref where C_id like '%" + userID + "%'";
            }
            else
            {
                Query = "select AC_id, C_id, name, dob, city, phone, mobile, cnic, address,gender, image from Customer_Add_Ref";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public string GetUserFirstLastName(int userID)
        {
            string userName = string.Empty;
            string Query = "SELECT First_Name + ' ' + Last_Name from User_Info where User_Id=" + userID.ToString();
            userName = _dbHelper.ExecuteScalar(Query).ToString();

            return userName;
        }

        public bool UpdateNewCustomer(string aut_customerid, string user_customerid, string name, string dob, string city, string phone,
            string mobile, string cnic, string address,string imagename, string gender)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@AC_id", aut_customerid));
            paramCollection.Add(new DBParameter("@C_id", user_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@dob", dob));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@cnic", cnic));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@gender", gender));
            paramCollection.Add(new DBParameter("@image", imagename));

            Query = "Update Customer_Add Set " +
                "C_id = @C_id, name = @name, dob=@dob, city =  @city, phone =  @phone, mobile=@mobile, cnic=@cnic, address=@address,gender=@gender,image = @image " +
                "Where AC_id = @AC_id";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        public bool UpdateNewReferencedCustomer(string aut_customerid, string user_customerid, string name, string dob, string city, string phone,
             string mobile, string cnic, string address, string image, string gender)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@AC_id", aut_customerid));
            paramCollection.Add(new DBParameter("@C_id", user_customerid));
            paramCollection.Add(new DBParameter("@name", name));
            paramCollection.Add(new DBParameter("@dob", dob));
            paramCollection.Add(new DBParameter("@city", city));
            paramCollection.Add(new DBParameter("@phone", phone));
            paramCollection.Add(new DBParameter("@mobile", mobile));
            paramCollection.Add(new DBParameter("@cnic", cnic));
            paramCollection.Add(new DBParameter("@address", address));
            paramCollection.Add(new DBParameter("@image", image));
            paramCollection.Add(new DBParameter("@gender", gender));

            Query = "Update Customer_Add_Ref Set " +
                "C_id = @C_id, name = @name, dob = @dob, city = @city, phone = @phone, mobile=@mobile, cnic=@cnic, address = @address,image =  @image, gender=@gender " +
                "Where C_id = @AC_id";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }
    }
}
