﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class Common
    {
        public enum UserRole
        {
            Admin, GeneralUser
        }

        public static UserRole GetUserRole(int roleId)
        {
            UserRole userRole = new UserRole();
            string sqlQuery = "SELECT Role from RoleDetails Where RoleId=" + roleId.ToString();
            object role = (new DBHelper()).ExecuteScalar(sqlQuery);
            if (role != null)
            {
                switch (role.ToString().ToUpper())
                {
                    case "ADMIN":
                        userRole = UserRole.Admin;
                        break;
                    case "USER":
                        userRole = UserRole.GeneralUser;
                        break;
                    default:
                        userRole = UserRole.GeneralUser;
                        break;
                }
            }

            return userRole;
        }

    }
}
