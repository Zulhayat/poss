﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class City
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewCity(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@City", city));
            
            Query = "insert into City_Add" +
                "(Cid, City) values (" +
                "@Cid, @City" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateCityDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@City", city));

            Query = "UPDATE City_Add SET " +
            "City = @City " +
            "WHERE Cid = @Cid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckCityExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@City", city));

            Query = "SELECT COUNT(*) FROM City_Add where City=@City";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from city_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        


        public bool DeleteCity(string userID)
        {
            string Query = "Delete City_Add where City='" + userID.ToString() +"'"; 
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetCityProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid, City from City_Add where City like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT City from City_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
