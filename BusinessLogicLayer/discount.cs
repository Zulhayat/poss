﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using DataFormatting;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class discount
    {
        private DBHelper _dbHelper = new DBHelper();

        /// <summary>
        /// Checks whether item with the specified name exists or not
        /// </summary>
        /// <param name="itemName">Item name</param>
        /// <returns>True if item with the specified name exists otherwise false</returns>
        public bool discountExist(string itemName)
        {
            string strCount = _dbHelper.ExecuteScalar("SELECT COUNT(*) FROM Discount WHERE Sid='"+itemName+"'").ToString();
            return Convert.ToInt16(strCount) > 0;
        }

        /// <summary>
        /// Gets Item Id for the specified Item name
        /// </summary>
        /// <param name="itemName">Item Name</param>
        /// <returns>Item Id</returns>
        public string GetDiscountId(string itemName)
        {
            string Query = "SELECT Did from Discount where Sid='" + itemName + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }


        /// <summary>
        /// Adds a new row to the Item_Details table for the specified values
        /// </summary>
        /// <param name="itemName">Item Name</param>
        /// <param name="itemDesc">Item Description</param>
        /// <param name="createdBy">Created By user Id</param>
        /// <param name="createdDate">Created on date</param>
        /// <returns>true if item is created successfully otherwise false</returns>
        public bool AddNewDiscount(string sale_id, string discount_id, string Sub_total, string discount, string per_discount,
            string tax, string per_tax, string freight, string adjustment, string totalamt)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@SId", sale_id));
            paramCollection.Add(new DBParameter("@DId", discount_id));
            paramCollection.Add(new DBParameter("@Subtotal", Sub_total));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@per_discount", per_discount));
            paramCollection.Add(new DBParameter("@tax", tax));
            paramCollection.Add(new DBParameter("@per_tax", per_tax));
            paramCollection.Add(new DBParameter("@freight", freight));
            paramCollection.Add(new DBParameter("@adjustment", adjustment));
            paramCollection.Add(new DBParameter("@totalamt", totalamt));
            Query = "INSERT INTO Discount (Sid, Did, subtotal, discount, per_discount, tax, per_tax, freight, adjustment, totalamt) VALUES (";
            Query = Query + "@Sid, @Did, @subtotal, @discount, @per_discount, @tax, @per_tax, @freight, @adjustment, @totalamt)";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        /// <summary>
        /// Updates item details for an item of specified itemId and Item details
        /// </summary>
        /// <param name="itemID">Item Id</param>
        /// <param name="itemDesc">Item Description</param>
        /// <param name="entryBy">Item modified by</param>
        /// <param name="entryDate">Item modified date</param>
        /// <returns>true if item is modified successfully otherwise false</returns>
        public bool UpdateDiscount(int itemID, string itemDesc, string entryBy, string entryDate)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@itemDesc", itemDesc));
            paramCollection.Add(new DBParameter("@entryBy", entryBy));
            paramCollection.Add(new DBParameter("@entryDate", entryDate, System.Data.DbType.DateTime));
            paramCollection.Add(new DBParameter("@itemID", itemID));
            Query = "UPDATE Item_Details SET Item_Desc=@itemDesc , " +
                "Created_By =@entryBy, Entry_Date = @entryDate " +
                "WHERE Item_Id=@itemID";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        /// <summary>
        /// Gets list of items having specified item name and item description.
        /// Pass String.Empty, String.Empty for fetching all the records i.e. GetItems("", "")
        /// </summary>
        /// <param name="itemName">Item name</param>
        /// <param name="itemDesc">Item description</param>
        /// <returns>Item collection in the form of data table</returns>
        public DataTable GetDiscount(string itemName, string itemDesc)
        {
            StringBuilder sqlCommand = new StringBuilder("SELECT Item_Id, Item_Name, Item_Desc, IsActive From Item_Details ");
            DBParameterCollection paramCollection = new DBParameterCollection();

            if (itemName != string.Empty)
            {
                sqlCommand.Append(" WHERE Item_Name LIKE @itemName");
                paramCollection.Add(new DBParameter("@itemName", itemName));
            }

            if (itemName != string.Empty && itemDesc != string.Empty)
            {
                sqlCommand.Append(" AND Item_Name LIKE @itemDesc ");
                paramCollection.Add(new DBParameter("@itemDesc", itemDesc));
            }
            else if (itemName == string.Empty && itemDesc != string.Empty)
            {
                sqlCommand.Append(" WHERE Item_Name LIKE @itemDesc");
                paramCollection.Add(new DBParameter("@itemDesc", itemDesc));
            }

            return _dbHelper.ExecuteDataTable(sqlCommand.ToString(), paramCollection);
        }

        /// <summary>
        /// Gets Item description for the specified item id.
        /// </summary>
        /// <param name="itemId">Item id</param>
        /// <returns>Item description</returns>
        public string GetDiscountDescription(string itemId)
        {
            string sqlCommand = "SELECT Item_Desc From Item_Details Where Item_Id = " + itemId;
            return DataFormat.GetString(_dbHelper.ExecuteScalar(sqlCommand));
        }

        public string GetDiscountHighestId()
        {
            string Query = "SELECT ISNULL(MAX(Did),0)+1 from Discount";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }
    }
}
