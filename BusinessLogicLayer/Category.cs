﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
public    class Category
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewCategory(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Category", city));

            Query = "insert into Category_Add" +
                "(Cid, Category) values (" +
                "@Cid, @Category" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateCategoryDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", cid));
            paramCollection.Add(new DBParameter("@Category", city));

            Query = "UPDATE Category_Add SET " +
            "Category = @Category " +
            "WHERE Cid = @Cid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckCategoryExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Category", city));

            Query = "SELECT COUNT(*) FROM Category_Add where Category=@Category";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from Category_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetCategoryID(string search)
        {
            string Query = string.Empty;
            Query = "select CID from Category_Add where Category = '"+search+"'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetCategoryName(string search)
        {
            string Query = string.Empty;
            Query = "select Category from Category_Add where CID = '" + search + "'";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public bool DeleteCategory(string userID)
        {
            string Query = "Delete Category_Add where Category='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetCategoryProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid, Category from Category_Add where Category like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT Category from Category_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
