﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Manufacturer
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();
        private Arch _arch = new Arch();

        public bool CreateNewMan(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@CId", cid));
            paramCollection.Add(new DBParameter("@Man", city));

            Query = "insert into Man_Add" +
                "(CId, Man) values (" +
                "@CId, @Man" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateManDetails(string cid, string city)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@CId", cid));
            paramCollection.Add(new DBParameter("@Man", city));

            Query = "UPDATE Man_Add SET " +
            "Man = @Man " +
            "WHERE CId = @CId";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckManExist(string city)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Man", city));

            Query = "SELECT COUNT(*) FROM Man_Add where Man=@Man";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from Man_add";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }

        public string GetManID(string search)
        {
            string Query = string.Empty;
            Query = "SELECT Cid from Man_Add where Man = '" + search + "'";
            return _dbHelper.ExecuteDataTable(Query).ToString();
        }

        public string GetManName(string search)
        {
            string Query = string.Empty;
            if (search != "0")
            {
                Query = "SELECT Man from Man_Add where Cid = '" + search + "'";
                return _dbHelper.ExecuteScalar(Query).ToString();
            }
            else
            {
                return "Nill";
            }
        }

        public bool DeleteMan(string userID)
        {
            string Query = "Delete Man_Add where Man='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetManProfile(string City)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (City != string.Empty)
            {
                Query = "SELECT Cid, Man from Man_Add where Man like '%" + City.ToString() + "%'";
            }
            else
            {
                Query = "SELECT Man from Man_Add";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
