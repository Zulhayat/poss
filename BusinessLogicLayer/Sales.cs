﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Sales
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool SaveInvoiceProducts(string Invoice, string PId, string qty, string retailprice,
             string ttlamount, string discount, string final)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Sid", Invoice));
            paramCollection.Add(new DBParameter("@PId", PId));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@retailprice", retailprice));
            paramCollection.Add(new DBParameter("@total", ttlamount));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@final", final));

            Query = "insert into salesub_add" +
                "(Sid,Pid,qty,retailprice,total,discount,final)values(" +
                "@Sid, @Pid, @qty, @retailprice, @total, @discount, @final" +
                ")" +
                "update Stock_Manage set qty = qty - @qty where Pid = @PId";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool SaveInvoiceMain(string Invoice, string CustomerID, string qty, string discount_, string total, DateTime Date,string visiter, string cashpaid, string change, string subtotal)
        {
            char[] charstoRemove = { 'Q', 't', 'y','R','s','S', '.', ' ' };
            string Query = string.Empty;
            string replacespace_discount = discount_.Trim(charstoRemove);
            string replacespace_cashpaid = cashpaid.Trim(charstoRemove);
            string replacespace_change = change.Trim(charstoRemove);
            string replacespace_subtotal = change.Trim(charstoRemove);
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@SId", Invoice));
            paramCollection.Add(new DBParameter("@Cid", CustomerID));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@discount", replacespace_discount));
            paramCollection.Add(new DBParameter("@total", total));
            paramCollection.Add(new DBParameter("@Date", Date));
            paramCollection.Add(new DBParameter("@Customer", visiter));
            paramCollection.Add(new DBParameter("@paid", replacespace_cashpaid));
            paramCollection.Add(new DBParameter("@change", replacespace_change));
            paramCollection.Add(new DBParameter("@subtotal", replacespace_subtotal));

            if (CustomerID != "")
            {
                Query = "insert into SaleMain_Add" +
                    "(SId,Cid,qty,discount,total,Date)values(" +
                    "@SId,@Cid,@qty,@discount,@total,@Date" +
                    ")";
            }
            else
            {
                Query = "insert into SaleMain_Add" +
                    "(SId,Cid,qty,discount,total,Date)values(" +
                    "@SId,@Cid,@qty,@discount,@total,@Date" +
                    ") insert into visitingcustomer (Sid, Customer, subtotal, discount, paid, change, total)values(@SId, @Customer,@subtotal, @discount, @paid, @change, @total)";
            }
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }


        public bool CheckInvoiceExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Id));

            Query = "SELECT COUNT(*) FROM SaleMain_Add where SId=@Invoice";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }



        public bool DeleteInvoice(int userID)
        {
            string Query = "delete from salemain_add where Sid = '" + userID + "'  delete from SaleSub_Add where Sid = '" + userID + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }


        public string GetHighestInvoiceId()
        {
            string query = "select ISNULL(MAX(Sid),0)+1 from SaleMain_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalSales()
        {
            string query = "select SUM(Total) from SaleMain_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalStock()
        {
            string query = "select SUM(cprice*qty) from Stock_Manage";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalInvoices()
        {
            string query = "select Count(Sid) from Salemain_Add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalSubTotal()
        {
            string query = "select SUM(total) from salesub_add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }

        public string GetTotalDiscount()
        {
            string query = "select SUM(Discount) from salemain_add";
            return _dbHelper.ExecuteScalar(query).ToString();
        }


        public DataTable GetSaleInvoice(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select ROW_NUMBER() over (order by SaleMain_Add.Sid DESC) as 'Sr#', " +
                        " SaleMain_Add.Sid as 'Receipt no',Date,name as 'Customer',SaleMain_Add.qty as qty,SaleMain_Add.Discount,'' as Freight," +
                        " total as Amount, '' as 'Amount Paid','' as Change, '' as SalesPerson, " +
                        " '' as PaymentMethod from SaleMain_Add " +
                        " join Customer_Add on Customer_Add.AC_id = SaleMain_Add.Cid where name like '%"+userID+"%'";
            }
            else
            {
                Query = "select ROW_NUMBER() over (order by SaleMain_Add.Sid DESC) as 'Sr#', " +
                        " SaleMain_Add.Sid as 'Receipt no',Date,name as 'Customer',SaleMain_Add.qty as qty,SaleMain_Add.Discount,'' as Freight," +
                        " total as Amount, '' as 'Amount Paid','' as Change, '' as SalesPerson, " +
                        " '' as PaymentMethod from SaleMain_Add " +
                        " join Customer_Add on Customer_Add.AC_id = SaleMain_Add.Cid";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public bool UpdateInvoiceMain(string Invoice, string CustomerID, string qty, string discount_, string total, DateTime Date)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@sid", Invoice));
            paramCollection.Add(new DBParameter("@Cid", CustomerID));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@discount", discount_));
            paramCollection.Add(new DBParameter("@total", total));
            paramCollection.Add(new DBParameter("@date", Date));

            Query = "update salemain_add set " +
                "Cid = @Cid, qty =@qty, discount = @discount, total = @total, Date = @date  " +
                "where SId = @sid";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public DataTable GetSaleMain_Add(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();

            Query = "select SId,name,qty,discount,total, Date from salemain_add " +
                    " join Customer_Add on Customer_Add.AC_id = SaleMain_Add.Cid " +
                    " where SId ='"+userID+"' ";
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetSaleSub_Add(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();

            Query = "select Pid,description,qty,retailprice,total,discount,final from salesub_add " +
                    " join Product_Add on Product_Add.PCode = SaleSub_Add.Pid where Sid = '"+userID+"' ";
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public bool UpdateInvoiceProducts(string Invoice,string PId, string qty, string retailprice, string total,string discount, 
            string final)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Invoice));
            paramCollection.Add(new DBParameter("@PId", PId));
            paramCollection.Add(new DBParameter("@qty", qty));
            paramCollection.Add(new DBParameter("@retailprice", retailprice));
            paramCollection.Add(new DBParameter("@ttlamount", total));
            paramCollection.Add(new DBParameter("@discount", discount));
            paramCollection.Add(new DBParameter("@final", final));

            Query = "update Stock_Manage set qty = qty + @qty where Pid =@Pid " +
                " delete from SaleSub_Add where Pid = @Pid and Sid = @Invoice " +
                " insert into SaleSub_Add(Sid, Pid, qty, retailprice, total, discount, final) " +
                " values(@Invoice, @Pid, @qty, @retailprice, @ttlamount, @discount, @final)";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public DataTable GetDetailedTransaction(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select 'Purchase' as 'Transaction', Invoice,Date,Name,SubTotal,Tax,Discount,Freight,Grandtotal from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid";
            }
            else
            {
                Query = "select 'Purchase' as 'Transaction', Invoice,Date,Name,SubTotal,Tax,Discount,Freight,Grandtotal from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid where paid is null";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetDetailedInventory(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Invoice,Supplier_Add.name, Date as 'Invoice Date', Date as 'Purchase Date',Invoice_no as 'Invoice No', Qty, SubTotal, Discount, Freight,paid as 'Paid Amount',Balance from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid where name like '%" + userID + "%' or Invoice like '%" + userID + "%'";
            }
            else
            {
                Query = "select Invoice,Supplier_Add.name, Date as 'Invoice Date', Date as 'Purchase Date',Invoice_no as 'Invoice No', Qty, SubTotal, Discount, Freight,paid as 'Paid Amount',Balance from PurchaseMain_Add join Supplier_Add on Supplier_Add.Sid = PurchaseMain_Add.Sid";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }

        public DataTable GetPurchaseProductList(string userID)
        {
            string query = "select Row_number() over (Order by Pid) as [S.N], PId as 'ID', Product_Add.description as 'Description' from purchaseSub_Add join Product_Add on Product_Add.PCode = PurchaseSUB_Add.PId where Invoice = '" + userID + "'";
            return _dbHelper.ExecuteDataTable(query);
        }

        public DataTable GetStockinHand()
        {
            string query = "select ROW_NUMBER() over (order by (select 1)) as Sr#, " +
                           " Sid, stock_manage.Pid as 'Product Code',Adcode as 'Aditional Code',description as 'Description', " +
                           " stock_manage.qty as 'Stock in Hand', rprice as 'Retail Price', cprice as 'Cost Price', " +
                           " location as 'Location', expire as 'Expiry Date', '' as 'Amount(R.P)', " +
                           " '' as 'Amount(C.P)' from stock_manage " +
                           " join Product_Add on Product_Add.PCode = Stock_Manage.Pid ";
            return _dbHelper.ExecuteDataTable(query);
        }
    }
}
