﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class CsUsers
    {
        private DBHelper _dbHelper = new DBHelper();
        private DataSecurity _securityProvider = new DataSecurity();

        private Arch _arch = new Arch();



        public bool CreateNewUser(string Uid, string uname, string password, string email, string date_login, string start, string eend)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Uid", Uid));
            paramCollection.Add(new DBParameter("@uname", uname));
            paramCollection.Add(new DBParameter("@password", password));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@date_login", date_login));
            paramCollection.Add(new DBParameter("@start", start));
            paramCollection.Add(new DBParameter("@eend", eend));

            Query = "insert into users" +
                "(Uid,uname,password,email,date_login,start,eend) values (" +
                "@Uid, @uname, @password, @email, @date_login, @start, @eend" +
                ")";

            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateUserDetails(string Uid, string uname, string password, string email, string date_login, string start, string eend)
        {
            string Query = string.Empty;

            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Uid", Uid));
            paramCollection.Add(new DBParameter("@uname", uname));
            paramCollection.Add(new DBParameter("@password", password));
            paramCollection.Add(new DBParameter("@email", email));
            paramCollection.Add(new DBParameter("@date_login", date_login));
            paramCollection.Add(new DBParameter("@start", start));
            paramCollection.Add(new DBParameter("@eend", eend));

            Query = "Update users set" +
                "uname=@uname,password=@password,email=@email,date_login=@date_login,start=@start,eend=@eend " +
                "where Uid = @Uid" +
                ")";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckUserExist(string uname)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@uname", uname));

            Query = "SELECT COUNT(*) FROM users where uname=@uname";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public bool CheckUserLogin(string uname, string password)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@uname", uname));
            paramCollection.Add(new DBParameter("@password", password));

            Query = "SELECT COUNT(*) FROM users where uname=@uname and password = @password";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public string GetHighestID()
        {
            string Query = string.Empty;
            Query = "select ISNULL(MAX(cid),0)+1 from users";
            return _dbHelper.ExecuteScalar(Query).ToString();
        }




        public bool DeleteUser(string userID)
        {
            string Query = "Delete from users where userid='" + userID.ToString() + "'";
            return (_dbHelper.ExecuteNonQuery(Query) > 0);
        }

        public DataTable GetUserProfile(string Uid)
        {
            DataTable dtProfile = new DataTable();
            string Query = string.Empty;
            if (Uid != string.Empty)
            {
                Query = "SELECT Uid, Uname from users where uname like '%" + Uid.ToString() + "%'";
            }
            else
            {
                Query = "SELECT uname from users";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
