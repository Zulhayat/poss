﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Threading.Tasks;


namespace BusinessLogicLayer
{
    public class Comments
    {
        private DBHelper _dbHelper = new DBHelper();
        
        public bool AddNewComment(string commentid, string comment, string patient, string consultation, string designation,string hospital, string Invoice)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", commentid));
            paramCollection.Add(new DBParameter("@Comment", comment));
            paramCollection.Add(new DBParameter("@Patient", patient));
            paramCollection.Add(new DBParameter("@Consultant", consultation));
            paramCollection.Add(new DBParameter("@Designation", designation));
            paramCollection.Add(new DBParameter("@Hospital", hospital));
            paramCollection.Add(new DBParameter("@Receipt", Invoice));
            Query = "insert into comments(Cid, Comment, Patient, Consultant, Designation, Hospital,receipt)values(";
            Query = Query + "@Cid, @Comment, @Patient, @Consultant, @Designation, @Hospital,@Receipt)";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool UpdateComment(string commentid, string comment, string patient, string consultation, string designation, string hospital, string Invoice)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Cid", commentid));
            paramCollection.Add(new DBParameter("@Comment", comment));
            paramCollection.Add(new DBParameter("@Patient", patient));
            paramCollection.Add(new DBParameter("@Consultant", consultation));
            paramCollection.Add(new DBParameter("@Designation", designation));
            paramCollection.Add(new DBParameter("@Hospital", hospital));
            paramCollection.Add(new DBParameter("@Receipt", Invoice));
            Query = "Update comments set Cid =@Cid, Comment = @Comment, Patient= @Patient, Consultant= @Consultant, Designation=@Designation, Hospital=@Hospital"+
                " where receipt= @Receipt";
            return _dbHelper.ExecuteNonQuery(Query, paramCollection) > 0;
        }

        public bool CheckInvoiceExist(string Id)
        {
            string Query = string.Empty;
            DBParameterCollection paramCollection = new DBParameterCollection();
            paramCollection.Add(new DBParameter("@Invoice", Id));

            Query = "SELECT COUNT(*) FROM comments where receipt=@Invoice";

            return Convert.ToInt16(_dbHelper.ExecuteScalar(Query, paramCollection).ToString()) > 0;
        }

        public DataTable GetCommentProfile(string userID)
        {
            string Query = string.Empty;
            DataTable dtProfile = new DataTable();
            if (userID != string.Empty)
            {
                Query = "select Cid, Comment,Customer_Add.name, Consultant, Designation, Hospital_Add.Hospital, receipt from Comments" +
                        " join Hospital_Add on Hospital_Add.Hid = Comments.Hospital" +
                        " join Customer_Add on Customer_Add.AC_id = Comments.Patient where receipt = '"+userID+"'";
            }
            dtProfile = _dbHelper.ExecuteDataTable(Query);
            return dtProfile;
        }
    }
}
